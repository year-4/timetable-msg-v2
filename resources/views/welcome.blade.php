@extends('layouts.app')

@section('content')

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">Education Website</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Courses</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Slider -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slide1.jpg" class="d-block w-100" alt="Slide 1">
    </div>
    <div class="carousel-item">
      <img src="images/slide2.jpg" class="d-block w-100" alt="Slide 2">
    </div>
    <div class="carousel-item">
      <img src="images/slide3.jpg" class="d-block w-100" alt="Slide 3">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!-- Header -->
<header class="text-center mt-4">
  <h1>Welcome to Our Education Website</h1>
  <p>Explore our wide range of courses and take your learning to the next level.</p>
  <a href="#" class="btn btn-primary">Get Started</a>
</header>

<!-- Course Cards -->
<section class="container mt-4">
  <div class="row">
    <!-- Replace the placeholders with actual course cards -->
    <div class="col-md-4 mb-4">
      <div class="card">
        <img src="images/course1.jpg" class="card-img-top" alt="Course 1">
        <div class="card-body">
          <h5 class="card-title">Course 1</h5>
          <p class="card-text">Description of Course 1</p>
          <a href="#" class="btn btn-primary">Enroll Now</a>
        </div>
      </div>
    </div>
    <div class="col-md-4 mb-4">
      <div class="card">
        <img src="images/course2.jpg" class="card-img-top" alt="Course 2">
        <div class="card-body">
          <h5 class="card-title">Course 2</h5>
          <p class="card-text">Description of Course 2</p>
          <a href="#" class="btn btn-primary">Enroll Now</a>
        </div>
      </div>
    </div>
    <div class="col-md-4 mb-4">
      <div class="card">
        <img src="images/course3.jpg" class="card-img-top" alt="Course 3">
        <div class="card-body">
          <h5 class="card-title">Course 3</h5>
          <p class="card-text">Description of Course 3</p>
          <a href="#" class="btn btn-primary">Enroll Now</a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Footer -->
<footer class="bg-dark text-white text-center py-3">
  <p>&copy; 2023 Education Website. All rights reserved.</p>
</footer>

@endsection
