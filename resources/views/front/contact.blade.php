@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-hero,
            .untree_co-hero>.container>.row,
            .bg-img,
            .bg-img>.container>.row {
                height: 50vh;
                min-height: 409px;
            }


            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }
                .banner-text{
                    font-size: 20px;
                 }
            }
        </style>
  @endpush
  @php
  if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
      $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
  } else {
      $image_url = asset('images/default.svg');
  }
@endphp
<div class="untree_co-hero overlay" style="background-image: url('{{ $image_url }}');">
  <div class="container">
      <div class="row align-items-center justify-content-center">
          <div class="col-12">
              <div class="row justify-content-center">
                  <div class="col-lg-6 text-center">
                      <h1 class="mb-4 banner-text text-white" data-aos="fade-up" data-aos-delay="100">
                          {{ __('Contact') }}
                      </h1>
                      <p class="mb-0" data-aos="fade-up" data-aos-delay="300"><a href="#"
                              class="btn btn-secondary">{{ __('Login Now') }}</a></p>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>




  <div class="untree_co-section">
    <div class="container">

      <div class="row mb-5" style="justify-content: center">
        <div class="col-lg-4 mb-5 order-2 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="contact-info">

            <div class="address mt-4">
              <i class="icon-room "style="background-color: #009D8A"></i>
              <h4 class="mb-2">{{__('Location:')}}</h4>
              <p>{{$address}}</p>
            </div>

            <div class="open-hours mt-4">
              <i class="icon-clock-o"style="background-color: #009D8A"></i>
              <h4 class="mb-2">{{__('Open Hours:')}}</h4>
              <p>
                {{__('Monday-Saturday:')}}<br>
                {{__('7:00 AM - 17:00 PM')}}
              </p>
            </div>

            <div class="email mt-4">
              <i class="icon-envelope" style="background-color: #009D8A"></i>
              <h4 class="mb-2">{{__('Email:')}}</h4>
              <p>{{$email}}</p>
            </div>

            <div class="phone mt-4">
              <i class="icon-phone"style="background-color: #009D8A"></i>
              <h4 class="mb-2">{{__('Call:')}}</h4>
              <p>{{$phone_number}}</p>
              <p>{{$phone_number1}}</p>
            </div>

          </div>
        </div>
        <div class="col-lg-7 mr-auto order-1 d-none" data-aos="fade-up" data-aos-delay="200">
          <form action="#">
            <div class="row">
                <div class="col-6 mb-3">
                    <input type="text" class="form-control" placeholder="{{__('Your Name')}}">
                </div>
                <div class="col-6 mb-3">
                    <input type="email" class="form-control" placeholder="{{__('Your Email')}}">
                </div>
                <div class="col-12 mb-3">
                    <input type="text" class="form-control" placeholder="{{__('Subject ')}}">
                </div>
                <div class="col-12 mb-3">
                    <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="{{__('Message')}}"></textarea>
                </div>

                <div class="col-12">
                    <input type="submit" value="{{__('Send Message')}}" class="btn btn-secondary">
                </div>
            </div>
        </form>
        </div>
      </div>


    </div>
  </div>

@endsection
