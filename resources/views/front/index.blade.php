 @extends('front.layouts.app')
 @section('content')
     @push('style')
         <style>
             @media (max-width:995px) {
                 .school-ac {
                     flex-direction: column-reverse
                 }
                 .banner-text{
                    font-size: 20px;
                 }
             }
         </style>
     @endpush

     @php
         if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
             $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
         } else {
             $image_url = asset('images/default.svg');
         }
     @endphp
     <div class="untree_co-hero overlay" style="background-image: url('{{ $image_url }}');">
         <div class="container">
             <div class="row align-items-center justify-content-center">
                 <div class="col-12">
                     <div class="row justify-content-center">
                         <div class="col-lg-6 text-center">
                             <h1 class="mb-4 text-white banner-text" data-aos="fade-up" data-aos-delay="100">
                                 {{ $banner_text }}
                             </h1>
                             <p class="mb-0" data-aos="fade-up" data-aos-delay="300"><a href="{{ route('login') }}"
                                     class="btn btn-secondary">{{ __('Login Now') }}</a></p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>



     <div class="untree_co-section">
         <div class="container">
             <div class="row justify-content-center mb-3">
                 <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="0">
                     <h2 class="line-bottom text-center mb-4">{{ __('Timetable') }}</h2>
                 </div>
             </div>
             <div class="row align-items-stretch">


                 @foreach ($grades as $grade)
                     <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="0">
                         <a href="{{ route('front.timetable', ['grade_id' => $grade->id]) }}"
                             class="category d-flex align-items-start h-100">
                             <div>
                                 <i class="uil uil-schedule"></i>
                             </div>
                             <div>
                                 <h3 class="text-center">{{ __('Grade') }} {{ $grade->name }}</h3>
                                 <span>{{ $grade->classes_count }} {{ __('Class') }}</span>
                             </div>
                         </a>
                     </div>
                 @endforeach
                 {{-- <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="100">
                     <a href="#" class="category d-flex align-items-start h-100">
                         <div>
                             <i class="uil uil-schedule"></i>
                         </div>
                         <div>
                             <h3 class="text-center">{{ __('Grade') }} 8</h3>
                             <span>71,391 {{ __('Class') }}</span>
                         </div>
                     </a>
                 </div>
                 <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="200">
                     <a href="#" class="category d-flex align-items-start h-100">
                         <div>
                             <i class="uil uil-schedule"></i>
                         </div>
                         <div>
                             <h3 class="text-center">{{ __('Grade') }} 9</h3>
                             <span>71,391 {{ __('Class') }}</span>
                         </div>
                     </a>
                 </div>
                 <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="300">
                     <a href="#" class="category d-flex align-items-start h-100">
                         <div>
                             <i class="uil uil-schedule"></i>
                         </div>
                         <div>
                             <h3 class="text-center">{{ __('Grade') }} 10</h3>
                             <span>71,391 {{ __('Class') }}</span>
                         </div>
                     </a>
                 </div>


                 <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="0">
                     <a href="#" class="category d-flex align-items-start h-100">
                         <div>
                             <i class="uil uil-schedule"></i>
                         </div>
                         <div>
                             <h3 class="text-center">{{ __('Grade') }} 11</h3>
                             <span>71,391 {{ __('Class') }}</span>
                         </div>
                     </a>
                 </div>
                 <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="100">
                     <a href="#" class="category d-flex align-items-start h-100">
                         <div>
                             <i class="uil uil-schedule"></i>
                         </div>
                         <div>
                             <h3 class="text-center">{{ __('Grade') }} 12</h3>
                             <span>71,391 {{ __('Class') }}</span>
                         </div>
                     </a>
                 </div> --}}
             </div>
         </div>
     </div>

     <div class="services-section">
         <div class="container">
             <div class="row justify-content-between">
                 <div class="col-lg-4 mb-5 mb-lg-0">
                     <div class="section-title mb-3" data-aos="fade-up" data-aos-delay="0">
                         <h2 class="line-bottom mb-4">{{ __('Vision') }}</h2>
                     </div>
                     <p data-aos="fade-up" data-aos-delay="100">
                         {!! $vision !!}
                     </p>
                 </div>
                 <div class="col-lg-6" data-aos="fade-up" data-aos-delay="0" style="margin-top: 90px">
                     @php
                         if (!empty($vission_image_names) && file_exists(public_path('upload/settings/' . $vission_image_names))) {
                             $image_url = asset('/upload/settings/' . rawurlencode($vission_image_names));
                         } else {
                             $image_url = asset('images/default.svg');
                         }
                     @endphp
                     <figure class="img-wrap-2">
                         <img src="{{ $image_url }}" alt="Image" class="img-fluid">
                         <div class="dotted"></div>
                     </figure>
                 </div>
             </div>
         </div>
     </div>



     <div class="untree_co-section">
         <div class="container">
             <div class="row justify-content-between school-ac">
                 <div class="col-lg-6" data-aos="fade-up" data-aos-delay="400">
                     @php
                         if (!empty($active_image_names) && file_exists(public_path('upload/settings/' . $active_image_names))) {
                             $image_url = asset('/upload/settings/' . rawurlencode($active_image_names));
                         } else {
                             $image_url = asset('images/default.svg');
                         }
                     @endphp
                     <a href="{{ $url }}" data-fancybox="" class="video-wrap">
                         <img src="{{ $image_url }}" alt="Image" class="img-fluid rounded">
                         <span class="play-wrap mt-3"><span class="icon-play"></span></span>
                     </a>
                 </div>

                 <div class="col-lg-5 mb-5">
                     <h2 class="line-bottom mb-4" data-aos="fade-up" data-aos-delay="0" style="text-align: center;">
                         {{ __('Student Activity') }}</h2>

                     <p data-aos="fade-up" data-aos-delay="100">{{ $desc }}</p>
                 </div>

             </div>
         </div>
     </div><!-- /.untree_co-section -->

     <div class="untree_co-section">
         <div class="container">
             <div class="row justify-content-between">
                 <div class="col-lg-5 mb-5">
                     <h2 class="line-bottom mb-4" data-aos="fade-up" data-aos-delay="0">{{ __('About Us') }}</h2>
                     <p data-aos="fade-up" data-aos-delay="100">{!! $about_us !!}</p>

                     <div class="row count-numbers mb-5">
                         <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="0">
                             <span class="counter d-block"><span data-number="{{ $timetableCount }}">{{ $timetableCount }}</span></span>
                             <span class="caption-2">{{ __('No. Timetable') }}</span>
                         </div>
                         <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                             <span class="counter d-block"><span
                                     data-number="{{ $teacherCount }}">{{ $teacherCount }}</span><span></span></span>
                             <span class="caption-2">{{ __('No. Teachers') }}</span>
                         </div>
                         <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                             <span class="counter d-block"><span
                                     data-number="{{ $classRoomCount }}">{{ $classRoomCount }}</span><span></span></span>
                             <span class="caption-2">{{ __('No. Class Rooms') }}</span>
                         </div>

                     </div>


                 </div>
                 @php
                     if (!empty($about_image_names) && file_exists(public_path('upload/settings/' . $about_image_names))) {
                         $image_url = asset('/upload/settings/' . rawurlencode($about_image_names));
                     } else {
                         $image_url = asset('images/default.svg');
                     }
                 @endphp
                 <div class="col-lg-6" data-aos="fade-up" data-aos-delay="400">
                     <img src="{{ $image_url }}" alt="Image" class="img-fluid rounded">
                 </div>
             </div>
         </div>
     </div> <!-- /.untree_co-section -->

     <div class="untree_co-section bg-light">
         <div class="container">
             <div class="row justify-content-center mb-5">
                 <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="0">
                     <h2 class=" text-center mb-4">{{ __('School News') }}</h2>
                 </div>
             </div>

            {{-- <div class="row align-items-stretch">
                @foreach ($news as $item)
                    <div class="col-lg-6 mb-2" data-aos="fade-up" data-aos-delay="100" >
                        <div class="media-h d-flex h-100">
                            <figure>
                                <img src="{{ asset('upload/news/' . $item->image) }}" alt="Image">
                            </figure>
                            <div class="media-h-body">
                                <h2 class="mb-3" id="postTitle_{{ $loop->index }}"><a href="{{ route('front.newsdetail', $item->id) }}">{{ \Illuminate\Support\Str::limit($item->title, 30) }}</a></h2>
                                <div class="meta mb-2">
                                    <span class="icon-calendar mr-2"></span>
                                    <span>{{ $item->created_at->format('F d, Y H:i') }}</span>
                                    <span class="icon-person mr-2"></span>
                                    {{ $item->creator ? $item->creator->name : 'Unknown User' }}
                                </div>

                                <p id="postContent_{{ $loop->index }}">{!! \Illuminate\Support\Str::limit($item->description, 50) !!}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> --}}
             <div class="row align-items-stretch">
                 @foreach ($news as $item)
                     <div class="col-lg-6 mb-2" data-aos="fade-up" data-aos-delay="100">

                         <div class="media-h d-flex h-100">
                             <figure>
                                 <img src="{{ asset('upload/news/' . $item->image) }}" alt="Image">
                             </figure>
                             <div class="media-h-body">
                                 <h2 class="mb-3" id="postTitle_{{ $loop->index }}"><a
                                         href="{{ route('front.newsdetail', $item->id) }}">{{ \Illuminate\Support\Str::limit($item->title, 30) }}</a>
                                 </h2>
                                 <div class="meta mb-2">
                                     <span class="icon-calendar mr-2"></span>
                                     <span>{{ $item->created_at->format('F d, Y H:i') }}</span>
                                     <span class="icon-person mr-2"></span>
                                     {{ $item->creator ? $item->creator->name : 'Unknown User' }}
                                 </div>
                                 <p id="postContent_{{ $loop->index }}">{!! \Illuminate\Support\Str::limit($item->description, 50) !!}</p>
                                 <p class="mb-0" data-aos="fade-up" data-aos-delay="300"><a
                                         href="{{ route('front.newsdetail', $item->id) }}"
                                         class="btn btn-secondary">{{ __('Read More') }}</a></p>
                             </div>
                         </div>
                     </div>
                 @endforeach
             </div>
             <div class="row mt-5">
                 <div class="col-12 text-center">
                     <p class="mb-0" data-aos="fade-up" data-aos-delay="300"><a href="{{ route('front.news') }}"
                             class="btn btn-secondary">{{ __('Show More News') }}</a></p>
                 </div>
             </div>

         </div>
     </div>
     {{-- <div class="untree_co-section bg-light">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 text-center mx-auto">
                     <h3 class="text-center mb-4">{{ __('Testimonials') }}</h3>

                     <div class="owl-carousel wide-slider-testimonial">
                         <div class="item">
                             <blockquote class="block-testimonial">

                                 <p>
                                     &ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                     incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                     exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&rdquo;</p>
                                 <div class="author">
                                     <img src="{{ asset('front/images/person_1.jpg') }}"
                                         alt="Free template by TemplateUX">
                                     <h3>John Doe</h3>
                                     <p class="position">CEO, Founder</p>
                                 </div>
                             </blockquote>
                         </div>

                         <div class="item">
                             <blockquote class="block-testimonial">
                                 <p>
                                     &ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                     incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                     exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&rdquo;</p>
                                 <div class="author">
                                     <img src="{{ asset('front/images/person_2.jpg') }}"
                                         alt="Free template by TemplateUX">
                                     <h3>James Woodland</h3>
                                     <p class="position">Designer at Facebook</p>
                                 </div>
                             </blockquote>
                         </div>

                         <div class="item">
                             <blockquote class="block-testimonial">

                                 <p>
                                     &ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                     incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                     exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&rdquo;</p>
                                 <div class="author">
                                     <img src="{{ asset('front/images/person_3.jpg') }}"
                                         alt="Free template by TemplateUX">
                                     <h3>Rob Smith</h3>
                                     <p class="position">Product Designer at Twitter</p>
                                 </div>
                             </blockquote>
                         </div>
                     </div>

                 </div>
             </div>
         </div>
     </div> --}}

     <div class="untree_co-section">
         <div class="container">
             <div class="col-lg-12">
                 <div class="widget">
                     <h3 class="text-center mb-4">{{ __('Gallery') }}</h3>

                     <ul class="instafeed instagram-gallery list-unstyled">
                         @foreach ($gallery as $image)
                             <li>
                                 <a class="instagram-item" href="{{ asset('/upload/gallery/' . $image->image) }}"
                                     data-fancybox="gal">
                                     <img src="{{ asset('upload/gallery/' . $image->image) }}" alt=""
                                         width="100%" height="100%" style="object-fit: cover">
                                 </a>
                             </li>
                         @endforeach
                     </ul>
                 </div>
             </div>
         </div>
     </div>
     <div class="untree_co-section">
         <div class="container">
             <h3 class="text-center mb-4">{{ __('Contact Us') }}</h3>
             <div class="row mb-5" style="justify-content: center">
                 <div class="col-lg-4 mb-5 order-2 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
                     <div class="contact-info">

                         <div class="address mt-4">
                             <i class="icon-room "style="background-color: #009D8A"></i>
                             <h4 class="mb-2">{{ __('Location:') }}</h4>
                             <p>{{ $address }}</p>
                         </div>

                         <div class="open-hours mt-4">
                             <i class="icon-clock-o"style="background-color: #009D8A"></i>
                             <h4 class="mb-2">{{ __('Open Hours:') }}</h4>
                             <p>
                                 {{ __('Monday-Saturday:') }}<br>
                                 {{__('7:00 AM - 17:00 PM')}}
                             </p>
                         </div>

                         <div class="email mt-4">
                             <i class="icon-envelope" style="background-color: #009D8A"></i>
                             <h4 class="mb-2">{{ __('Email:') }}</h4>
                             <p>{{ $email }}</p>
                         </div>

                         <div class="phone mt-4">
                             <i class="icon-phone"style="background-color: #009D8A"></i>
                             <h4 class="mb-2">{{ __('Call:') }}</h4>
                             <p>{{ $phone_number }}</p>
                             <p>{{ $phone_number1 }}</p>
                         </div>

                     </div>
                 </div>
                 <div class="col-lg-7 mr-auto order-1 d-none" data-aos="fade-up" data-aos-delay="200">
                     <form action="#">
                         <div class="row">
                             <div class="col-6 mb-3">
                                 <input type="text" class="form-control" placeholder="{{__('Your Name')}}">
                             </div>
                             <div class="col-6 mb-3">
                                 <input type="email" class="form-control" placeholder="{{__('Your Email')}}">
                             </div>
                             <div class="col-12 mb-3">
                                 <input type="text" class="form-control" placeholder="{{__('Subject ')}}">
                             </div>
                             <div class="col-12 mb-3">
                                 <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="{{__('Message')}}"></textarea>
                             </div>

                             <div class="col-12">
                                 <input type="submit" value="{{__('Send Message')}}" class="btn btn-secondary">
                             </div>
                         </div>
                     </form>
                 </div>
             </div>


         </div>
     </div>
 @endsection

 @push('script')
 <script>
     const contentMaxLength = 50; // Set the maximum length of the content
     const titleMaxLength = 30; // Set the maximum length of the title

     const contents = document.querySelectorAll('[id^="postContent"]');
     const titles = document.querySelectorAll('[id^="postTitle"]');

     contents.forEach((content, index) => {
         const title = titles[index];

         if (content.textContent.length > contentMaxLength) {
             const truncatedContent = content.textContent.substring(0, contentMaxLength);
             content.textContent = truncatedContent + '... ';

             const readMoreContentLink = document.createElement('a');
             readMoreContentLink.textContent = 'Read more';
             readMoreContentLink.href = '/front/news'; // Replace '/front/news' with your appropriate route
             readMoreContentLink.onclick = function(e) {
                 e.preventDefault();
                 content.textContent = $news[index].description; // Replace with the full content
                 readMoreContentLink.style.display = 'none';
             };

             content.insertAdjacentElement('beforeend', readMoreContentLink);
         }

         if (title.textContent.length > titleMaxLength) {
             const truncatedTitle = title.textContent.substring(0, titleMaxLength);
             title.textContent = truncatedTitle + '... ';

             const readMoreTitleLink = document.createElement('a');
             // readMoreTitleLink.textContent = 'Read more';
             readMoreTitleLink.href = '/front/news'; // Replace '/front/news' with your appropriate route
             readMoreTitleLink.onclick = function(e) {
                 e.preventDefault();
                 title.textContent = $news[index].title; // Replace with the full title
                 readMoreTitleLink.style.display = 'none';
             };

             title.insertAdjacentElement('beforeend', readMoreTitleLink);
         }
     });
 </script>
@endpush
