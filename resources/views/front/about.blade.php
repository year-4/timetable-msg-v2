@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-hero,
            .untree_co-hero>.container>.row,
            .bg-img,
            .bg-img>.container>.row {
                height: 50vh;
                min-height: 409px;
            }

            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }
                .banner-text{
                    font-size: 20px;
                 }
            }
        </style>
    @endpush
    <div class="untree_co-hero overlay" style="background-image: url('{{ asset('front/images/10mkp2.jpg') }}');">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h1 class="mb-4 banner-text text-white" data-aos="fade-up" data-aos-delay="100">
                                {{ __('About Us') }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

    <div class="services-section">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-5 mb-5">
                    <h2 class="line-bottom mb-4" data-aos="fade-up" data-aos-delay="0">{{ __('About Us') }}</h2>
                    <p data-aos="fade-up" data-aos-delay="100">{!! $about_us !!}</p>

                    <div class="row count-numbers mb-5">
                        <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="0">
                            <span class="counter d-block"><span data-number="{{ $timetableCount }}">{{ $timetableCount }}</span><span>+</span></span>
                            <span class="caption-2">{{ __('No. Timetable') }}</span>
                        </div>
                        <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                            <span class="counter d-block"><span
                                    data-number="{{ $teacherCount }}">{{ $teacherCount }}</span><span></span></span>
                            <span class="caption-2">{{ __('No. Teachers') }}</span>
                        </div>
                        <div class="col-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                            <span class="counter d-block"><span
                                    data-number="{{ $classRoomCount }}">{{ $classRoomCount }}</span><span></span></span>
                            <span class="caption-2">{{ __('No. Class Rooms') }}</span>
                        </div>

                    </div>
                </div>
                @php
                    if (!empty($about_image_names) && file_exists(public_path('upload/settings/' . $about_image_names))) {
                        $image_url = asset('/upload/settings/' . rawurlencode($about_image_names));
                    } else {
                        $image_url = asset('images/default.svg');
                    }
                @endphp
                <div class="col-lg-6" data-aos="fade-up" data-aos-delay="400">
                    <img src="{{ $image_url }}" alt="Image" class="img-fluid rounded">
                </div>
            </div>
        </div>
    </div>
@endsection
