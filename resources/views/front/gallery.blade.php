@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-hero,
            .untree_co-hero>.container>.row,
            .bg-img,
            .bg-img>.container>.row {
                height: 50vh;
                min-height: 409px;
            }

            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }
                .banner-text{
                    font-size: 20px;
                 }
            }
        </style>
    @endpush
    <div class="untree_co-hero overlay" style="background-image: url('{{ asset('front/images/10mkp2.jpg') }}');">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h1 class="mb-4 banner-text text-white" data-aos="fade-up" data-aos-delay="100">
                                {{ __('Gallery') }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <div class="untree_co-section">
        <div class="container">
            <div class="col-lg-12">
                <div class="widget">
                    <h3 class="text-center">{{ __('Gallery') }}</h3>
                    <ul class="instafeed instagram-gallery list-unstyled">

                        @foreach ($gallery as $image)
                            <li>
                                <a class="instagram-item" href="{{ asset('/upload/gallery/' . $image->image) }}"
                                    data-fancybox="gal">
                                    <img src="{{ asset('upload/gallery/' . $image->image) }}" alt="" width="100%"
                                        height="100%" style="object-fit: cover">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div> <!-- /.widget -->
            </div> <!-- /.col-lg-3 -->
        </div>
    </div>
@endsection
