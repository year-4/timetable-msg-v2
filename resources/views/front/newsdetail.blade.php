@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-hero,
            .untree_co-hero>.container>.row,
            .bg-img,
            .bg-img>.container>.row {
                height: 50vh;
                min-height: 409px;
            }

            .decription img {
                width: 100% !important;
                margin-bottom: 12px;

            }

            .decription {
                background-color: transparent !important;
            }

            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }
                .banner-text {
                    font-size: 20px;
                }
                .media-h .media-h-body h2 {
                    font-size: 15px;
                }
            }
        </style>
    @endpush
    <div class="untree_co-hero overlay" style="background-image: url('{{ asset('front/images/10mkp2.jpg') }}'); height:10vh;">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h1 class="mb-4 banner-text text-white" data-aos="fade-up" data-aos-delay="100">
                                {{ __('School News') }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <div class="untree_co-section">
        <div class="container">
            <div class="justify-content-between">
                <h5 class="">{{ $newsdetail->title }}</h5>
                <div class="meta mb-2"><span
                        class="icon-calendar mr-2"></span><span>{{ $newsdetail->created_at->format('F d, Y H:i') }}</span>
                    <span
                        class="icon-person mr-2"></span>{{ $newsdetail->creator ? $newsdetail->creator->name : 'Unknown User' }}
                </div>
                <div class="col-lg-12" data-aos="fade-up" data-aos-delay="400" style="margin-bottom:30px">
                    @if ($newsdetail->image)
                        <img src="{{ asset('upload/news/' . $newsdetail->image) }}" alt="News Image"
                            style="max-width: 100%;">
                    @else
                        No image
                    @endif
                </div>
                <div class="col-lg-12 mb-5 decription" style=" ">
                    <p data-aos="fade-up" class="" data-aos-delay="100"> {!! $newsdetail->description !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="untree_co-section bg-light">
        <div class="container">
            <h3 class="text-center" style="margin-bottom: 40px">{{ __('Relate News') }}</h3>
            <div class="row align-items-stretch">
                @foreach ($news as $item)
                    <div class="col-lg-6 mb-2" data-aos="fade-up" data-aos-delay="100">

                        <div class="media-h d-flex h-100">
                            <figure>
                                <img src="{{ asset('upload/news/' . $item->image) }}" alt="Image">
                            </figure>
                            <div class="media-h-body">
                                <h2 class="mb-3" id="postTitle_{{ $loop->index }}"><a
                                        href="{{ route('front.newsdetail', $item->id) }}">{{ \Illuminate\Support\Str::limit($item->title, 30) }}</a>
                                </h2>
                                <div class="meta mb-2">
                                    <span class="icon-calendar mr-2"></span>
                                    <span>{{ $item->created_at->format('F d, Y H:i') }}</span>
                                    <span class="icon-person mr-2"></span>
                                    {{ $item->creator ? $item->creator->name : 'Unknown User' }}
                                </div>
                                <p id="postContent_{{ $loop->index }}">{!! \Illuminate\Support\Str::limit($item->description, 50) !!}</p>
                                <p class="mb-0" data-aos="fade-up" data-aos-delay="300"><a
                                        href="{{ route('front.newsdetail', $item->id) }}"
                                        class="btn btn-secondary">{{ __('Read More') }}</a></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div> <!-- /.untree_co-section -->
@endsection

@push('script')
    <script>
        const contentMaxLength = 50; // Set the maximum length of the content
        const titleMaxLength = 30; // Set the maximum length of the title

        const contents = document.querySelectorAll('[id^="postContent"]');
        const titles = document.querySelectorAll('[id^="postTitle"]');

        contents.forEach((content, index) => {
            const title = titles[index];

            if (content.textContent.length > contentMaxLength) {
                const truncatedContent = content.textContent.substring(0, contentMaxLength);
                content.textContent = truncatedContent + '... ';

                const readMoreContentLink = document.createElement('a');
                readMoreContentLink.textContent = 'Read more';
                readMoreContentLink.href = '/front/news'; // Replace '/front/news' with your appropriate route
                readMoreContentLink.onclick = function(e) {
                    e.preventDefault();
                    content.textContent = $news[index].description; // Replace with the full content
                    readMoreContentLink.style.display = 'none';
                };

                content.insertAdjacentElement('beforeend', readMoreContentLink);
            }

            if (title.textContent.length > titleMaxLength) {
                const truncatedTitle = title.textContent.substring(0, titleMaxLength);
                title.textContent = truncatedTitle + '... ';

                const readMoreTitleLink = document.createElement('a');
                // readMoreTitleLink.textContent = 'Read more';
                readMoreTitleLink.href = '/front/news'; // Replace '/front/news' with your appropriate route
                readMoreTitleLink.onclick = function(e) {
                    e.preventDefault();
                    title.textContent = $news[index].title; // Replace with the full title
                    readMoreTitleLink.style.display = 'none';
                };

                title.insertAdjacentElement('beforeend', readMoreTitleLink);
            }
        });
    </script>
@endpush
