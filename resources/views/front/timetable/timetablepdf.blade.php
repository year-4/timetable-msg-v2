<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{!! $app_name !!}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Kantumruy+Pro:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Display+Playfair:wght@400;700&family=Inter:wght@400;700&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Moul&display=swap" rel="stylesheet">

    <style>
        @page {
            size: A4 landscape;
        }

        body {
            font-family: 'Kantumruy Pro', sans-serif;
            margin: 0;
            padding: 0;
        }

        .header {
            margin-top: -40px;
            margin-bottom: 100px;
        }

        .appname {
            text-align: center;
            margin-bottom: 10px;
        }

        .teacher-info {
            margin-bottom: 0px;
        }
        .timetable {
            width: 100%;
            border-collapse: collapse;
            /*box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);*/
        }

        .timetable th,
        .timetable td {
            border: 1px solid black; /* Updated to black border */
            padding: 5px;
            text-align: center;
            font-size: 12px;
        }

        .timetable th {
            color: black;
        }

        .alignleft {
            float: left;
            font-size: 12px;
            margin-bottom: 2px;
        }

        .alignright {
            float: right;
            font-size: 12px;
             margin-bottom: 2px;
        }
       
    </style>

</head>

<body>
    <div class="header">
        {{-- <img src="data:image/png;base64,'.base64_encode(file_get_contents($imagePath)).'" width="40px" alt="Logo"> --}}
        <h1 class="appname">{!! $app_name !!}</h1>
        <div class="teacher-info">
            <div class="alignleft">
                <p>{{ __('Class') }}: {{ $timetable->classroom->name }}</p>
            
                <p>{{ __('Academic Year') }}: {{ session('current_academic_year')->first()->name }}</p>
            </div>
        </div>
        <div class="teacher-info">
            <div class="alignright">
                <p>{{ __('Teacher') }}: {{ $timetable->classroom->teacher->full_name }}</p>
            
                <p>{{ __('Phone') }}: {{ $timetable->classroom->teacher->phone }}</p>
            </div>
        </div>
    </div>

    <table class="timetable">
        <thead>
            <tr>
                <th class="text-center">{{ __('Time') }}</th>
                @foreach ($week_days as $day)
                    <th class="text-center">{{ $day }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="8" class="text-center">{{ __('Morning') }}</th>
            </tr>
            @foreach ($time_slots as $time)
                @if ($time->daytime == 'morning')
                    <tr>
                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                        @foreach ($week_days as $day)
                            @php
                                $timetable_slots = $timetable_slots ?? null;
                                $timetable_slot = null;
                                if (@$timetable_slots != null) {
                                    $timetable_slot = $timetable_slots
                                        ->where('time_slot_id', $time->id)
                                        ->where('week_day', $day)
                                        ->first();
                                }
                            @endphp
                            <td class="text-center" id="{{ $day . '-' . $time->id }}">
                                @if ($timetable_slot)
                                    <span class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                    <br>
                                    {{ $timetable_slot->teacher->full_name }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endif
            @endforeach
            <tr>
                <th colspan="8" class="text-center">Evening</th>
            </tr>
            @foreach ($time_slots as $time)
                @if ($time->daytime == 'afternoon')
                    <tr>
                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                        @foreach ($week_days as $day)
                            @php
                                $timetable_slot = null;
                                if ($timetable_slots) {
                                    $timetable_slot = $timetable_slots
                                        ->where('time_slot_id', $time->id)
                                        ->where('week_day', $day)
                                        ->first();
                                }
                            @endphp
                            <td class="text-center" id="{{ $day . '-' . $time->id }}">
                                @if ($timetable_slot)
                                    <span class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                    <br>
                                    {{ $timetable_slot->teacher->full_name }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</body>

</html>
