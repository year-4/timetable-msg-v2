@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-hero,
            .untree_co-hero>.container>.row,
            .bg-img,
            .bg-img>.container>.row {
                height: 50vh;
                min-height: 409px;
            }


            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }
            }
        </style>
    @endpush
    @php
        if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
            $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
        } else {
            $image_url = asset('images/default.svg');
        }
    @endphp
    <div class="untree_co-hero overlay" style="background-image: url('{{ $image_url }}');">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h1 class="mb-4 heading text-white" data-aos="fade-up" data-aos-delay="100">
                                {{ __('Time Table') }}
                            </h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="untree_co-section">
        <div class="container">
            <div class="row justify-content-center mb-3">
                <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="0">
                    <h2 class="line-bottom text-center mb-4">{{ __('Timetable') }}</h2>
                </div>
            </div>
            <div class="row align-items-stretch">
                @foreach ($class_rooms as $room)
                    <div class="col-sm-6 col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="0">
                        <a href="{{ route('front.timetable.detail', $room->timetable->id) }}" class="category d-flex align-items-start h-100">
                            <div>
                                <i class="uil uil-schedule"></i>
                            </div>
                            <div>
                                <h3 class="text-center">{{ __('Class') }} {{ $room->name }}</h3>
                                {{-- <span>71,391 {{ __('Class') }}</span> --}}
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
