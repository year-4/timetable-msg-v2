@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            /* .untree_co-hero,
                    .untree_co-hero>.container>.row,
                    .bg-img,
                    .bg-img>.container>.row {
                        height: 50vh;
                        min-height: 409px;
                    } */


            @media (max-width: 767.98px) {

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 300px;
                }

                .table-responsive {
                    display: block;
                    width: 100%;
                    overflow-x: auto;
                    -webkit-overflow-scrolling: touch;
                    font-size: x-small;
                }
            }

            @page {
                size: A4 portrait;
                margin: 20mm 10mm;
            }

            @media print {
                body * {
                    visibility: hidden;
                }

                #contentToSave,
                #contentToSave * {
                    visibility: visible;
                }

                #contentToSave {
                    position: fixed !important;
                    left: 0;
                    top: 0;
                }

                .untree_co-hero,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    display: none;
                }

            }
        </style>
    @endpush
    @php
        if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
            $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
        } else {
            $image_url = asset('images/default.svg');
        }
    @endphp
    <div class="untree_co-hero overlay" style="background-image: url('{{ $image_url }}');">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h1 class="mb-4 heading text-white" data-aos="fade-up" data-aos-delay="100">
                                {{ __('Class') }}: {{ $timetable->classroom->name }}
                            </h1>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="untree_co-section pt-3">
        <div class="container">
            <div class="row btn-print">
                <div class="col-12">
                    <div class="text-right mt-4">

                        {{-- <a href="{{ route('timetable.timetablepdf', ['id' => $timetable->id]) }}" class="btn btn-danger mt-2 mb-2">{{__('Download PDF')}}</a> --}}
                        {{-- <a href="{{ route('front.timetable.detail', $room->timetable->id) }}" class="category d-flex align-items-start h-100"> --}}
                        <button onclick="printInvoice()" class="btn btn-secondary">
                            <i class="fa fa-print"></i>
                            {{ __('Download') }}
                        </button>

                    </div>
                </div>
            </div>
            <div class="card-body " id="contentToSave">
                <div class="row justify-content-center mb-3">
                    <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="0">
                        <img src="{{ asset('front/images/logo.png') }}" alt="MyApp Logo" style="width: 40px;">
                        <h3 class="text-center mb-4">{!! $app_name !!}</h3>
                    </div>
                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                        <span class=" mb-4">{{ __('Class') }}: {{ $timetable->classroom->name }}</span>
                    </div>
                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                        <span class="float-right mb-4 ">{{ __('Teacher') }}:
                            {{ $timetable->classroom->teacher->full_name }}</span>
                    </div>
                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                        <span class="mb-4 ">{{ __('Academic Year') }}: {{ $timetable->year->name }}</span>
                    </div>
                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                        <span class="mb-4 float-right ">{{ __('Phone Number') }}:
                            {{ $timetable->classroom->teacher->phone }}</span>
                    </div>

                </div>
                <div class="table-responsive">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">{{ __('Time') }}</th>
                                @foreach ($week_days as $day)
                                    <th class="text-center">{{ __($day) }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th colspan="8" class="text-center">{{ __('Morning') }}</th>
                            </tr>
                            @foreach ($time_slots as $time)
                                @if ($time->daytime == 'morning')
                                    <tr>
                                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                                        @foreach ($week_days as $day)
                                            @php
                                                $timetable_slots = $timetable_slots ?? null;
                                                $timetable_slot = null;
                                                if (@$timetable_slots != null) {
                                                    $timetable_slot = $timetable_slots
                                                        ->where('time_slot_id', $time->id)
                                                        ->where('week_day', $day)
                                                        ->first();
                                                }
                                            @endphp
                                            <td class="text-center" id="{{ $day . '-' . $time->id }}">

                                                @if ($timetable_slot)
                                                    <span class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                                    <br>
                                                    {{ $timetable_slot->teacher->full_name }}
                                                    {{-- <a class="btn btn-edit-timetable-slot btn-outline-primary btn-modal d-none"
                                                        href="#"
                                                        data-href="{{ trim(route('admin.timetable.showPopupEdit', $timetable_slot->id)) }}"
                                                        data-toggle="modal" data-container=".modal_form">
                                                        <span>
                                                            {{ __('Edit') }}
                                                        </span>
                                                    </a> --}}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <th colspan="8" class="text-center">{{ __('Evening') }}</th>
                            </tr>
                            @foreach ($time_slots as $time)
                                @if ($time->daytime == 'afternoon')
                                    <tr>
                                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                                        @foreach ($week_days as $day)
                                            @php
                                                $timetable_slot = null;
                                                if ($timetable_slots) {
                                                    $timetable_slot = $timetable_slots
                                                        ->where('time_slot_id', $time->id)
                                                        ->where('week_day', $day)
                                                        ->first();
                                                }
                                            @endphp
                                            <td class="text-center" id="{{ $day . '-' . $time->id }}">

                                                @if ($timetable_slot)
                                                    <span class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                                    <br>
                                                    {{ $timetable_slot->teacher->full_name }}
                                                    {{-- <a class="btn btn-edit-timetable-slot btn-outline-primary btn-modal d-none"
                                                        href="#"
                                                        data-href="{{ trim(route('admin.timetable.showPopupEdit', $timetable_slot->id)) }}"
                                                        data-toggle="modal" data-container=".modal_form">
                                                        <span>
                                                            {{ __('Edit') }}
                                                        </span>
                                                    </a> --}}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function printInvoice() {
            window.print();
        }
    </script>
@endpush
