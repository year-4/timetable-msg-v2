@push('style')
    <style>
        @media only screen and (max-width: 430px) {
            .logo {
                display: none !important;
                font-size: 1px;
            }
        }
    </style>
@endpush

<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
            <span class="icofont-close js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>

<nav class="site-nav mb-5">
    <div class="pb-2 top-bar mb-3">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-6 col-lg-9">
                    <a href="{{ route('front.contact') }}" class="small mr-3">
                        <span class="icon-question-circle-o mr-2"></span>
                        <span class="d-none d-lg-inline-block">{{ __('Have a question?') }}</span>
                    </a>
                    <a href="#" class="small mr-3">
                        <span class="icon-phone mr-2"></span>
                        <span class="d-none d-lg-inline-block">{{ $phone_number }}</span>
                    </a>
                    <a href="#" class="small mr-3">
                        <span class="icon-envelope mr-2"></span>
                        <span class="d-none d-lg-inline-block">{{ $email }}</span>
                    </a>
                </div>

                <div class="col-6 col-lg-3 text-right">
                    <a href="{{ route('login') }}" class="small mr-3">
                        <span class="icon-lock"></span>
                        {{ __('Login') }}
                    </a>
                    {{-- <a href="{{ url('register') }}"class="small">
                        <span class="icon-person"></span>
                        {{ __('Register') }}
                    </a> --}}
                </div>

            </div>
        </div>
    </div>
    <div class="sticky-nav js-sticky-header">
        <div class="container position-relative">
            <div class="site-navigation text-center">
                <a href="{{ route('front.index') }}"class="logo menu-absolute m-0" style="font-size: 20px">
                    <img src="{{ asset('front/images/logo.png') }}" alt="MyApp Logo" style="width: 40px;">
                    <span class="app_name">{!! $app_name !!}</span>
                </a>
                <ul class="js-clone-nav d-none d-lg-inline-block site-menu">
                    <li class="active"><a href="{{ route('front.index') }}">{{ __('Home') }}</a></li>

                    <li><a href="{{ route('front.news') }}">{{ __('School News') }}</a></li>
                    <li><a href="{{ route('front.gallery') }}">{{ __('Gallery') }}</a></li>
                    <li><a href="{{ route('front.about') }}">{{ __('About') }}</a></li>
                    <li><a href="{{ route('front.contact') }}">{{ __('Contact') }}</a></li>
                    <div class="nav-item dropdown d-inline-block float-right">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="flag-icon flag-icon-{{ $current_locale == 'en' ? 'gb' : $current_locale }}"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-0">
                            @foreach ($available_locales as $locale_name => $available_locale)
                                @if ($available_locale === $current_locale)
                                    <a href="{{ route('change_language', $available_locale) }}"
                                        class="dropdown-item active">
                                        <i
                                            class="flag-icon flag-icon-{{ $available_locale == 'en' ? 'gb' : $available_locale }} mr-2"></i>
                                        {{ $locale_name }}
                                    </a>
                                @else
                                    <a href="{{ route('change_language', $available_locale) }}" class="dropdown-item">
                                        <i
                                            class="flag-icon flag-icon-{{ $available_locale == 'en' ? 'gb' : $available_locale }} mr-2"></i>
                                        {{ $locale_name }}
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </ul>

                <a href="#"
                    class="burger ml-auto float-right site-menu-toggle js-menu-toggle d-inline-block d-lg-none light"
                    data-toggle="collapse" data-target="#main-navbar">
                    <span></span>
                </a>

            </div>
        </div>
    </div>
</nav>
