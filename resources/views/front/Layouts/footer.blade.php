<div class="site-footer d-print-none">
        <h5 href="#" class="text-center" style="margin-bottom: 1cm">
            <img src="{{ asset('front/images/logo.png') }}" alt="MyApp Logo" class="img-fluid" style="width: 70px;">
            {{$app_name}}
        </h5>
    <div class="container">
        
        <div class="row">
            <div class="col-lg-3 mr-auto">
                {{-- <div class="widget">
                    <h3>{{ __('About Us') }}<span class="text-primary">.</span> </h3>
                    <p>{!! $about_us !!}</p>
                </div> <!-- /.widget --> --}}
                <div class="widget">
                    <h3>{{ __('Social Media') }}</h3>
                    <ul class="list-unstyled social">
                        <li><a href="{{ $facebook }}"><span class="icon-facebook"></span></a></li>
                        <li><a href="{{ $telegram }}"><span class="icon-telegram"></span></a></li>
                    </ul>
                </div> <!-- /.widget -->
            </div> <!-- /.col-lg-3 -->

            <div class="col-lg-2 ml-auto">
                <div class="widget">
                    <h3>{{ __('Location') }}</h3>
                    <ul class="list-unstyled float-left links">
                        <li>
                            <p>{{ $address }}</p>
                        </li>
                    </ul>
                </div> <!-- /.widget -->
            </div> <!-- /.col-lg-3 -->


            <div class="col-lg-3">
                <div class="widget">
                    <h3>{{ __('Open Hour') }}</h3>
                    <ul class="list-unstyled links mb-4">
                        <li>
                            <p>{{ __('Monday-Saturday:') }}<br>
                                {{__('7:00 AM - 17:00 PM')}}
                            </p>
                        </li>
                    </ul>
                </div> <!-- /.widget -->
            </div> <!-- /.col-lg-3 -->

            <div class="col-lg-3">
                <div class="widget">
                    <h3>{{ __('Contact') }}</h3>

                    <ul class="list-unstyled links mb-4">
                        <li>
                            <p>{{ $phone_number }}</p>
                        </li>
                        <li>
                            <p>{{ $phone_number1 }}</p>
                        </li>
                    </ul>
                </div> <!-- /.widget -->
            </div> <!-- /.col-lg-3 -->

        </div> <!-- /.row -->

        <div class="row mt-5">
            <div class="col-12 text-center">
                <strong>{{ $copyright }}</strong>
            </div>
        </div>
    </div> <!-- /.container -->
</div> <!-- /.site-footer -->
