@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>{{ __('Time Slot') }}</h3>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <a class="btn btn-primary btn-modal" href="#" data-href="{{ route('admin.time_slot.create') }}" data-toggle="modal" data-container=".modal_form">
                        <i class=" fa fa-plus-circle"></i>
                        {{ __('Add New') }}
                    </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('_message')
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Time Slot List') }}</h3>
                            <span class="badge bg-warning total-count">{{ $timeslots->total() }}</span>
                        </div>
                        <!-- /.card-header -->

                        {{-- table --}}
                        @include('admin.time_slot.partials._table')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>
</div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.btn-modal', function(){
            $("div.modal_form").load($(this).data('href'), function(){

                $(this).modal('show');

            });
        });

        $(document).on('click', '.btn-edit', function(){
            $("div.modal_form").load($(this).data('href'), function(){

                $(this).modal('show');

            });
        });

        $('.submit').click(function (e) {
            e.preventDefault();
            // alert('ok');
            // console.log('ok');
        });

        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function (response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            });
        });
    </script>
@endpush
