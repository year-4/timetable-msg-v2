{{-- <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Add Time Slot') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.time_slot.store') }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf

                <div class="form-group">
                    <label for="time">{{ __('Start Time') }}</label>
                    <div class="input-group date" id="timepicker">
                        <input type="time"  name="time" class="form-control">

                    </div>
                </div>
                <div class="form-group">
                    <label for="time">{{ __('End Time') }}</label>
                    <div class="input-group date" id="timepicker">
                        <input type="time"  name="time" class="form-control">

                    </div>
                </div>
                <div class="form-group">
                    <label for="order">{{ __('Order') }}</label>
                    <input type="text" name="oder" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('.submit').click(function(e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({

            type: "post",
            url: `{{ route('admin.time_slot.store') }}`,
            data: data,

            success: function(response) {
                console.log(response)
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Created successfully') }}`,

                    });
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);
                }

            }
        });
    });
</script> --}}
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Add Time Slot') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.time_slot.store') }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="start_time">{{ __('Start Time') }}</label>
                    <input type="time" name="start_time" class="form-control">
                </div>
                <div class="form-group">
                    <label for="end_time">{{ __('End Time') }}</label>
                    <input type="time" name="end_time" class="form-control">
                </div>
                {{-- <div class="form-group">
                    <label for="order">{{ __('Order') }}</label>
                    <input type="text" name="order" class="form-control">
                </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

{{-- @push('script') --}}
<script>
    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({

            type: "post",
            url: `{{ route('admin.time_slot.store') }}`,
            data: data,
           // dataType: "json",
            success: function (response) {
                console.log(response)
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Created successfully') }}`,
                        // message: ,
                    });
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);
                }

            }
        });
    });
</script>
{{-- @endpush --}}

