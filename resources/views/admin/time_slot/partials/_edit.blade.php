{{-- <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Edit Time Slot') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.time_slot.update', $timeslot->id) }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="start_time">{{ __('Start Time') }}</label>
                    <input type="time" name="start_time" class="form-control">
                </div>
                <div class="form-group">
                    <label for="end_time">{{ __('End Time') }}</label>
                    <input type="time" name="end_time" class="form-control">
                </div>
                <div class="form-group">
                    <label for="order">{{ __('Order') }}</label>
                    <input type="text"alue="{{ $timeslot->order }}" name="oder" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({

            type: "post",
            url: `{{ route('admin.time_slot.update', $timeslot->id) }}`,
            data: data,

            success: function (response) {
                console.log(response);
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);

                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Updated successfully') }}`
                    });
                }

            }
        });
    });
</script> --}}



<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Edit Time Slot') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.time_slot.update', $timeslot->id) }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="start_time">{{ __('Start Time') }}</label>
                    <input type="time" value="{{ $timeslot->start_time }}" name="start_time" class="form-control">
                </div>
                <div class="form-group">
                    <label for="end_time">{{ __('End Time') }}</label>
                    <input type="time" value="{{ $timeslot->end_time }}" name="end_time" class="form-control">
                </div>
                {{-- <div class="form-group">
                    <label for="order">{{ __('Order') }}</label>
                    <input type="text" value="{{ $timeslot->order }}" name="order" class="form-control">
                </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('.submit-form').submit(function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "post",
            url: `{{ route('admin.time_slot.update', $timeslot->id) }}`,
            data: data,
            // dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);

                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Updated successfully') }}`
                    });
                }
            }
        });
    });
</script>
