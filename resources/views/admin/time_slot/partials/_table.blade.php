<div class="card-body p-0 table-wrapper">
    <table class="table table-striped">
        <thead>
            <tr>
                {{-- <th >#</th> --}}
                <th>{{ __('Start Time') }}</th>
                <th>{{ __('End Time') }}</th>
                <th>{{ __('Created date') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($timeslots as $timeslot)
                <tr>
                    {{-- <td>{{$grade->id}}</td> --}}
                    <td>{{ $timeslot->start_time }}</td>
                    <td>{{ $timeslot->end_time }}</td>
                    <td>{{ $timeslot->created_at->format('d M Y h:i A') }}</td>
                    <td>
                        @if (auth()->user()->can('time_slot.edit'))
                            <a href="#" data-href="{{ route('admin.time_slot.edit', $timeslot->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-sm btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        @endif
                        @if (auth()->user()->can('time_slot.delete'))
                            <form action="{{ route('admin.time_slot.destroy', $timeslot->id) }}"
                                class="d-inline-block form-delete-{{ $timeslot->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $timeslot->id }}"
                                    data-href="{{ route('admin.time_slot.destroy', $timeslot->id) }}"
                                    class="btn btn-danger btn-sm btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- <div style="padding: 10px; float: right;">

        {{ $grades->links() }}
    </div> --}}

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $timeslots->firstItem() }} {{ __('to') }}
                    {{ $timeslots->lastItem() }} {{ __('of') }} {{ $timeslots->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $timeslots->links() }}</div>
            </div>
        </div>
    </div>


</div>
