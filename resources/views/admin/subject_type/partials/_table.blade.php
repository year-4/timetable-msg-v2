<div class="card-body p-0 table-wrapper">
    <table class="table">
        <thead>
            <tr>
                {{-- <th >#</th> --}}
                <th>{{ __('Name') }}</th>
                {{-- <th>Created by</th> --}}
                <th>{{ __('Created date') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($subjectTypes as $subject_type)
                <tr>
                    {{-- <td>{{$grade->id}}</td> --}}
                    <td>{{ $subject_type->name }}</td>
                    <td>{{ $subject_type->created_at->format('d M Y h:i A') }}</td>
                    <td>
                        @if (auth()->user()->can('subject_type.edit'))
                            <a href="#" data-href="{{ route('admin.subject_type.edit', $subject_type->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-sm btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        @endif
                        @if (auth()->user()->can('subject_type.delete'))
                            <form action="{{ route('admin.subject_type.destroy', $subject_type->id) }}"
                                class="d-inline-block form-delete-{{ $subject_type->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $subject_type->id }}"
                                    data-href="{{ route('admin.subject_type.destroy', $subject_type->id) }}"
                                    class="btn btn-danger btn-sm btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- <div style="padding: 10px; float: right;">

        {{ $grades->links() }}
    </div> --}}

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $subjectTypes->firstItem() }} {{ __('to') }}
                    {{ $subjectTypes->lastItem() }} {{ __('of') }} {{ $subjectTypes->total() }}
                    {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $subjectTypes->links() }}</div>
            </div>
        </div>
    </div>


</div>
