<div class="card-body p-0 table-responsive teacher-table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Gender') }}</th>
                <th>{{ __('Code') }}</th>
                <th class="text-nowrap">{{ __('Type') }}</th>
                <th>{{ __('Class') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($teachers as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->full_name }}</td>
                    <td>
                        {{ $value->gender == 1 ? __('Male') : __('Female') }}
                    </td>
                    <td>{{ $value->code }}</td>
                    <td class="text-capitalize">{{ __(str_replace('_', ' ', $value->type)) }}</td>
                    <td>{{ @$value->classroom->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $teachers->firstItem() }} {{ __('to') }} {{ $teachers->lastItem() }}
                    {{ __('of') }} {{ $teachers->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $teachers->links() }}</div>
            </div>
        </div>
    </div>

    {{-- <div style="padding: 10px; float: right;">

    </div> --}}

</div>
