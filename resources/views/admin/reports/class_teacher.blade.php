@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>{{ __('Teacher report') }}</h3>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    <a href="{{route('admin.report.class_teacher_export')}}" class="btn btn-success">{{ __('Export CSV')}}</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Search Teacher') }}</h3>
                        </div>
                        {{-- <form method="get" action="{{ url()->current() }}"> --}}
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ __('Name') }}</label>
                                        <input type="text" class="form-control name-input" value="{{Request::get('name')}}" name="name" id="name" placeholder="Name" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="code">{{ __('Code') }}</label>
                                        <input type="text" class="form-control code-input" value="{{Request::get('code')}}" name="code" id="code" placeholder="Code">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <button class="btn btn-primary" type="submit" style="margin-top:30px;">Search</button> --}}
                                        <a href="{{ url()->current() }}" class="btn btn-secondary" style="margin-top:30px;">
                                            <i class="fa fa-eraser"></i>
                                            {{ __('Reset') }}
                                        </a>

                                    </div>
                                </div>
                            </div>
                        {{-- </form> --}}
                    </div>
                    {{-- @include('_message') --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('Teacher report list')}}</h3>
                            <span class="badge bg-warning total-count">{{ $teachers->total() }}</span>
                        </div>
                        <!-- /.card-header -->
                        @include('admin.reports.partials._class_teacher')
                        <!-- /.card-body -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>

    </div>
@endsection

@push('script')
    <script>
        // name input
        $(document).on('keyup', '.name-input', function () {
            var name = $(this).val();
            var code = $('.code-input').val();
            var data = {
                'name': name,
                'code': code,
            }
            console.log(name);
            $.ajax({
                type: "get",
                url: window.location.href,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.view) {
                        $('.teacher-table-wrapper').replaceWith(response.view);
                    }

                }
            });
        });
        // ------------------------

        // code input
        $(document).on('keyup', '.code-input', function () {
            var name = $('.name-input').val();
            var code = $(this).val();
            var data = {
                'name': name,
                'code': code,
            }
            console.log(code);
            $.ajax({
                type: "get",
                url: window.location.href,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.view) {
                        $('.teacher-table-wrapper').replaceWith(response.view);
                    }

                }
            });
        });
        // ------------------------

        $(document).on('click', '.btn-view', function(){
            $("div.modal_form").load($(this).data('href'), function(){

                $(this).modal('show');

            });
        });
        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function (response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            });
        });
    </script>
@endpush
