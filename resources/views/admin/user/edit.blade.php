@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{(__('User Edit'))}}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <form method="POST" action="{{ route('admin.user.update', $user->id) }}"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="row">

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Username') }}<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control"
                                                value="{{ old('name', $user->name) }}" name="name"
                                                placeholder="{{ __('Enter Username') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('First Name') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control"
                                                value="{{ old('first_name', $user->first_name) }}" name="first_name"
                                                placeholder="{{ __('Enter First Name') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Last Name') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control"
                                                value="{{ old('last_name', $user->last_name) }}" name="last_name"
                                                placeholder="{{ __('Enter Last Name') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Gender') }}<span style="color: red;">*</span></label>
                                            <select class="form-control" name="gender">
                                                <option value="1" {{ $user->gender == 1 ? 'selected' : null }}>
                                                    {{ __('Male') }}</option>
                                                <option value="2" {{ $user->gender == 2 ? 'selected' : null }}>
                                                    {{ __('Female') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Email') }}</label>
                                            <input type="text" class="form-control"
                                                value="{{ old('email', $user->email) }}" name="email"
                                                placeholder="{{ __('Enter Email') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Password') }}</label>
                                            <input type="password" class="form-control" value="" name="password"
                                                placeholder="{{ __('Enter Password') }}">
                                        </div>
                                        {{-- <div class="form-group col-md-6">
                                            <label>{{ __('Role') }}</label>
                                            <select name="role" id="role" class="form-control select2">
                                                @foreach ($roles as $id => $name)
                                                    <option value="{{ $id }}" {{ $user->roles && $user->roles->first()->id == $id ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Role') }}</label>
                                            <select name="role" id="role" class="form-control select2">
                                                @foreach ($roles as $id => $name)
                                                    <option value="{{ $id }}" {{ optional($user->roles)->first() ? ($user->roles->first()->id == $id ? 'selected' : '') : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        

                                        {{-- <div class="form-group col-md-6">
                                            <label>{{ __('Image') }}<span style="color: red;">*</span></label>
                                            <input type="file" name="image" class="form-control" id="exampleInputFile">
                                            <p><img src="{{ asset('storage/upload/user/' . $user->image) }}"
                                                    alt="Profile Pic" width="50"></p>
                                        </div> --}}
                                        <div class="form-group col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputFile">{{__('Image')}}</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="hidden" name="image_names" class="image_names_hidden">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/png, image/jpeg">
                                                        <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                                                    </div>
                                                </div>
                                                <div class="preview preview-multiple text-center border rounded mt-2" style="height: 150px">
                                                    <div class="update_image">
                                                        <div class="img_container">
                                                            <img src="{{ $user->image_url }}" alt="" height="100%">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                </div>
                            </form>


                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('script')
    <script>
        const compressor = new window.Compress();
        $('.custom-file-input').change(function (e) {
            compressor.compress([...e.target.files], {
                size: 4,
                quality: 0.75,
            }).then((output) => {
                var files = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                var formData = new FormData();

                var image_names_hidden = $(this).closest('.custom-file').find('input[type=hidden]');
                var container = $(this).closest('.form-group').find('.preview');
                container.find('.update_image').empty();
                if (container.find('img').attr('src') === `{{ asset('images/default.svg') }}`) {
                    container.empty();
                }
                var _token = `{{ csrf_token() }}`;
                formData.append('image', files);
                formData.append('_token', _token);

                $.ajax({
                    url: "{{ route('save_temp_file') }}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 0) {
                            toastr.error(response.msg);
                        }
                        if (response.status == 1) {
                            container.empty();
                            var temp_files = response.temp_files;
                            for (var i = 0; i < temp_files.length; i++) {
                                var temp_file = temp_files[i];
                                var img_container = $('<div></div>').addClass('img_container');
                                var img = $('<img>').attr('src', "{{ asset('upload/temp') }}" +'/'+ temp_file);
                                img_container.append(img);
                                container.append(img_container);
                                // $(selector).replaceWith(newContent);

                                var new_file_name = temp_file;
                                console.log(new_file_name);

                                image_names_hidden.val(new_file_name);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush