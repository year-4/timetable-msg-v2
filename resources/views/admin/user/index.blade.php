@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>{{ __('Users') }}</h3>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    @if (auth()->user()->can('user.create'))
                        <a href="{{ route('admin.user.create') }}" class="btn btn-primary">{{ __('Add New') }}</a>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Search User') }}</h3>
                        </div>
                        {{-- <form method="get" action=""> --}}
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="name">{{ __('Name') }}</label>
                                        <input type="text" class="form-control name-input" value="{{Request::get('name')}}" name="name" id="name" placeholder="{{__('Name')}}">
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <button class="btn btn-primary" type="submit" style="margin-top:30px;">Search</button> --}}
                                        <a href="{{ url()->current() }}" class="btn btn-secondary" style="margin-top:30px;">
                                            <i class="fa fa-eraser"></i>
                                            {{ __('Reset') }}
                                        </a>

                                    </div>
                                </div>
                            </div>
                        {{-- </form> --}}
                    </div>
                    {{-- @include('_message') --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('User List')}}</h3>
                            <span class="badge bg-warning total-count">{{ $users->total() }}</span>
                        </div>
                        <!-- /.card-header -->
                        @include('admin.user.partials._table')
                        <!-- /.card-body -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>

    </div>
@endsection

@push('script')
    <script>

        // name input
        $(document).on('keyup', '.name-input', function () {
            var name = $(this).val();
            var data = {
                'name': name,
            }
            console.log(name);
            $.ajax({
                type: "get",
                url: window.location.href,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (response.view) {
                        $('.table-wrapper').replaceWith(response.view);
                    }

                }
            });
        });
        // ------------------------

        $(document).on('click', '.btn-view', function(){
            $("div.modal_form").load($(this).data('href'), function(){

                $(this).modal('show');

            });
        });
        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function (response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            });
        });
    </script>
@endpush
