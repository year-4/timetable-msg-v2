@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__('User Information')}}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container">

                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row justify-content-center"> <!-- Centering the content -->
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="profile-picture text-center" style="padding: 20px;">
                                    <!-- Centering the image -->
                                    @if (Auth::user()->image)
                                        <img src="{{ asset('upload/users/' . Auth::user()->image) }}" class="avatar"
                                            width="100%" style="object-fit: cover; border-radius:5px;" alt="User Image">
                                    @else
                                        <img src="{{ asset('images/default.svg') }}"width="100%"
                                            style="object-fit: cover; border-radius:5px;" alt="User Image">
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-12" style="margin-top:30px ; padding:20px" >
                                <div class="profile-basic" >
                                    <div class="row">
                                        @if ($user->teacher)
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    <h3 class="user-name m-t-0">{{ $user->teacher->khmer_fullname }}</h3>
                                                    <h3 class="user-name m-t-0">{{ Auth::user()->name }}</h3>
                                                    <h5 class="company-role m-t-0 m-b-0">
                                                        @if (Auth::user()->hasRole('admin'))
                                                            Admin
                                                        @elseif (Auth::user()->hasRole('teacher'))
                                                            {{__('Teacher')}}
                                                        @else
                                                            {{ __('User') }}
                                                        @endif
                                                    </h5>
                                                    <div class="staff-id">{{__('Primary Subject')}} : {{ $user->teacher->primarySubject->name }} </div>
                                                    
                                                    <div class="staff-id">{{__('Teacher ID')}} : {{ $user->teacher->code }} </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <ul class="personal-info">
                                                    <li>
                                                        <h6 >{{__('Phone Number')}}: <span>{{ $user->teacher->phone }}</span>
                                                        </h6>

                                                    </li>
                                                    <li>
                                                        <h6 class="title">{{__('Email')}}: <span>{{ $user->teacher->email }}</span>
                                                        </h6>

                                                    </li>

                                                    <li>
                                                        <h6 class="title">{{__('Gender')}}: <span>
                                                                @if (Auth::user()->gender)
                                                                    {{__("Male")}}
                                                                @else
                                                                    {{__("Female")}}
                                                                @endif
                                                            </span></h6>
                                                    </li>
                                                    <li>
                                                        <h6 class="title">
                                                            {{__('Address')}}: <span>{{ $user->teacher->address }}</span></h6>

                                                    </li>
                                                </ul>
                                            </div>
                                        @else
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    {{-- <h3 class="user-name m-t-0">{{ $user->teacher->khmer_fullname }}</h3> --}}
                                                    <h3 class="user-name m-t-0">{{ Auth::user()->name }}</h3>
                                                    <h5 class="company-role m-t-0 m-b-0">
                                                        @if (Auth::user()->hasRole('admin'))
                                                            Admin
                                                        @elseif (Auth::user()->hasRole('teacher'))
                                                            {{__('Teacher')}}
                                                        @else
                                                            {{ __('User') }}
                                                        @endif
                                                    </h5>
                                                    {{-- <div class="staff-id">{{__('Primary Subject')}} : {{ $user->teacher->primarySubject->name }} </div>
                                                    <div class="staff-id">{{__('Secondary Subject')}} : {{ $user->teacher->secondarySubject->name }} </div>
                                                    <div class="staff-id">{{__('Teacher ID')}} : {{ $user->teacher->code }} </div> --}}
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <ul class="personal-info">
                                                    <li>
                                                        <h6 >{{__('Phone Number')}}: <span>{{ $user->phone }}</span>
                                                        </h6>

                                                    </li>
                                                    <li>
                                                        <h6 class="title">{{__('Email')}}: <span>{{ $user->email }}</span>
                                                        </h6>

                                                    </li>

                                                    <li>
                                                        <h6 class="title">{{__('Gender')}}: <span>
                                                                @if (Auth::user()->gender)
                                                                    {{__("Male")}}
                                                                @else
                                                                    {{__("Female")}}
                                                                @endif
                                                            </span></h6>
                                                    </li>
                                                    {{-- <li>
                                                        <h6 class="title">
                                                            {{__('Address')}}: <span>{{ $user->teacher->address }}</span></h6>

                                                    </li> --}}
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            @php
                                $id = auth()->id();
                                // if (auth()->user()->hasRole('teacher')) {
                                //     $id = $user->teacher->id;
                                // }
                            @endphp
                                {{-- <a href="{{ route('admin.user.profile_edit', $user->teacher->id) }}"class="btn btn-info  btn-edit"> --}}
                                    <a href="{{ route('admin.user.profile_edit', $id) }}"class="btn btn-info  btn-edit">
                                    <i class="fas fa-pencil-alt"></i>
                                    {{ __('Edit') }}
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
