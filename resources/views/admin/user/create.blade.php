@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Add New') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <form method="POST" action="{{ route('admin.user.store') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('User Information') }}</h1>
                                    <span class="badge bg-warning total-count"></span>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Name') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('name') }}"
                                                name="name" placeholder="{{ __('Enter Name') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('First Name') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('first_name') }}"
                                                name="first_name" placeholder="{{ __('Enter First Name') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Last Name') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('last_name') }}"
                                                name="last_name" placeholder="{{ __('Enter Last Name') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Gender') }}<span style="color: red;">*</span></label>
                                            <select class="form-control" name="gender">
                                                <option value="1">{{ __('Male') }}</option>
                                                <option value="2">{{ __('Female') }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Email') }}<span style="color: red;">*</span></label>
                                            <input type="email" class="form-control" value="{{ old('email') }}"
                                                name="email" placeholder="{{ __('Enter Email') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Password') }}</label>
                                            <input type="text" class="form-control" value="{{ old('password') }}"
                                                name="password" placeholder="{{ __('Enter Password') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Role') }}</label>
                                            <select name="role" id="role" class="form-control select2">
                                                @foreach ($roles as $id => $name)
                                                    <option value="{{ $id }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        {{-- <div class="form-group col-md-6">
                                            <label>{{ __('Image') }}<span style="color: red;">*</span></label>
                                            <input type="file" name="image" class="form-control" id="exampleInputFile">
                                        </div> --}}
                                        <div class="form-group col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputFile">{{__('Image')}}</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="hidden" name="image_names" class="image_names_hidden">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/png, image/jpeg">
                                                        <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                                                    </div>
                                                </div>
                                                <div class="preview preview-multiple text-center border rounded mt-2" style="height: 150px">
                                                    <img src="{{ asset('images/default.svg') }}" alt="" height="100%">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('script')
    <script>
        const compressor = new window.Compress();
        $('.custom-file-input').change(function (e) {
            compressor.compress([...e.target.files], {
                size: 4,
                quality: 0.75,
            }).then((output) => {
                var files = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                var formData = new FormData();

                var image_names_hidden = $(this).closest('.custom-file').find('input[type=hidden]');
                var container = $(this).closest('.form-group').find('.preview');
                if (container.find('img').attr('src') === `{{ asset('images/default.svg') }}`) {
                    container.empty();
                }
                var _token = `{{ csrf_token() }}`;
                formData.append('image', files);
                formData.append('_token', _token);

                $.ajax({
                    url: "{{ route('save_temp_file') }}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 0) {
                            toastr.error(response.msg);
                        }
                        if (response.status == 1) {
                            container.empty();
                            var temp_file = response.temp_files;
                            var img_container = $('<div></div>').addClass('img_container');
                            var img = $('<img>').attr('src', "{{ asset('upload/temp') }}" +'/'+ temp_file);
                            img_container.append(img);
                            container.append(img_container);

                            var new_file_name = temp_file;
                            console.log(new_file_name);

                            image_names_hidden.val(new_file_name);
                        }
                    }
                });
            });
        });
    </script>
@endpush
