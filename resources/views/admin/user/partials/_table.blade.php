<div class="card-body p-0 table-responsive table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{__('Name')}}</th>
                <th>{{__('First Name')}}</th>
                <th>{{__('Last Name')}}</th>
                <th>{{__('Gender')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('Type')}}</th>
                <th>{{__('Image')}}</th>
                <th>{{__('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $value)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->first_name}}</td>
                    <td>{{$value->last_name}}</td>
                    <td>
                        {{ $value->gender == 1 ? __('Male') : __('Female') }}
                    </td>
                     <td>{{$value->email}}</td>
                    <td>{{$value->type}}</td>
                    <td>
                        <img src="{{ $value->image_url }}" alt="Profile Pic" width="50px"
                            height="50px" style="object-fit: cover; border-radius:5px;">
                    </td>
                    <td style="white-space: nowrap">

                        @if (auth()->user()->can('user.view'))
                            <a href="#" data-href="{{ route('admin.user.show', $value->id) }}" data-container=".modal_form" class="btn btn-info btn-xs btn-view">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Detail') }}
                            </a>
                        @endif

                        @if (($value->id != 3 || auth()->id() == 3) && auth()->user()->can('user.edit'))
                            <a href="{{ route('admin.user.edit', $value->id) }}"class="btn btn-info btn-xs btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        @endif

                        @if (!$value->hasRole('admin') && $value->id != 3 && auth()->user()->can('user.delete'))
                            <form action="{{ route('admin.user.destroy', $value->id) }}" class="d-inline-block form-delete-{{ $value->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $value->id }}" data-href="{{ route('admin.user.destroy', $value->id) }}" class="btn btn-danger btn-xs btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $users->firstItem() }} {{ __('to') }} {{ $users->lastItem() }} {{ __('of') }} {{ $users->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $users->links() }}</div>
            </div>
        </div>
    </div>

    {{-- <div style="padding: 10px; float: right;">

    </div> --}}

</div>
