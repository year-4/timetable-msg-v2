<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('User Detail') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="profile-picture" style="padding: 20px;">
                        <img src="{{ $user->image_url }}" alt="Profile Pic" width="100%"  style="object-fit: cover; border-radius:5px;" >
                    </div>
                </div>

                <div class="col-lg-8 col-md-6 col-12">
                    <div class="profile-details" style="padding: 20px;">
                        <p><strong>{{ __('Username') }}:</strong> {{ $user->name }}</p>
                        <p><strong>{{ __('First Name') }}:</strong> {{ $user->first_name }}</p>
                        <p><strong>{{ __('Last Name') }}:</strong> {{ $user->last_name }}</p>
                        <p><strong>{{ __('Gender') }}:</strong> {{ $user->gender == 1 ? 'Male' : 'Female' }}</p>
                        <p><strong>{{ __('Email') }}:</strong> {{ $user->email }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            <a href="{{ route('admin.user.edit', $user->id) }}"class="btn btn-info  btn-edit">
                <i class="fas fa-pencil-alt"></i>
                {{ __('Edit') }}
            </a>
        </div>

    </div>
</div>
