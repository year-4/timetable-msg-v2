@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{ __('Timetable') }} - {{ __('Teacher') }}: {{ $teacher->full_name }}</h1>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        @if ($timetable)
                            {{-- <a href="{{ route('admin.timetable.weekly_adjustment', ['id' => $timetable->id, 'academic_year_id' => request('academic_year_id'), 'grade_id' => request('grade_id'), 'class_id' => request('class_id')]) }}" class="btn btn-primary">{{ __('Weekly Adjustment') }}</a> --}}
                            <a href="{{ route('admin.timetable.adjustment', ['id' => $timetable->id, 'academic_year_id' => request('academic_year_id'), 'grade_id' => request('grade_id'), 'class_id' => request('class_id')]) }}" class="btn btn-primary">{{ __('Adjustment') }}</a>
                        @endif
                        {{-- <a href="{{ route('admin.timetable.generate') }}"class="btn btn-primary">{{ __('Generate') }}</a> --}}
                        {{-- <a href="{{ url('admin/timetable/index') }}"class="btn btn-primary">{{ __('Back') }}</a> --}}
                        {{-- <button class="btn btn-primary" onclick="printTimetable()">Print {{ __('Timetable') }}</button> --}}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="8" class="text-center">{{ __('Morning') }}</th>
                                            </tr>
                                            <tr>
                                                <th class="text-center">{{ __('Time') }}</th>
                                                @foreach ($week_days as $day)
                                                    <th class="text-center">{{ $day }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($time_slots as $time)
                                                @if ($time->daytime == 'morning')
                                                    <tr>
                                                        <td>{{ date('H:i', strtotime($time->start_time)) }} - {{ date('H:i', strtotime($time->end_time)) }}</td>
                                                        @foreach ($week_days as $day)
                                                            @php
                                                                $timetable_slot = null;
                                                                if ($timetable_slots) {
                                                                    $timetable_slot = $timetable_slots->where('time_slot_id', $time->id)
                                                                                                ->where('week_day', $day)
                                                                                                ->first();
                                                                }
                                                            @endphp
                                                            <td class="text-center position-relative timetble-slot-td" id="{{ $day . '-' . $time->id }}">
                                                                <input type="hidden" name="week_day" class="week_day" value="{{ $day }}">
                                                                <input type="hidden" name="time_slots" class="time_slot" value="{{ $time->id }}">
                                                                @if ($timetable_slot)
                                                                    <span class=" text-bold">{{ $timetable_slot->timetable->classroom->name }}</span>

                                                                @else
                                                                    {{-- <a class="btn btn-outline-primary btn-modal rounded-circle btn_circle" href="#"
                                                                        data-href="{{ trim(route('admin.timetable.showPopupCreate')) }}"
                                                                        data-toggle="modal" data-container=".modal_form">
                                                                        <i class=" fa fa-plus-circle"></i>
                                                                    </a> --}}
                                                                @endif

                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <th colspan="8" class="text-center">Evening</th>
                                            </tr>
                                            @foreach ($time_slots as $time)
                                                @if ($time->daytime == 'afternoon')
                                                    <tr>
                                                        <td>{{ date('H:i', strtotime($time->start_time)) }} - {{ date('H:i', strtotime($time->end_time)) }}</td>
                                                        @foreach ($week_days as $day)
                                                            @php
                                                                $timetable_slot = null;
                                                                if ($timetable_slots) {
                                                                    $timetable_slot = $timetable_slots->where('time_slot_id', $time->id)
                                                                                                ->where('week_day', $day)
                                                                                                ->first();
                                                                }
                                                            @endphp

                                                            <td class="text-center position-relative timetble-slot-td" id="{{ $day . '-' . $time->id }}">
                                                                <input type="hidden" name="week_day" class="week_day" value="{{ $day }}">
                                                                <input type="hidden" name="time_slots" class="time_slot" value="{{ $time->id }}">
                                                                @if ($timetable_slot)
                                                                    <span class=" text-bold">{{ $timetable_slot->timetable->classroom->name }}</span>

                                                                @else
                                                                    {{-- <a class="btn btn-outline-primary btn-modal rounded-circle btn_circle" href="#"
                                                                        data-href="{{ trim(route('admin.timetable.showPopupCreate')) }}"
                                                                        data-toggle="modal" data-container=".modal_form">
                                                                        <i class=" fa fa-plus-circle"></i>
                                                                    </a> --}}
                                                                @endif

                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->



        </section>

        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>

    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function () {
            $('.select2-modal').select2({
                theme: 'bootstrap4',
                dropdownParent: $('.modal_form')
            });
        });
        $(document).on('click', '.btn-modal', function() {
            console.log($(this).data('href'));
            var academic_year_id = `{{ request('academic_year_id') }}`;
            var grade_id = `{{ request('grade_id') }}`;
            var class_id = `{{ request('class_id') }}`;
            var week_day = $(this).closest('td').find('input.week_day').val();
            var time_slot = $(this).closest('td').find('input.time_slot').val();
            var data = {
                'academic_year_id' : academic_year_id,
                'grade_id' : grade_id,
                'class_id' : class_id,
                'week_day' : week_day,
                'time_slot' : time_slot,
            }
            // $("div.modal_form").load($(this).data('href'), data, function() {

            //     $(this).modal('show');

            // });
            $.get($(this).data('href'), data, function(response) {
                $("div.modal_form").html(response).modal('show');
            });
        });

        // $(document).on('click', '.btn-edit', function(){
        //     $("div.modal_form").load($(this).data('href'), function(){

        //         $(this).modal('show');

        //     });
        // });

        $('.submit').click(function(e) {
            e.preventDefault();
            // alert('ok');
            // console.log('ok');
        });

        $(document).on('mouseover', '.timetble-slot-td', function (e) {
            $(this).find('a.btn-edit-timetable-slot').removeClass('d-none');
        });
        $(document).on('mouseout', '.timetble-slot-td', function (e) {
            $(this).find('a.btn-edit-timetable-slot').addClass('d-none');
        });
    </script>
@endpush
