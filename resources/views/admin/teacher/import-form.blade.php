{{-- @extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Add New') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="card">
                            @if (Session::has('success'))
                            <div class="alert alert-success p-2">{{Session::get('success')}}</div>
                            @endif
                            @if (Session::has('fail'))
                            <div class="alert alert-danger p-2">{{Session::get('fail')}}</div>
                            @endif
                    <div class="card-body">
                      <form action="{{ route('admin.teacher.import')}}" method="post" enctype="multipart/form-data">
                            @csrf    
                        <div class="form-group my-2">
                                <input type="file" name="file" id="" class="form-control">
                                <span class="text-danger">@error('file'){{$message}}@enderror</span>
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-sm">Import</button>
                      </form>
                    </div>
                </div>
                
                
            </div>
@endsection --}}

@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Import File') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success p-2">{{ Session::get('success') }}</div>
                        @endif
                        @if (Session::has('fail'))
                            <div class="alert alert-danger p-2">{{ Session::get('fail') }}</div>
                        @endif
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <form action="{{ route('admin.teacher.import') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('Import Teacher') }}</h1>
                                    <span class="badge bg-warning total-count"></span>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Please Choose Excel File') }}<span
                                                    style="color: red;">*</span></label>
                                            <input type="file" name="file" id="" class="form-control">
                                            <span class="text-danger">
                                                @error('file')
                                                    {{ $message }}
                                                @enderror
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Import') }}</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
