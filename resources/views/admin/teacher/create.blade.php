@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    @push('style')
    {{-- <link href="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet"> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
    @endpush
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Add New') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <form method="POST" action="{{ route('admin.teacher.store') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('Teacher Information') }}</h1>
                                    <span class="badge bg-warning total-count"></span>
                                </div>
                                <div class="card-body">
                                    <div class="row">

                                        <div class="form-group col-md-6">
                                            <label>{{ __('First Name (Khmer)') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('khmer_fname') }}"
                                                name="khmer_fname" placeholder="{{ __('Enter Khmer Letter') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Last Name (Khmer)') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('khmer_lname') }}"
                                                name="khmer_lname" placeholder="{{ __('Enter Khmer Letter') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('First Name (English)') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('first_name') }}"
                                                name="first_name" placeholder="{{ __('Enter English Letter') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Last Name (English)') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('last_name') }}"
                                                name="last_name" placeholder="{{ __('Enter English Letter') }}">
                                        </div>


                                        <div class="form-group col-md-6">
                                            <label for="primary_subject_id">{{ __('Primary Subject') }}<span
                                                    style="color: red;">*</span></label>
                                            <select name="primary_subject_id" id="primary_subject_id"
                                                class="form-control select2">
                                                <option value="">{{ __('Select Subject') }}</option>
                                                @foreach ($subjects as $id => $name)
                                                    <option value="{{ $id }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group col-md-6">
                                            <label for="secondary_subject_id">{{ __('Secondary Subject') }}<span
                                                    style="color: red;">*</span></label>
                                            <select name="secondary_subject_id" id="secondary_subject_id"
                                                class="form-control select2">
                                                <option value="">{{ __('Select Subject') }}</option>
                                                @foreach ($subjects as $id => $name)
                                                    <option value="{{ $id }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Code') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('code') }}"
                                                name="code" placeholder="{{ __('Enter Code') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Type') }}<span style="color: red;">*</span></label>
                                            <select name="type" id="type" class="form-control">
                                                @foreach ($teacher_types as $type_value => $type_text)
                                                    <option value="{{ $type_value }}">{{__($type_text)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Gender') }}<span style="color: red;">*</span></label>
                                            <select class="form-control" name="gender">
                                                <option value="1">{{ __('Male') }}</option>
                                                <option value="2">{{ __('Female') }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Phone Number') }}<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" value="{{ old('phone') }}"
                                                name="phone" placeholder="{{ __('Enter Phone Number') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Address') }}</label>
                                            <input type="text" class="form-control" value="{{ old('address') }}"
                                                name="address" placeholder="{{ __('Enter Address') }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>{{ __('Date of Employee') }}</label>
                                            <input type="text" id="datepicker" class="form-control"
                                            value="{{ old('doe') }}"
                                            name="doe" placeholder="{{ __('Enter Date of Employee') }}">
                                        </div>

                                        {{-- <div class="form-group col-md-6">
                                            <label>{{ __('Image') }}<span style="color: red;">*</span></label>
                                            <input type="file" name="image" class="form-control" id="exampleInputFile">
                                        </div> --}}
                                        <div class="form-group col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputFile">{{__('Image')}}</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="hidden" name="image_names" class="image_names_hidden">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/png, image/jpeg">
                                                        <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                                                    </div>
                                                </div>
                                                <div class="preview preview-multiple text-center border rounded mt-2" style="height: 150px">
                                                    <img src="{{ asset('images/default.svg') }}" alt="" height="100%">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('Teacher Login Information') }}</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Username') }}<span style="color: red;">*</span></label>
                                            <input type="name" class="form-control" value="{{ old('username') }}"
                                                name="username" placeholder="{{ __('Enter Username') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Email') }}</label>
                                            <input type="text" class="form-control" value="{{ old('email') }}"
                                                name="email" placeholder="{{ __('Enter Email') }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Password') }}</label>
                                            <input type="password" class="form-control" value="" name="password"
                                                placeholder="{{ __('Enter Password') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        const compressor = new window.Compress();
        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });
        $('.custom-file-input').change(function (e) {
            compressor.compress([...e.target.files], {
                size: 4,
                quality: 0.75,
            }).then((output) => {
                var files = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                var formData = new FormData();

                var image_names_hidden = $(this).closest('.custom-file').find('input[type=hidden]');
                var container = $(this).closest('.form-group').find('.preview');
                if (container.find('img').attr('src') === `{{ asset('images/default.svg') }}`) {
                    container.empty();
                }
                var _token = `{{ csrf_token() }}`;
                formData.append('image', files);
                formData.append('_token', _token);

                $.ajax({
                    url: "{{ route('save_temp_file') }}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 0) {
                            toastr.error(response.msg);
                        }
                        if (response.status == 1) {
                            container.empty();
                            var temp_file = response.temp_files;
                            var img_container = $('<div></div>').addClass('img_container');
                            var img = $('<img>').attr('src', "{{ asset('upload/temp') }}" +'/'+ temp_file);
                            img_container.append(img);
                            container.append(img_container);

                            var new_file_name = temp_file;
                            console.log(new_file_name);

                            image_names_hidden.val(new_file_name);
                        }
                    }
                });
            });
        });
    </script>
@endpush
