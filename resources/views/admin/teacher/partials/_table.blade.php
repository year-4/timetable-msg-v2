<div class="card-body p-0 table-responsive table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Khmer Name') }}</th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Username') }}</th>
                <th>{{ __('Code') }}</th>
                <th>{{ __('Gender') }}</th>
                <th class="text-nowrap">{{ __('Phone Number') }}</th>
                <th>{{ __('Email') }}</th>
                {{-- <th>{{__('Password')}}</th> --}}
                {{-- <th>{{__('Address')}}</th> --}}
                <th>{{ __('Image') }}</th>
                {{-- <th>{{__('Created date') }}</th> --}}
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($teachers as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->khmer_fullname }}</td>
                    <td>{{ $value->full_name }}</td>
                    <td>{{ $value->username }}</td>
                    <td>{{ $value->code }}</td>
                    <td>
                        {{ $value->gender == 1 ? __('Male') : __('Female') }}
                    </td>
                    <td>{{ $value->phone }}</td>
                    <td>{{ $value->email }}</td>
                    {{-- <td>{{$value->password}}</td> --}}
                    {{-- <td>{{$value->address}}</td> --}}
                    <td>
                        <img src="{{ $value->image_url }}" alt="Profile Pic" width="50px"
                            height="50px" style="object-fit: cover; border-radius:5px;">
                    </td>
                    {{-- <td>{{date('d-m-Y H:i A', strtotime($value->created_at))}}</td> --}}
                    <td style="white-space: nowrap">
                        <a href="{{ route('admin.teacher.timetable', $value->id) }}"class="btn btn-info btn-xs btn-edit">
                            {{-- <i class="fa fa-clock"></i> --}}
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            {{-- {{ __('View Timetable') }} --}}
                        </a>
                        @if (auth()->user()->can('teacher.view'))
                            <a href="#" data-href="{{ route('admin.teacher.show', $value->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-xs btn-view">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                                
                            </a>
                        @endif
                        @if (auth()->user()->can('teacher.edit'))
                            <a
                                href="{{ route('admin.teacher.edit', $value->id) }}"class="btn btn-info btn-xs btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                               
                            </a>
                        @endif
                        @if (auth()->user()->can('teacher.delete'))
                            <form action="{{ route('admin.teacher.destroy', $value->id) }}"
                                class="d-inline-block form-delete-{{ $value->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $value->id }}"
                                    data-href="{{ route('admin.teacher.destroy', $value->id) }}"
                                    class="btn btn-danger btn-xs btn-delete">
                                    <i class="fas fa-trash"></i>
                                    
                                </button>
                            </form>
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $teachers->firstItem() }} {{ __('to') }} {{ $teachers->lastItem() }}
                    {{ __('of') }} {{ $teachers->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $teachers->links() }}</div>
            </div>
        </div>
    </div>

    {{-- <div style="padding: 10px; float: right;">

    </div> --}}

</div>
