<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Teacher Detail') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="profile-picture" style="padding: 20px;">
                        <img src="{{ $teacher->image_url }}" alt="Profile Pic" width="100%"  style="object-fit: cover; border-radius:5px;" >
                    </div>
                </div>

                <div class="col-lg-8 col-md-6 col-12">
                    <div class="profile-details" style="padding: 20px;">
                        <p><strong>{{ __('Full Name') }}:</strong> {{ $teacher->fullname }}</p>
                        <p><strong>{{ __('Username') }}:</strong> {{ $teacher->username }}</p>
                        <p><strong>{{ __('Code') }}:</strong> {{ $teacher->code }}</p>
                        <p><strong>{{ __('Phone Number') }}:</strong> {{ $teacher->phone }}</p>
                        <p><strong>{{ __('Email') }}:</strong> {{ $teacher->email }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
            <a href="{{ route('admin.teacher.edit', $teacher->id) }}"class="btn btn-info  btn-edit">
                <i class="fas fa-pencil-alt"></i>
                {{ __('Edit') }}
            </a>
        </div>

    </div>
</div>
