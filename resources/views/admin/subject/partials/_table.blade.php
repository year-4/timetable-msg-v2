<div class="card-body p-0 table-wrapper">
    <table class="table">
        <thead>
            <tr>
                {{-- <th >#</th> --}}
                <th>{{ __('Name') }}</th>
                <th>{{ __('Grade') }}</th>
                <th>{{ __('Created date') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($subjects as $subject)
                <tr>
                    {{-- <td>{{$grade->id}}</td> --}}
                    <td>{{ $subject->name }}</td>
                    <td>
                        @foreach ($subject->grade_id as $id)
                            @php
                                $grade = \App\Models\Grade::find($id);
                            @endphp
                            {{ $grade->name }}
                            @if (!$loop->last)
                                {{ ', ' }}
                            @endif
                        @endforeach
                    </td>
                    <td>{{ $subject->created_at->format('d M Y h:i A') }}</td>
                    <td>
                        @if (auth()->user()->can('subject.edit'))
                            <a href="#" data-href="{{ route('admin.subject.edit', $subject->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-xs btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        @endif
                        @if (auth()->user()->can('subject.delete'))
                            <form action="{{ route('admin.subject.destroy', $subject->id) }}"
                                class="d-inline-block form-delete-{{ $subject->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $subject->id }}"
                                    data-href="{{ route('admin.subject.destroy', $subject->id) }}"
                                    class="btn btn-danger btn-xs btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- <div style="padding: 10px; float: right;">

        {{ $grades->links() }}
    </div> --}}

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $subjects->firstItem() }} {{ __('to') }} {{ $subjects->lastItem() }}
                    {{ __('of') }} {{ $subjects->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $subjects->links() }}</div>
            </div>
        </div>
    </div>


</div>
