<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Add Subject') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.subject_type.store') }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="name">{{ __('Name') }}</label>
                    <input type="text" name="name" class="form-control">
                </div>

                {{-- <div class="form-group">
                    <label for="subject_type_id">{{ __('Subject Type') }}</label>
                    <select name="subject_type_id" id="subject_type_id" class="form-control select2-modal" style="width: 100%">
                        <option value="" selected disabled>{{ __('Select Suject Type') }}</option>
                        @foreach ($subject_types as $id => $type)
                            <option value="{{ $id }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div> --}}

                <div class="form-group">
                    <label for="grade_id">{{ __('Grade') }}</label>
                    <select name="grade[]" id="grade_id" class="form-control select2-modal" style="width: 100%" multiple>
                        @foreach ($grades as $id => $grade)
                            <option value="{{ $id }}">{{ $grade }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.select2-modal').select2({
            theme: 'bootstrap4',
            dropdownParent: $('.modal_form')
        });
    });
    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({

            type: "post",
            url: `{{ route('admin.subject.store') }}`,
            data: data,
            // dataType: "json",
            success: function (response) {
                console.log(response)
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Created successfully') }}`,
                        // message: ,
                    });
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);
                }

            }
        });
    });
</script>
