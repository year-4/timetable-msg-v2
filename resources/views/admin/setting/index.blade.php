@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h3>{{ __('Setting') }}</h3>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        {{-- <a class="btn btn-primary btn-modal" href="#" data-href="{{ route('admin.grade.create') }}" data-toggle="modal" data-container=".modal_form">
                        <i class=" fa fa-plus-circle"></i>
                        {{ __('Add New') }}
                    </a> --}}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form action="{{ route('admin.setting.save') }}" method="post">
                    @csrf
                    @method('PUT')
                    @include('admin.setting.partials._tab')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">{{ __('Teacher Limit Hour') }}</h3>
                                    {{-- <span class="badge bg-warning total-count">{{ $grades->total() }}</span> --}}
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 mb-2">
                                            <h5>{{ __('Teacher Limit hour') }}</h5>
                                            <span><i>{{ __('Enter the maximum hours of each teacher can teach a week') }}</i></span>
                                        </div>
                                        <div class="form-group col-12 col-sm-6 ">
                                            <label for="secondary_teacher">{{ __('Secondary School Teacher') }}</label>
                                            <input type="number" name="limit_hour[teacher][secondary_school]"
                                                id="secondary_teacher" class="form-control"
                                                value="{{ $teacher_limit ? $teacher_limit['secondary_school'] : null }}"
                                                min="1">
                                        </div>
                                        <div class="form-group col-12 col-sm-6 ">
                                            <label for="high_teacher">{{ __('Hight School Teacher') }}</label>
                                            <input type="number" name="limit_hour[teacher][high_school]" id="high_teacher"
                                                class="form-control"
                                                value="{{ $teacher_limit ? $teacher_limit['high_school'] : null }}"
                                                min="1">
                                        </div>

                                        <div class="col-12 mb-2">
                                            <h5>{{ __('Subject Limit hour') }}</h5>
                                            <span><i>{{ __('Enter the maximum hours of each subject can be taught a week') }}</i></span>
                                        </div>
                                        <div class="col-12">
                                            @if (isset($grades))
                                                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                                                    {{-- @dump($language) --}}
                                                    @foreach ($grades as $grade)
                                                        {{-- @if ($lang['status'] == 1) --}}
                                                        <li class="nav-item">
                                                            <a class="nav-link text-capitalize {{ $loop->iteration == 1 ? 'active' : '' }}"
                                                                id="tab_{{ $grade->id }}-tab" data-toggle="pill"
                                                                href="#tab_{{ $grade->id }}" role="tab"
                                                                aria-controls="tab_{{ $grade->id }}"
                                                                aria-selected="false">{{ __('Grade') }}
                                                                {{ $grade->name }}</a>
                                                        </li>
                                                        {{-- @endif --}}
                                                    @endforeach

                                                </ul>
                                                <div class="tab-content" id="custom-content-below-tabContent">
                                                    @foreach ($grades as $grade)
                                                        {{-- @if ($lang['status'] == 1) --}}
                                                        <div class="tab-pane fade {{ $loop->iteration == 1 ? 'show active' : '' }} mt-3"
                                                            id="tab_{{ $grade->id }}" role="tabpanel"
                                                            aria-labelledby="tab_{{ $grade->id }}-tab">
                                                            <div class="row">
                                                                @if ($grade->name == 12)
                                                                    <div class="form-group col-12 text-right">
                                                                        <select name="grade_type" id="grade_type" class="form-control">
                                                                            <option value="real_science">{{ __('Real Science') }}</option>
                                                                            <option value="social_science">{{ __('Social Science') }}</option>
                                                                        </select>
                                                                    </div>
                                                                    @foreach ($grade->subjects as $subject)
                                                                        {{-- @foreach ($subjects as $subject) --}}
                                                                        <div class="form-group col-12 col-sm-6 real_science_wrapper">
                                                                            <label for="subject_{{ $subject->id }}"
                                                                                class="text-capitalize">{{ $subject->name }}</label>
                                                                            <input type="number"
                                                                                name="limit_hour[subject][{{ $subject->id }}][grade_{{ $grade->id }}][real_science]"
                                                                                id="subject_{{ $subject->id }}"
                                                                                class="form-control"
                                                                                value="{{ $subject_limit ? @$subject_limit[$subject->id]['grade_' . $grade->id]['real_science'] : null }}"
                                                                                min="1">
                                                                        </div>
                                                                        <div class="form-group col-12 col-sm-6 social_science_wrapper d-none">
                                                                            <label for="subject_{{ $subject->id }}"
                                                                                class="text-capitalize">{{ $subject->name }}</label>
                                                                            <input type="number"
                                                                                name="limit_hour[subject][{{ $subject->id }}][grade_{{ $grade->id }}][social_science]"
                                                                                id="subject_{{ $subject->id }}"
                                                                                class="form-control"
                                                                                value="{{ $subject_limit ? @$subject_limit[$subject->id]['grade_' . $grade->id]['social_science'] : null }}"
                                                                                min="1">
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    @foreach ($grade->subjects as $subject)
                                                                        {{-- @foreach ($subjects as $subject) --}}
                                                                        <div class="form-group col-12 col-sm-6 ">
                                                                            <label for="subject_{{ $subject->id }}"
                                                                                class="text-capitalize">{{ $subject->name }}</label>
                                                                            <input type="number"
                                                                                name="limit_hour[subject][{{ $subject->id }}][grade_{{ $grade->id }}]"
                                                                                id="subject_{{ $subject->id }}"
                                                                                class="form-control"
                                                                                value="{{ $subject_limit ? @$subject_limit[$subject->id]['grade_' . $grade->id] : null }}"
                                                                                min="1">
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                {{-- table --}}
                                {{-- @include('admin.grade.partials._table') --}}


                            </div>
                            <div class="row">
                                <div class="col-12 my-3">
                                    <button type="submit" class="btn btn-primary float-right">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>
    </div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.btn-modal', function() {
            $("div.modal_form").load($(this).data('href'), function() {

                $(this).modal('show');

            });
        });

        $(document).on('click', '.btn-edit', function() {
            $("div.modal_form").load($(this).data('href'), function() {

                $(this).modal('show');

            });
        });

        $('.submit').click(function(e) {
            e.preventDefault();
            // alert('ok');
            // console.log('ok');
        });

        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function(response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            });
        });

        $(document).on('change', '#grade_type', function (e) {
            var type = $(this).val();
            console.log(type);
            if (type == 'real_science') {
                $('.social_science_wrapper').addClass('d-none');
                $('.real_science_wrapper').removeClass('d-none');
            } else {
                $('.social_science_wrapper').removeClass('d-none');
                $('.real_science_wrapper').addClass('d-none');

            }
        });
    </script>
@endpush
