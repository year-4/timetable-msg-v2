@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h3>{{ __('Setting') }}</h3>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        {{-- <a class="btn btn-primary btn-modal" href="#" data-href="{{ route('admin.grade.create') }}" data-toggle="modal" data-container=".modal_form">
                        <i class=" fa fa-plus-circle"></i>
                        {{ __('Add New') }}
                    </a> --}}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <form action="{{ route('admin.setting.general.save') }}" method="post">
                    @csrf
                    @method('PUT')
                    @include('admin.setting.partials._tab')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header d-flex p-0">
                                    <h3 class="card-title p-3">{{ __('General setting') }}</h3>
                                    {{-- <span class="badge bg-warning total-count">{{ $grades->total() }}</span> --}}
                                    <ul class="nav nav-pills ml-auto p-2">
                                        @foreach (json_decode($language, true) as $key => $lang)
                                            @if ($lang['status'] == 1)
                                                <li class="nav-item">
                                                    <a class="nav-link grade-tab text-uppercase @if ($lang['code'] == $default_lang) active @endif"
                                                        href="#lang_{{ $lang['code'] }}" data-toggle="pill" role="tab"
                                                        data-lang="{{ $lang['code'] }}"
                                                        aria-controls="lang_{{ $lang['code'] }}"
                                                        aria-selected="false">{{ \App\Helper\AppHelper::get_language_name($lang['code']) . '(' . strtoupper($lang['code']) . ')' }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content p-0">

                                        @foreach (json_decode($language, true) as $key => $lang)
                                            @if ($lang['status'] == 1)
                                                @php
                                                    $translate = [];
                                                    foreach ($settings as $setting) {
                                                        if (count($setting['translations'])) {
                                                            // dd($setting['translations']);

                                                            foreach ($setting['translations'] as $t) {
                                                                // dd($t);
                                                                if ($t->locale == $lang['code'] && $t->key == 'about_us') {
                                                                    $translate[$lang['code']]['about_us'] = $t->value;
                                                                }
                                                                if ($t->locale == $lang['code'] && $t->key == 'vision') {
                                                                    $translate[$lang['code']]['vision'] = $t->value;
                                                                }
                                                                if ($t->locale == $lang['code'] && $t->key == 'app_name') {
                                                                    $translate[$lang['code']]['app_name'] = $t->value;
                                                                }
                                                                if ($t->locale == $lang['code'] && $t->key == 'copyright') {
                                                                    $translate[$lang['code']]['copyright'] = $t->value;
                                                                }
                                                                if ($t->locale == $lang['code'] && $t->key == 'desc') {
                                                                    $translate[$lang['code']]['desc'] = $t->value;
                                                                }
                                                                if ($t->locale == $lang['code'] && $t->key == 'banner_text') {
                                                                    $translate[$lang['code']]['banner_text'] = $t->value;
                                                                }
                                                            }
                                                        }
                                                    }
                                                @endphp
                                                <div class="tab-pane fade {{ $lang['code'] == $default_lang ? 'show active' : '' }}"
                                                    id="lang_{{ $lang['code'] }}" role="tabpanel"
                                                    aria-labelledby="flamingo-tab">
                                                    <div class="row">
                                                        <input type="hidden" name="lang[]" value="{{ $lang['code'] }}">
                                                        <div class="form-group col-md-6 ">
                                                            <label for="banner_text_{{ $lang['code'] }}">{{ __('Banner Text') }}
                                                                ({{ strtoupper($lang['code']) }})
                                                            </label>
                                                            <input name="banner_text[]" id="banner_text_{{ $lang['code'] }}"
                                                                value="{{ $translate[$lang['code']]['banner_text'] ?? $banner_text }}"
                                                                type="text" class="form-control" />
                                                        </div>
                                                        @if ($lang['code'] == 'en')
                                                            <div class="form-group col-md-6">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="exampleInputFile">{{ __('Banner Images') }}</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="hidden" name="banner_image_names"
                                                                                class="banner_image_names_hidden">
                                                                            <input type="file" class="custom-file-input"
                                                                                id="exampleInputFile" name="image"
                                                                                accept="image/png, image/jpeg">
                                                                            <label class="custom-file-label"
                                                                                for="exampleInputFile">{{ __('Choose file') }}</label>
                                                                        </div>
                                                                    </div>
                                                                    @php
                                                                        if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
                                                                            $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
                                                                        } else {
                                                                            $image_url = asset('images/default.svg');
                                                                        }
                                                                    @endphp
                                                                    <div class="preview preview-multiple text-center border rounded mt-2"
                                                                        style="height: 150px">
                                                                        <img src="{{ $image_url }}" alt=""
                                                                            height="100%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-12 mb-2">
                                                            <h5>{{ __('App Name') }}</h5>
                                                        </div>
                                                        <div class="form-group col-12 ">
                                                            <label for="app_name_{{ $lang['code'] }}">{{ __('App Name') }}
                                                                ({{ strtoupper($lang['code']) }})
                                                            </label>
                                                            <input name="app_name[]" id="app_name_{{ $lang['code'] }}"
                                                                value="{{ $translate[$lang['code']]['app_name'] ?? $app_name }}"
                                                                type="text" class="form-control" />
                                                        </div>

                                                        <div class="col-12 mb-2 mt-3">
                                                            <h5>{{ __('Student Activity') }}</h5>
                                                        </div>
                                                        <div class="form-group col-12 col-sm-6 ">
                                                            <label for="desc">{{ __('Description') }}
                                                            ({{ strtoupper($lang['code']) }})</label>
                                                            <textarea name="desc[]" id="desc_{{ $lang['code'] }}" rows="2" class="form-control ">{{ $translate[$lang['code']]['desc'] ?? $desc }}</textarea>
                                                        </div>
                                                        @if ($lang['code'] == 'en')
                                                            <div class="form-group col-12 col-sm-6 ">
                                                                <label for="url">{{ __('Video Url') }}</label>
                                                                <input name="url" id="url"
                                                                    value="{{ $url }}" type="text"
                                                                    class="form-control" />
                                                            </div>
                                                        @endif
                                                        @if ($lang['code'] == 'en')
                                                            <div class="form-group col-md-6">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="exampleInputFile">{{ __('Student Activity Images') }}</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="hidden" name="active_image_names"
                                                                                class="active_image_names_hidden">
                                                                            <input type="file" class="custom-file-input"
                                                                                id="exampleInputFile" name="image"
                                                                                accept="image/png, image/jpeg">
                                                                            <label class="custom-file-label"
                                                                                for="exampleInputFile">{{ __('Choose file') }}</label>
                                                                        </div>
                                                                    </div>
                                                                    @php
                                                                        if (!empty($active_image_names) && file_exists(public_path('upload/settings/' . $active_image_names))) {
                                                                            $image_url = asset('/upload/settings/' . rawurlencode($active_image_names));
                                                                        } else {
                                                                            $image_url = asset('images/default.svg');
                                                                        }
                                                                    @endphp
                                                                    <div class="preview preview-multiple text-center border rounded mt-2"
                                                                        style="height: 150px">
                                                                        <img src="{{ $image_url }}" alt=""
                                                                            height="100%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-12 mb-2 mt-3">
                                                            <h5 for="about_us_{{ $lang['code'] }}">{{ __('About Us') }}
                                                                ({{ strtoupper($lang['code']) }})</h5>
                                                        </div>
                                                        <div class="form-group col-12 ">
                                                            <label for="about_us_{{ $lang['code'] }}">{{ __('About Us') }}
                                                                ({{ strtoupper($lang['code']) }})</label>
                                                            <textarea name="about_us[]" id="about_us_{{ $lang['code'] }}" rows="10" class="form-control summernote">{{ $translate[$lang['code']]['about_us'] ?? $about_us }}</textarea>
                                                        </div>
                                                        @if ($lang['code'] == 'en')
                                                            <div class="form-group col-md-6">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="exampleInputFile">{{ __('AboutImages') }}</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="hidden" name="about_image_names"
                                                                                class="about_image_names_hidden">
                                                                            <input type="file" class="custom-file-input"
                                                                                id="exampleInputFile" name="image"
                                                                                accept="image/png, image/jpeg">
                                                                            <label class="custom-file-label"
                                                                                for="exampleInputFile">{{ __('Choose file') }}</label>
                                                                        </div>
                                                                    </div>
                                                                    @php
                                                                        if (!empty($about_image_names) && file_exists(public_path('upload/settings/' . $about_image_names))) {
                                                                            $image_url = asset('/upload/settings/' . rawurlencode($about_image_names));
                                                                        } else {
                                                                            $image_url = asset('images/default.svg');
                                                                        }
                                                                    @endphp
                                                                    <div class="preview preview-multiple text-center border rounded mt-2"
                                                                        style="height: 150px">
                                                                        <img src="{{ $image_url }}" alt=""
                                                                            height="100%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-12 mb-2 mt-3">
                                                            <h5 for="vision_{{ $lang['code'] }}">{{ __('Vision') }}
                                                                ({{ strtoupper($lang['code']) }})</label>
                                                        </div>
                                                        <div class="form-group col-12 ">
                                                            <label for="vision_{{ $lang['code'] }}">{{ __('Vision') }}
                                                                ({{ strtoupper($lang['code']) }})</label>
                                                            <textarea name="vision[]" id="vision_{{ $lang['code'] }}" rows="10" class="form-control summernote">{{ $translate[$lang['code']]['vision'] ?? $vision }}</textarea>
                                                        </div>
                                                        @if ($lang['code'] == 'en')
                                                            <div class="form-group col-md-6">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="exampleInputFile">{{ __('Vission Images') }}</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="hidden" name="vission_image_names"
                                                                                class="vission_image_names_hidden">
                                                                            <input type="file" class="custom-file-input"
                                                                                id="exampleInputFile" name="image"
                                                                                accept="image/png, image/jpeg">
                                                                            <label class="custom-file-label"
                                                                                for="exampleInputFile">{{ __('Choose file') }}</label>
                                                                        </div>
                                                                    </div>
                                                                    @php
                                                                        if (!empty($vission_image_names) && file_exists(public_path('upload/settings/' . $vission_image_names))) {
                                                                            $image_url = asset('/upload/settings/' . rawurlencode($vission_image_names));
                                                                        } else {
                                                                            $image_url = asset('images/default.svg');
                                                                        }
                                                                    @endphp
                                                                    <div class="preview preview-multiple text-center border rounded mt-2"
                                                                        style="height: 150px">
                                                                        <img src="{{ $image_url }}" alt=""
                                                                            height="100%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-12 mb-2 mt-3">
                                                            <h5 for="copyright_{{ $lang['code'] }}">{{ __('Copyright') }}
                                                                ({{ strtoupper($lang['code']) }})</label>
                                                        </div>
                                                        <div class="form-group col-12 ">
                                                            <label
                                                                for="copyright_{{ $lang['code'] }}">{{ __('Copyright') }}
                                                                ({{ strtoupper($lang['code']) }})</label>
                                                            <input name="copyright[]" id="copyright_{{ $lang['code'] }}"
                                                                value="{{ $translate[$lang['code']]['copyright'] ?? $copyright }}"
                                                                type="text" class="form-control" />
                                                        </div>
                                                        {{-- Contact and Address --}}
                                                        @if ($lang['code'] == 'en')
                                                            <div class="col-12 mb-2 mt-3">
                                                                <h5>{{ __('Contact') }}</label>
                                                            </div>
                                                            <div class="form-group col-12 ">
                                                                <label for="address">{{ __('Address') }}</label>
                                                                <input name="address" id="address"
                                                                    value="{{ $address }}" type="text"
                                                                    class="form-control" />
                                                            </div>

                                                            <div class="form-group col-12">
                                                                <label for="phone_number">{{ __('Phone Number') }}</label>
                                                                <input name="phone_number" value="{{ $phone_number }}"
                                                                    type="text" class="form-control "
                                                                    id="" />
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <label
                                                                    for="phone_number1">{{ __('Phone Number') }}</label>
                                                                <input name="phone_number1" value="{{ $phone_number1 }}"
                                                                    type="text" class="form-control "
                                                                    id="" />
                                                            </div>

                                                            <div class="form-group col-12 ">
                                                                <label for="email">{{ __('Email') }}</label>
                                                                <input name="email" id="email"
                                                                    value="{{ $email }}" type="text"
                                                                    class="form-control" />
                                                            </div>


                                                            {{-- Contact and Address --}}

                                                            {{-- Socail media --}}
                                                            <div class="col-12 mb-2 mt-3">
                                                                <h5>{{ __('Social Media') }}</label>
                                                            </div>

                                                            <div class="form-group col-12 col-sm-6 ">
                                                                <label for="facebook">{{ __('Facebook Url') }}</label>
                                                                <input name="facebook" id="facebook"
                                                                    value="{{ $facebook }}" type="text"
                                                                    class="form-control" />
                                                            </div>
                                                            <div class="form-group col-12 col-sm-6 ">
                                                                <label for="telegram">{{ __('Telegram Url') }}</label>
                                                                <input name="telegram" id="telegram"
                                                                    value="{{ $telegram }}" type="text"
                                                                    class="form-control" />
                                                            </div>
                                                        @endif

                                                        {{-- Contact and Address --}}
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                {{-- table --}}
                                {{-- @include('admin.grade.partials._table') --}}


                            </div>
                            <div class="row">
                                <div class="col-12 my-3">
                                    <button type="submit"
                                        class="btn btn-primary float-right">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>
    </div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.btn-modal', function() {
            $("div.modal_form").load($(this).data('href'), function() {

                $(this).modal('show');

            });
        });

        $(document).on('click', '.btn-edit', function() {
            $("div.modal_form").load($(this).data('href'), function() {

                $(this).modal('show');

            });
        });

        $('.submit').click(function(e) {
            e.preventDefault();
            // alert('ok');
            // console.log('ok');
        });

        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function(response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            });
        });
        var input = document.querySelector("#phone_number");
        var iti = window.intlTelInput(input, {
            initialCountry: "kh",
            separateDialCode: true,
            hiddenInput: "phone_number",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "us";
                    callback(countryCode);
                });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
            nationalMode: false,
            autoPlaceholder: "aggressive"

        });
        var input = document.querySelector("#phone_number1");
        var iti = window.intlTelInput(input, {
            initialCountry: "kh",
            separateDialCode: true,
            hiddenInput: "phone_number1",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "us";
                    callback(countryCode);
                });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
            nationalMode: false,
            autoPlaceholder: "aggressive"

        });


        const compressor = new window.Compress();
        $('.custom-file-input').change(function(e) {
            compressor.compress([...e.target.files], {
                size: 4,
                quality: 0.75,
            }).then((output) => {
                var files = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                var formData = new FormData();

                var image_names_hidden = $(this).closest('.custom-file').find('input[type=hidden]');
                var container = $(this).closest('.form-group').find('.preview');
                if (container.find('img').attr('src') === `{{ asset('images/default.svg') }}`) {
                    container.empty();
                }
                var _token = `{{ csrf_token() }}`;
                formData.append('image', files);
                formData.append('_token', _token);

                $.ajax({
                    url: "{{ route('save_temp_file') }}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 0) {
                            toastr.error(response.msg);
                        }
                        if (response.status == 1) {
                            container.empty();
                            var temp_file = response.temp_files;
                            var img_container = $('<div></div>').addClass('img_container');
                            var img = $('<img>').attr('src', "{{ asset('upload/temp') }}" +
                                '/' + temp_file);
                            img_container.append(img);
                            container.append(img_container);

                            var new_file_name = temp_file;
                            console.log(new_file_name);

                            image_names_hidden.val(new_file_name);
                        }
                    }
                });
            });
        });
    </script>
@endpush
