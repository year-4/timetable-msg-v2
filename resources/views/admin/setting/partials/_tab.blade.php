<div class="row">
    <div class="col-12 col-md-8 col-lg-6 mb-3">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('admin.setting.index')) active @endif" href="{{ route('admin.setting.index') }}"
                    aria-controls="pills-flamingo" aria-selected="true">{{ __('Timetable') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('admin.setting.general.index')) active @endif" href="{{ route('admin.setting.general.index') }}"
                    aria-controls="pills-cuckoo" aria-selected="false">{{ __('Front Website') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('admin.setting.language*')) active @endif" href="{{ route('admin.setting.language.index') }}"
                    aria-controls="pills-cuckoo" aria-selected="false">{{ __('Language') }}</a>
            </li>
        </ul>
    </div>
</div>
