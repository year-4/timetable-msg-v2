@extends('layouts.app')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{ __('Dashboard') }}</h1>
                    </div>

                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-white">
                            <div class="icon">
                                <div class="widget-icon rounded-circle position-absolute text-info">
                                    <span>
                                        <img src="{{ asset('images/teacher_icon.png') }}" width="100%" height="100%"
                                            style="object-fit: cover">
                                    </span>

                                </div>

                            </div>
                            <div class="inner">
                                <h3>{{ $teachersCount }}</h3>
                                <p>{{ __('Teacher') }}</p>
                                <a href="{{ route('admin.teacher.index') }}"><span>{{__('Show all')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-white">
                            <div class="icon">
                                <div class="widget-icon rounded-circle position-absolute text-info">
                                    <img src="{{ asset('images/academic.png') }}" width="100%" height="100%"
                                        style="object-fit: cover">
                                </div>
                            </div>
                            <div class="inner">
                                <h3>{{ $academicyearsCount }}</h3>
                                <p>{{ __('Academic Year') }}</p>
                                <a href="{{ route('admin.academic_year.index') }}"><span>{{__('Show all')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-white">
                            <div class="icon">
                                <div class="widget-icon rounded-circle position-absolute text-info">
                                    <img src="{{ asset('images/classroom.png') }}" width="100%" height="100%"
                                        style="object-fit: cover">
                                </div>
                            </div>
                            <div class="inner">
                                <h3>{{ $classRoomsCount }}</h3>
                                <p>{{ __('Class Room') }}</p>
                                <a href="{{ route('admin.class_room.index') }}"><span>{{__('Show all')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-white">
                            <div class="icon">
                                <div class="widget-icon rounded-circle position-absolute text-info">
                                    <img src="{{ asset('images/timetable.png') }}" width="100%" height="100%"
                                        style="object-fit: cover">
                                </div>
                            </div>
                            <div class="inner">
                                <h3>{{ $timetableCount }}</h3>
                                <p>{{ __('Timetable') }}</p>
                                <a href="{{ route('admin.timetable.index') }}"><span>{{__('Show all')}}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex">
                        <div class="card flex-fill">
                            {{-- <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <div class="page-title">
                                            <h5>Users Report</h5>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="card-body">
                                <div id="user-chart"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div id="comparison-chart"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-flex">
                        <div class="card flex-fill">
                            {{-- <div class="card-header">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <div class="page-title">
                                            <h5>Teacher Report</h5>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="card-body">
                                <div id="teacher-chart"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
    </div>

    @push('script')
        <!-- Include Highcharts library -->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <script type="text/javascript">
            var userData = @json($userData);
            var allYears = @json($allYears);
            var teacherSeries = @json($teacherSeries);
            var teacherLabels = @json($teacherLabels);
            var selectedYear;
            var timetableCount = <?= $timetableCount ?>;
            var classRoomsCount = <?= $classRoomsCount ?>;
            var difference = classRoomsCount - timetableCount;
            
            var userChart = Highcharts.chart('user-chart', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: "{{ __('Total Users') }}"
                },
                // subtitle: {
                //     text:
                //         'Source: <a target="_blank" ' +
                //         'href="https://www.indexmundi.com/agriculture/?commodity=corn">indexmundi</a>',
                //     align: 'left'
                // },
                xAxis: {
                    categories: ['{{__('January')}}', '{{__('February')}}', '{{__('March')}}', '{{__('April')}}', '{{__('May')}}', '{{__('June')}}', '{{__('July')}}', '{{__('August')}}', '{{__('September')}}',
                        '{{__('October')}}', '{{__('November')}}', '{{__('December')}}'
                    ],
                    crosshair: true,
                    accessibility: {
                        description: 'Countries'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '{{__('Number of New Users')}}'
                    }
                },
                tooltip: {
                    valueSuffix: ' users'
                },
                plotOptions:     {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: '{{__('New users')}}',
                    data: <?php echo json_encode($userSeries); ?>
                }, ],
            });


            // Create teacher chart
            var teacherChart = Highcharts.chart('teacher-chart', {
                chart: {
                    type: 'areaspline',
                    scrollablePlotArea: {
                        minWidth: 600,
                        scrollPositionX: 1
                    }
                },
                title: {
                    text: "{{ __('Total Teacher') }}"
                },
                // subtitle: {
                //     text:
                //         'Source: <a target="_blank" ' +
                //         'href="https://www.indexmundi.com/agriculture/?commodity=corn">indexmundi</a>',
                //     align: 'left'
                // },
                xAxis: {
                    categories: <?php echo json_encode(array_keys($teacherSeries)); ?>,
                    // crosshair: true,
                    accessibility: {
                        description: 'Teacher'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '{{__('Number of Teacher')}}'
                    }
                },
                // tooltip: {
                //     valueSuffix: ' Teachers'
                // },
                plotOptions:     {
                    areaspline: {
                        color: '#7a51be',
                        fillColor: {
                            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                            stops: [
                                [0, '#7B51BECF'],
                                [1, '#002eff1f']
                            ]
                        },
                        threshold: null,
                        marker: {
                            enabled: false
                            // lineWidth: 1,
                            // lineColor: null,
                            // fillColor: 'white'
                        }
                    }
                },
                series: [{
                    name: '{{__ ('Total Teachers')}}',
                    data: <?php echo json_encode(array_values($teacherSeries)); ?>
                }, ],
            });
            
            
            Highcharts.chart('comparison-chart', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: "{{ __('Complete Timetables') }}"
                },
                series: [{
                    name: 'Total',
                    data: [{
                            name: '{{__('Completed')}}',
                            y: timetableCount
                        },
                        // {
                        //     name: 'Class Rooms',
                        //     y: classRoomsCount - difference // Adjust classRoomsCount to show the difference
                        // },
                        {
                            name: '{{__('Pending')}}',
                            y: difference,
                            color: 'gray' // Adjust color as needed
                        }
                    ]
                }],
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}%)'
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: true
                }
            });
            // Function to update the chart title based on the selected year
            function updateChartTitle(year) {
                selectedYear = year;

                // Update user chart title
                userChart.setTitle({
                    text: 'New User Growth, ' + (selectedYear ? selectedYear : 'All Years')
                });

                // Update teacher chart title
                teacherChart.setTitle({
                    text: 'Teacher Growth, ' + (selectedYear ? selectedYear : 'All Years')
                });
            }
        </script>
    @endpush
@endsection
