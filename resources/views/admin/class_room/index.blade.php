@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>{{ __('Class Room') }}</h3>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    {{-- <a class="btn btn-primary btn-add" href="#" data-href="{{ route('admin.class_room.create') }}" data-toggle="modal" data-target=".modal_form">
                        <i class="fa fa-plus-circle"></i>
                        {{ __('Add New') }}
                    </a> --}}
                    <a href="#" data-href="{{ route('admin.class_room.create') }}" data-container=".modal_form" class="btn btn-primary btn-add">
                        <i class="fa fa-plus-circle"></i>
                        {{ __('Add New') }}
                    </a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('_message')
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Class Room List') }}</h3>
                            <span class="badge bg-warning total-count">{{ $classRooms->total() }}</span>
                        </div>
                        <!-- /.card-header -->

                        {{-- table --}}
                        @include('admin.class_room.partials._table')

                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div> --}}
    <div class="modal fade modal_form" data-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"></div>
</div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.btn-add', function(){
            $("div.modal_form").load($(this).data('href'), function(){
                $(this).modal('show');
            });
        });

        $(document).on('click', '.btn-edit', function(){
            $("div.modal_form").load($(this).data('href'), function(){
                $(this).modal('show');
            });
        });

        $('.submit').click(function (e) {
            e.preventDefault();
        });

        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        success: function (response) {
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            });
        });

        $(document).on('change', '.outstanding_class', function (e) {
            e.preventDefault();
            console.log('okk');
            if ($('.outstand_class_wrapper').hasClass('d-none')) {
                $('.outstand_class_wrapper').removeClass('d-none');
            } else {
                $('.outstand_class_wrapper').addClass('d-none');

            }
        });

        $(document).on('change', 'input.is_outstanding', function () {
            $.ajax({
                type: "get",
                url: "{{ route('admin.class_room.update_is_outstanding') }}",
                data: { "id" : $(this).data('id') },
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.status == 1) {
                        Toast.fire({
                                icon: 'success',
                                title: response.msg
                            });
                    } else {
                        Toast.fire({
                                icon: 'error',
                                title: response.msg
                            });
                    }
                }
            });
        });
    </script>
@endpush
