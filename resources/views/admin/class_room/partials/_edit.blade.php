<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Edit Classroom') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.class_room.update', $classRoom->id) }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">{{ __('Name') }}</label>
                    <input type="text" name="name" value="{{ $classRoom->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="grade">{{ __('Grade') }}</label>
                    <select name="grade_id" class="form-control" id="grade" required>
                        <option value="">Select Grade</option>
                        @foreach ($grades as $grade)
                            <option value="{{ $grade->id }}" {{ $grade->id == $classRoom->grade_id ? 'selected' : '' }}>{{ $grade->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="teacher_id">{{ __('Teacher') }}</label>
                    <select name="teacher_id" class="form-control select2-modal">
                        <option value="">{{ __('Select Teacher') }}</option>
                        @foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}" {{ $teacher->id == $classRoom->teacher_id ? 'selected' : '' }}>{{ $teacher->username }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group type_wrapper @if($classRoom->grade->name != 12) d-none @endif">
                    <label for="type">{{ __('Type') }}</label>
                    <select name="type" class="form-control">
                        <option value="">{{ __('Select Type') }}</option>
                        @foreach ($types as $key => $text)
                            <option value="{{ $key }}" @if ($key == $classRoom->type) selected @endif>{{ $text }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <div class="icheck-primary d-inline">
                        <input type="checkbox" id="outstanding_class" name="is_outstanding_class" class="outstanding_class" @if($classRoom->is_outstanding_class == 1) checked @endif value="1">
                        <label for="outstanding_class">
                            {{ __('Outstanding Class') }}
                        </label>
                        <br>
                        <span class=" font-italic text-secondary">{{ __('Check to add this class as a outstanding class.') }}</span>
                    </div>
                </div>

                <div class="outstand_class_wrapper @if($classRoom->is_outstanding_class != 1) d-none @endif">
                    <div class="form-group ">
                        <label for="outstanding_subject">{{ __('Outstand Subject') }}</label>
                        <select name="outstanding_subject" class="form-control select2-modal">
                            <option value="">{{ __('Select Outstand Subject') }}</option>
                            @foreach ($subjects as $id => $name)
                                <option value="{{ $id }}" @if($classRoom->outstanding_subject == $id) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="extra_hours">{{ __('Extra Hours') }}</label>
                        <input type="number" name="extra_hours" id="extra_hours" class="form-control" min="0" step="1" value="{{ $classRoom->extra_hours }}">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.select2-modal').select2({
            theme: 'bootstrap4',
            dropdownParent: $('.modal_form')
        });
    });
    $(document).on('change', 'select#grade', function (e) {
        e.preventDefault();
        var grade = $(this).find('option:selected').text();
        if (grade == '12') {
            $('.type_wrapper').removeClass('d-none');
        } else {
            $('.type_wrapper').addClass('d-none');
        }
    });

    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({
            type: "post",
            url: `{{ route('admin.class_room.update', $classRoom->id) }}`,
            data: data,
            // dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    $('div.modal_form').modal('hide');
                    $('.table-wrapper').replaceWith(response.view);
                    $('.total-count').html(response.total);

                    Toast.fire({
                        icon: 'success',
                        title: `{{ __('Updated successfully') }}`
                    });
                }
            }
        });
    });
</script>
