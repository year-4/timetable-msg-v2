<div class="card-body p-0 table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>{{ __('Grade') }}</th>
                <th>{{ __('Teacher') }}</th>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Is oustanding class') }}</th>
                <th>{{ __('Created date') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($classRooms as $classroom)
                <tr>
                    <td>{{ $classroom->grade->name }}</td>
                    <td>{{ optional($classroom->teacher)->username }}</td>
                    <td>{{ $classroom->name }}</td>
                    <td>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input switcher_input is_outstanding" id="is_outstanding_{{ $classroom->id }}" data-id="{{ $classroom->id }}"
                            @if($classroom->is_outstanding_class == 1)
                            checked
                            @endif
                            name="is_outstanding">
                            <label class="custom-control-label" for="is_outstanding_{{ $classroom->id }}"></label>
                        </div>
                    </td>
                    <td>
                        @if ($classroom->created_at)
                            {{ $classroom->created_at->format('d M Y h:i A') }}
                        @else
                            N/A
                        @endif
                    </td>

                    <td>
                        @if (auth()->user()->can('class_room.edit'))
                            <a href="#" data-href="{{ route('admin.class_room.edit', $classroom->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-sm btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        @endif
                        @if (auth()->user()->can('class_room.delete'))
                            <form action="{{ route('admin.class_room.destroy', $classroom->id) }}"
                                class="d-inline-block form-delete-{{ $classroom->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $classroom->id }}"
                                    data-href="{{ route('admin.class_room.destroy', $classroom->id) }}"
                                    class="btn btn-danger btn-sm btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $classRooms->firstItem() }} {{ __('to') }}
                    {{ $classRooms->lastItem() }} {{ __('of') }} {{ $classRooms->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3">
                    {{ $classRooms->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
