@extends('layouts.app')

@push('style')
<style media="print">
    .content-header {
        display: none;
    }

    .main-footer {
        display: none;
    }

    /* Set paper size and orientation for landscape A4 */
    @page {
        size: A4 portrait;
        margin: 20px;
        page-break-after: always; /* Add this line to force page break after each section */
    }

    /* Additional style to hide the unnecessary content for printing */
    .no-print {
        display: none;
    }
</style>
@endpush
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{ __('Timetable') }}</h1>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        <a href="{{ route('admin.timetable.index') }}"class="btn btn-primary">{{ __('Back') }}</a>
                        {{-- <button class="btn btn-primary" onclick="printTimetable()">Print {{ __('Timetable') }}</button> --}}
                        <button class="btn btn-primary" onclick="printTimetable()">Print Section</button>

                        {{-- <button class="btn btn-primary" onclick="downloadTimetableImage()">Download Image</button> --}}
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content timetable-detail">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            {{-- <div class="card-header">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-md-2 mx-auto">
                                            <!-- Add your logo image here -->
                                            <img src="{{ asset('front/images/logo.png') }}" alt="Logo" height="50" width="50">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="card-body">
                                <div class="row justify-content-center mb-3">
                                    <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="0">
                                        <img src="{{ asset('front/images/logo.png') }}" alt="MyApp Logo" style="width: 40px;">
                                        <h3 class="text-center mb-4">{{ session('app_name') ?? null }}</h3>
                                    </div>
                                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                                        <span class=" mb-4">{{ __('Class') }}: {{ @$class->name }}</span>
                                    </div>
                                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0">
                                        <span class="float-right mb-4 ">{{ __('Teacher Name') }}: {{ @$class->teacher->full_name }}</span>
                                    </div>
                                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0" >
                                        <span class="mb-4 " >{{ __('Academic Year') }}: {{ @$year->name }}</span>
                                    </div>
                                    <div class="col-6 " data-aos="fade-up" data-aos-delay="0" >
                                        <span class="mb-4 float-right " >{{ __('Phone Number') }}: {{ @$class->teacher->phone }}</span>
                                    </div>

                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">{{ __('Time') }}</th>
                                                @foreach ($week_days as $day)
                                                    <th class="text-center">{{ $day }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th colspan="8" class="text-center">{{ __('Morning') }}</th>
                                            </tr>
                                            @foreach ($time_slots as $time)
                                                @if ($time->daytime == 'morning')
                                                    <tr>
                                                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                                                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                                                        @foreach ($week_days as $day)
                                                            @php
                                                                $timetable_slots = $timetable_slots ?? null;
                                                                $timetable_slot = null;
                                                                if (@$timetable_slots != null) {
                                                                    $timetable_slot = $timetable_slots
                                                                        ->where('time_slot_id', $time->id)
                                                                        ->where('week_day', $day)
                                                                        ->first();
                                                                }
                                                            @endphp
                                                            <td class="text-center"
                                                                id="{{ $day . '-' . $time->id }}">

                                                                @if ($timetable_slot)
                                                                    <span
                                                                        class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                                                    <br>
                                                                    {{ $timetable_slot->teacher->full_name }}
                                                                @endif
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <th colspan="8" class="text-center">Evening</th>
                                            </tr>
                                            @foreach ($time_slots as $time)
                                                @if ($time->daytime == 'afternoon')
                                                    <tr>
                                                        <td>{{ date('H:i', strtotime($time->start_time)) }} -
                                                            {{ date('H:i', strtotime($time->end_time)) }}</td>
                                                        @foreach ($week_days as $day)
                                                            @php
                                                                $timetable_slot = null;
                                                                if ($timetable_slots) {
                                                                    $timetable_slot = $timetable_slots
                                                                        ->where('time_slot_id', $time->id)
                                                                        ->where('week_day', $day)
                                                                        ->first();
                                                                }
                                                            @endphp
                                                            <td class="text-center {{-- position-relative timetble-slot-td --}}"
                                                                id="{{ $day . '-' . $time->id }}">
                                                                <input type="hidden" name="week_day" class="week_day"
                                                                    value="{{ $day }}">
                                                                <input type="hidden" name="time_slots" class="time_slot"
                                                                    value="{{ $time->id }}">
                                                                @if ($timetable_slot)
                                                                    <span
                                                                        class=" text-bold">{{ $timetable_slot->subject->name }}</span>
                                                                    <br>
                                                                    {{ $timetable_slot->teacher->full_name }}
                                                                    <a class="btn btn-edit-timetable-slot btn-outline-primary btn-modal d-none"
                                                                        href="#"
                                                                        data-href="{{ trim(route('admin.timetable.showPopupEdit', $timetable_slot->id)) }}"
                                                                        data-toggle="modal" data-container=".modal_form">
                                                                        <span>
                                                                            {{ __('Edit') }}
                                                                        </span>
                                                                    </a>
                                                                @endif
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
@endsection

@push('script')
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script>
    function printTimetable() {
        window.print();
    }

    function printTimetableSection() {
        window.print();
    }

    function downloadTimetableImage() {
        html2canvas(document.querySelector(".timetable-detail"), {
            width: 842, // A4 paper width in pixels
            height: 595, // A4 paper height in pixels
            scrollX: 0,
            scrollY: -window.scrollY
        }).then(function (canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL();
            link.download = 'timetable.png';
            link.click();
        });
    }
</script>
@endpush
