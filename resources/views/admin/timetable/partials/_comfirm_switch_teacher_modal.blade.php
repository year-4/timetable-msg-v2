<div class="modal-dialog modal-dialog-centered">
    <form action="{{ route('admin.timetable_slot.update', $timetable_slot->id) }}" method="POST" class="comfirm_form">
        @csrf
        @method('PUT')
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none">
            </div>
            <div class="modal-body">
                <input type="hidden" name="is_confirmed" value="1">
                @if ($switch_teacher_input)
                    <input type="hidden" name="academic_year_id" value="{{ $switch_teacher_input['academic_year_id'] }}">
                    <input type="hidden" name="grade_id" value="{{ $switch_teacher_input['grade_id'] }}">
                    <input type="hidden" name="class_id" value="{{ $switch_teacher_input['class_id'] }}">
                    <input type="hidden" name="week_day" value="{{ $switch_teacher_input['week_day'] }}">
                    <input type="hidden" name="time_slot" value="{{ $switch_teacher_input['time_slot'] }}">
                    <input type="hidden" name="subject" value="{{ $switch_teacher_input['subject'] }}">
                    <input type="hidden" name="teacher" value="{{ $switch_teacher_input['teacher'] }}">

                @endif
                <div class="text-center mb-3">
                    <img src="{{ asset('images/switch-teacher.png') }}" alt="switch-teacher" width="70px">
                </div>
                <h3 class="text-center">{{ __('Are you sure ?') }}</h3>
                <p class="text-center" style="font-size: 1.1rem">
                    <span>
                        {{ __('The select teacher is not available') }}
                    </span>
                    <br>
                    <span>
                        {{ __('Click "CONTINUE" to swith current teacher with the selected teacher') }}
                    </span>
                </p>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('CANCEL') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('CONTINUE') }}</button>
            </div>
        </div>
    </form>
</div>

{{-- @push('script') --}}
    <script>
        $('.submit').click(function (e) {
            e.preventDefault();
            var data = $('.comfirm_form').serialize();
            $.ajax({
                type: "post",
                url: `{{ route('admin.timetable_slot.update', $timetable_slot->id) }}`,
                data: data,
                success: function (response) {
                    console.log(response);
                    if (response.errors) {
                        for (var i = 0; i < response.errors.length; i++) {
                            Toast.fire({
                                icon: 'error',
                                title: response.errors[i].message,
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'success',
                            title: `{{ __('Updated successfully') }}`,
                            // message: ,
                        });

                        $('div.modal_comfirm').modal('hide');
                        // $('.table-wrapper').replaceWith(response.view);
                        $(`#${response.id}`).replaceWith(response.view);
                        // $('.total-count').html(response.total);
                    }

                }
            });
        });
    </script>
{{-- @endpush --}}
