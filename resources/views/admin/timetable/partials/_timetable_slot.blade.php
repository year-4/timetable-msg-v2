<td class="text-center position-relative timetble-slot-td" id="{{ $day . '-' . $time_id }}">
    <input type="hidden" name="week_day" class="week_day" value="{{ $day }}">
    <input type="hidden" name="time_slots" class="time_slot" value="{{ $time_id }}">

    <span class=" text-bold">{{ $timetable_slot->subject->name }}</span>
        <br>
        {{ $timetable_slot->teacher->full_name }}
        <a class="btn btn-edit-timetable-slot btn-outline-primary btn-modal d-none" href="#"
            data-href="{{ trim(route('admin.timetable.showPopupEdit', $timetable_slot->id)) }}"
            data-toggle="modal" data-container=".modal_form">
            <span>
                {{ __('Edit') }}
            </span>
        </a>

    {{-- <a class="btn btn-outline-success btn-modal rounded-circle btn_circle" href="#"
        data-href="{{ trim(@$timetable_slot ? route('admin.timetable.showPopupEdit', $timetable_slot->id) : route('admin.timetable.showPopupCreate')) }}"
        data-toggle="modal" data-container=".modal_form">
        <i class=" fa fa-pencil-alt"></i>
    </a> --}}
</td>
