<div class="card-body p-0 table-wrapper">
    <table class="table table-striped">
        <thead>
            <tr>
                {{-- <th >#</th> --}}
                <th>{{ __('Name') }}</th>
                {{-- <th>Created by</th> --}}
                <th>{{ __('Created date') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            {{-- @foreach ($academicyears as $academicyear)
                <tr>
                
                    <td>{{$academicyear->name}}</td>
                    <td>{{ $academicyear->created_at->format('d M Y h:i A') }}</td>
                    <td>
                        <a href="#" data-href="{{ route('admin.timetable.edit', $academicyear->id) }}" data-container=".modal_form" class="btn btn-info btn-sm btn-edit">
                            <i class="fas fa-pencil-alt"></i>
                            {{ __('Edit') }}
                        </a>

                        <form action="{{ route('admin.timetable.destroy', $academicyear->id) }}" class="d-inline-block form-delete-{{ $academicyear->id }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" data-id="{{ $academicyear->id }}" data-href="{{ route('admin.timetable.destroy', $academicyear->id) }}" class="btn btn-danger btn-sm btn-delete">
                                <i class="fas fa-trash"></i>
                                {{ __('Delete') }}
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach --}}
        </tbody>
    </table>
    {{-- <div style="padding: 10px; float: right;">

        {{ $grades->links() }}
    </div> --}}

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            {{-- <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $academicyears->firstItem() }} {{ __('to') }} {{ $academicyears->lastItem() }} {{ __('of') }} {{ $academicyears->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $academicyears->links() }}</div>
            </div> --}}
        </div>
    </div>


</div>
