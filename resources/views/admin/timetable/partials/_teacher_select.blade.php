<div class="teacher_select_wrapper">
    <select name="teacher" id="teacher" class="form-control select2-modal teacher_select" style="width: 100%">
        <option value="" selected disabled>{{ __('Select Teacher') }}</option>
        @foreach ($teachers as $id => $teacher)
            <option value="{{ $id }}" @if(isset($timetable_slot) && $id == $timetable_slot->teacher_id) selected @endif>{{ $teacher }}</option>
        @endforeach
    </select>
</div>

