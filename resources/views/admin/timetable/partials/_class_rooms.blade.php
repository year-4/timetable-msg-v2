<div class="tab-pane class-rooms-wrapper" id="tab_1">
    <div class="row">
        {{-- @dd($class_rooms->pluck('name', 'id')) --}}
        @foreach ($class_rooms as $room)
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title font-weight-bold">
                            {{ __('Class') }} {{ $room->name ?? null }}</h5>
                    </div>
                    <div class="card-footer btn-group d-flex justify-content-center"
                        role="group">
                        @php
                            $timetable_id = 0;
                            if (($room->timetables)->isNotEmpty()) {
                                $timetable = $room->timetables->where('academic_year_id', request('academic_year_id'))->first();
                                // dd($timetable);
                                $timetable_id = $timetable->id ?? 0;
                            }
                        @endphp
                        {{-- @dd($timetable_id) --}}
                        <a href="{{ route('admin.timetable.show', $timetable_id) . '?academic_year_id=' . request('academic_year_id') . '&grade_id=' . $grade_id . '&class_id=' . $room->id }}" class="btn btn-primary">{{ __('View') }}</a>

                        <a href="{{ route('admin.timetable.create', [
                            'academic_year_id' => request('academic_year_id'),
                            'grade_id' => $grade_id,
                            'class_id' => $room->id,
                        ]) }}"
                            class="btn btn-danger">{{ __('Create') }}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
