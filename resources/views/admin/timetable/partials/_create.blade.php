<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Add Timetable') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <form action="{{ route('admin.subject_type.store') }}" class="submit-form" method="post">
            <div class="modal-body">
                @csrf
                <input type="hidden" name="academic_year_id" value="{{ $academic_year_id }}">
                <input type="hidden" name="grade_id" value="{{ $grade_id }}">
                <input type="hidden" name="class_id" value="{{ $class_id }}">
                <input type="hidden" name="week_day" value="{{ $week_day }}">
                <input type="hidden" name="time_slot" value="{{ $time_slot }}">
                <div class="form-group">
                    <label for="subject">{{ __('Subject') }}</label>
                    <select name="subject" id="subject" class="form-control select2-modal subject_select" style="width: 100%">
                        <option value="" selected disabled>{{ __('Select Suject') }}</option>
                        @foreach ($subjects as $id => $subject)
                            <option value="{{ $id }}">{{ $subject }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="teacher">{{ __('Teacher') }}</label>
                    @include('admin.timetable.partials._teacher_select')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                <button type="submit" class="btn btn-primary submit">{{ __('Save') }}</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.select2-modal').select2({
            theme: 'bootstrap4',
            dropdownParent: $('.modal_form')
        });
    });

    $(document).on('change', '.subject_select', function(e) {
        e.preventDefault();
        var subject_id = $(this).val();
        $.ajax({
            type: "get",
            url: window.location.href,
            data: {
                'subject_id' : subject_id
            },
            dataType: "json",
            success: function (response) {
                if (response.view) {
                    $('.teacher_select_wrapper').replaceWith(response.view);
                    $('.select2-modal').select2({
                        theme: 'bootstrap4',
                        dropdownParent: $('.modal_form')
                    });
                }
            }
        });
    });

    $('.submit').click(function (e) {
        e.preventDefault();
        var data = $('.submit-form').serialize();
        console.log(data);
        $.ajax({
            type: "post",
            url: `{{ route('admin.timetable_slot.store') }}`,
            data: data,
            success: function (response) {
                console.log(response)
                if (response.errors) {
                    for (var i = 0; i < response.errors.length; i++) {
                        Toast.fire({
                            icon: 'error',
                            title: response.errors[i].message,
                        });
                    }
                } else {
                    if (response.status == 'teacher_limit_warning') {
                        Confirmation.fire({
                            title: '{{ __("This teacher has reach the limit teach hour") }}',
                            text: "{{ __('Do you want to continue') }}",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Continue',
                            cancelButtonText: 'No, cancel',
                            reverseButtons: true
                        }).then((result) => {
                            if (result.isConfirmed) {
                                data = $('.submit-form').serialize() + '&ignore_teacher_limit=1';
                                console.log(data);
                                $.ajax({
                                    type: "post",
                                    url: `{{ route('admin.timetable_slot.store') }}`,
                                    data: data,
                                    success: function (response) {
                                        if (response.errors) {
                                            for (var i = 0; i < response.errors.length; i++) {
                                                Toast.fire({
                                                    icon: 'error',
                                                    title: response.errors[i].message,
                                                });
                                            }
                                        } else {
                                            Toast.fire({
                                                icon: 'success',
                                                title: `{{ __('Created successfully') }}`,
                                                // message: ,
                                            });
                                        }

                                        $('div.modal_form').modal('hide');
                                        // $('.table-wrapper').replaceWith(response.view);
                                        $(`#${response.id}`).replaceWith(response.view);

                                    }
                                });
                            }
                        });
                    } else {
                        Toast.fire({
                            icon: 'success',
                            title: `{{ __('Updated successfully') }}`,
                            // message: ,
                        });
                    }

                    $('div.modal_form').modal('hide');
                    // $('.table-wrapper').replaceWith(response.view);
                    $(`#${response.id}`).replaceWith(response.view);
                    // $('.total-count').html(response.total);

                }

            }
        });
    });
</script>
