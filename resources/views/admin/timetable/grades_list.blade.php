@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Timetable') }}</h1>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        {{-- <a href="{{ url('admin/timetable/welcome') }}" class="btn btn-primary">Welcome</a>
                        <a href="{{ url('admin/timetable/create') }}" class="btn btn-primary">Create</a> --}}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header d-flex p-0">
                                <h3 class="card-title p-3">{{ __('Grade') }}</h3>
                                <ul class="nav nav-pills ml-auto p-2">
                                    @foreach ($grades as $grade)
                                        <li class="nav-item">
                                            <a class="nav-link grade-tab @if($loop->iteration == 1) active @endif" href="#tab_1" data-toggle="tab" data-grade_id="{{ $grade->id }}">{{ $grade->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="">
                                    @include('admin.timetable.partials._class_rooms')
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.grade-tab', function (e) {
            e.preventDefault();
            var grade_id = $(this).data('grade_id');
            $.ajax({
                type: "get",
                url: window.location.href,
                data: { 'grade_id':grade_id },
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    if (response.view) {
                        $('.class-rooms-wrapper').replaceWith(response.view);
                    }
                }
            });
        })
    </script>
@endpush
