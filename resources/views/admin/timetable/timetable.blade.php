@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__('Timetable')}}</h1>
                    </div>
                    <div class="col-sm-6" style="text-align: right">
                        <a href="{{ url('admin/timetable/welcome') }}" class="btn btn-primary">Welcome</a>
                        <a href="{{ url('admin/timetable/create') }}" class="btn btn-primary">Create</a>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-primary card-tabs">
                                            <div class="card-header p-0 pt-1">
                                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist" style="margin-right: 10px;">

                                                    @foreach ($grades as $grade)
                                                        <li class="nav-item" style="">
                                                            <a class="nav-link text-center " style="width: 100px" id="custom-tabs-one-home-tab"
                                                            data-toggle="pill" href="#grade_{{ $grade->id }}" role="tab"
                                                            aria-controls="class_{{ $grade->id }}" aria-selected="true">{{ $grade->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                                    <div class="tab-pane fade show active" id="grade_7"
                                                         role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['7A', '7B', '7C', '7D', '7E', '7F', '7G', '7H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                            <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>

                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="class8" role="tabpanel"
                                                         aria-labelledby="custom-tabs-one-profile-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['8A', '8B', '8C', '8D', '8E', '8F', '8G', '8H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold"> {{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                             <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>
                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="class9" role="tabpanel"
                                                         aria-labelledby="custom-tabs-one-messages-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['9A', '9B', '9C', '9D', '9E', '9F', '9G', '9H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                             <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>
                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="class10" role="tabpanel"
                                                         aria-labelledby="custom-tabs-one-settings-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['10A', '10B', '10C', '10D', '10E', '10F', '10G', '10H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                             <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>
                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="class11" role="tabpanel"
                                                         aria-labelledby="custom-tabs-one-settings-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['11A', '11B', '11C', '11D', '11E', '11F', '11G', '11H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                             <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>
                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="class12" role="tabpanel"
                                                         aria-labelledby="custom-tabs-one-settings-tab">
                                                        <div class="row">
                                                            <?php
                                                            $classes = ['12A', '12B', '12C', '12D', '12E', '12F', '12G', '12H'];

                                                            foreach ($classes as $class) {
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="card">
                                                                        <div class="card-body text-center">
                                                                            <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?></h5>
                                                                        </div>
                                                                        <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                                                             <a href="{{url('admin/timetable/detail')}}" class="btn btn-primary">{{ __('View') }}</a>
                                                                            <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
