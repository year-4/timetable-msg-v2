@extends('layouts.front')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">{{ __('10 Makra 1979 High School') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">{{ __('Home') }}</a>
                    </li>
                    <!-- Dropdown for buttons 7-12 -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true">
                            {{ __('Timetable') }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#class7"
                                role="tab" aria-controls="class7" aria-selected="true">7</a>
                            <a class="dropdown-item" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#class8"
                                role="tab" aria-controls="class8" aria-selected="false">8</a>
                            <a class="dropdown-item" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#class9"
                                role="tab" aria-controls="class9" aria-selected="false">9</a>
                            <a class="dropdown-item" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#class10"
                                role="tab" aria-controls="class10" aria-selected="false">10</a>
                            <a class="dropdown-item" id="custom-tabs-one-another-tab" data-toggle="pill" href="#class11"
                                role="tab" aria-controls="class11" aria-selected="false">11</a>
                            <a class="dropdown-item" id="custom-tabs-one-more-tab" data-toggle="pill" href="#class12"
                                role="tab" aria-controls="class12" aria-selected="false">12</a>
                        </div>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="teacher-section">{{ __('Teacher') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about-section">{{ __('About') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact-section">{{ __('Contact') }}</a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- Carousel -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('/dist/img/10mk1.jpg') }}" class="d-block w-100" alt="Slide 1">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Slide 1 Heading</h3>
                    <p>Slide 1 Description</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('/dist/img/10mk2.jpg') }}" class="d-block w-100" alt="Slide 2">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Slide 2 Heading</h3>
                    <p>Slide 2 Description</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('/dist/img/10mk3.jpg') }}" class="d-block w-100" alt="Slide 3">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Slide 3 Heading</h3>
                    <p>Slide 3 Description</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('/dist/img/10mk4.jpg') }}" class="d-block w-100" alt="Slide 4">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Slide 4 Heading</h3>
                    <p>Slide 4 Description</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>




    <!-- Header -->
    <header class="text-center mt-4">
        <h1>{{ __('Welcome to 10 Makra 1979 High School Site.') }}</h1>
        <p>{{ __('Explore our wide range of courses and take your learning to the next level.') }}</p>
        <a href="#" class="btn btn-primary">{{ __('Get Started') }}</a>
    </header>

    <!-- Course Cards -->
    <section class="container mt-4" id="teacher-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-4">{{ __('Timetable') }}</h2>
            </div>
        </div>
        <div class="row">
            <!-- Hidden Card Container -->
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                    <div class="tab-pane fade show active" id="class7" role="tabpanel"
                        aria-labelledby="custom-tabs-one-home-tab">
                        <div class="row">
                            <?php
                        $classes = ['7A', '7B', '7C', '7D', '7E', '7F', '7G', '7H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <!-- Rest of the tab panes for classes 8-12 -->
                    <div class="tab-pane fade" id="class8" role="tabpanel"
                        aria-labelledby="custom-tabs-one-profile-tab">
                        <div class="row">
                            <?php
                        $classes = ['8A', '8B', '8C', '8D', '8E', '8F', '8G', '8H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="class9" role="tabpanel"
                        aria-labelledby="custom-tabs-one-messages-tab">
                        <div class="row">
                            <?php
                        $classes = ['9A', '9B', '9C', '9D', '9E', '9F', '9G', '9H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                        <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="class10" role="tabpanel"
                        aria-labelledby="custom-tabs-one-settings-tab">
                        <div class="row">
                            <?php
                        $classes = ['10A', '10B', '10C', '10D', '10E', '10F', '10G', '10H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                        <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="class11" role="tabpanel"
                        aria-labelledby="custom-tabs-one-settings-tab">
                        <div class="row">
                            <?php
                        $classes = ['11A', '11B', '11C', '11D', '11E', '11F', '11G', '11H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                        <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="class12" role="tabpanel"
                        aria-labelledby="custom-tabs-one-settings-tab">
                        <div class="row">
                            <?php
                        $classes = ['12A', '12B', '12C', '12D', '12E', '12F', '12G', '12H'];

                        foreach ($classes as $class) {
                            ?>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title font-weight-bold">{{ __('Class') }} <?php echo $class; ?>
                                        </h5>
                                    </div>
                                    <div class="card-footer btn-group d-flex justify-content-center" role="group">
                                        <a href="{{ url('admin/timetable/detail') }}"
                                            class="btn btn-primary">{{ __('View') }}</a>
                                        <a href="#" class="btn btn-danger">{{ __('Edit') }}</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <!-- Add similar tab panes for classes 9-12 -->
                    <!-- ... (Add similar tab panes for classes 9-12) ... -->
                </div>
            </div>
            <!-- ... (other course cards) ... -->
        </div>
    </section>

    <section class="container mt-4" id="teacher-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="display-5 font-weight-bold text-gray text-center mt-5">Online Course</h2>
            </div>
        </div>
        <div class="card mb-3">
            <div class="row g-0">
                <div class="col-md-4">
                    <div class="embed-responsive embed-responsive-16by9">
                        <!-- Paste the entire iframe code here as the src attribute value -->
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/g_TE7kvtATc"
                            allowFullScreen="true"></iframe>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">រូបវិទ្យា ៖ សៀគ្វីចរន្តឆ្លាស់ (តចប់) |ថ្នាក់ទី១២ លោក នុត ធីវុត</h5>
                        <p class="card-text">កម្មវិធី​បង្រៀន​ពី​ចម្ងាយ​​របស់​វិទ្យាល័យ ១០មករា ១៩៧៩ ខេត្តសៀមរាប
                            បង្រៀនដោយ ៖ លោកគ្រូ នុត ធីវុត គ្រូឯកទេស រូបវិទ្យា បង្រៀននៅ វិទ្យាល័យ ១០មករា១៩៧៩
                            មេរៀន៖ សៀគ្វីចរន្តឆ្លាស់ (តចប់)</p>
                        <a href="https://youtube.com/@user-pl5ze7hm6d" class="btn btn-sm bg-maroon">More Video</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="row g-0">
                <div class="col-md-4">
                    <div class="embed-responsive embed-responsive-16by9">
                        <!-- Paste the entire iframe code here as the src attribute value -->
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zQ1uqPfMQoA"
                            allowFullScreen="true"></iframe>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">គីមីវិទ្យា ៖ អំបិល 1|ថ្នាក់ទី៩ លោកគ្រូ អ៊ួង ភាក់</h5>
                        <p class="card-text">កម្មវិធី​បង្រៀន​ពី​ចម្ងាយ​​របស់​វិទ្យាល័យ ១០មករា ១៩៧៩ ខេត្តសៀមរាប
                            បង្រៀនដោយ ៖លោកគ្រូ អ៊ួង ភាក់ គ្រូឯកទេសគីមីវិទ្យា បង្រៀននៅ វិទ្យាល័យ ១០មករា១៩៧៩
                            មេរៀន ៖ អំបិល ភាគ១</p>
                        <a href="https://youtube.com/@user-pl5ze7hm6d" class="btn btn-sm bg-maroon">More Video</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="row g-0">
                <div class="col-md-4">
                    <div class="embed-responsive embed-responsive-16by9">
                        <!-- Paste the entire iframe code here as the src attribute value -->
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/KndoIswgnVw"
                            allowFullScreen="true"></iframe>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">គណិតវិទ្យា ថ្នាក់ទី១២ ៖ ព្រីមីទីវ​ និង អាំងតេក្រាលមិនកំណត់ ភាគ៣ |លោក ជី
                            សំណាង</h5>
                        <p class="card-text">កម្មវិធី​បង្រៀន​ពី​ចម្ងាយ​​របស់​វិទ្យាល័យ ១០មករា ១៩៧៩ ខេត្តសៀមរាប
                            បង្រៀនដោយ ៖ លោកគ្រូ ជី សំណាង សាស្ត្រាចារ្យគណិតវិទ្យា បង្រៀននៅវិទ្យាល័យ ១០មករា១៩៧៩
                            ជំពូកទី៤៖ ព្រីមីទីវ​ និង អាំងតេក្រាលមិនកំណត់ ភាគ៣
                            II - អាំងតេក្រាលប្រភាគសនិទាន</p>
                        <a href="https://youtube.com/@user-pl5ze7hm6d" class="btn btn-sm bg-maroon">More Video</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Teacher Section -->
    <section class="container mt-4" id="teacher-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="display-5 font-weight-bold text-gray text-center mt-5">Teachers</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="p-3 bg-primary/5 rounded-lg text-center">
                        <img src="{{ asset('/dist/img/user2-160x160.jpg') }}" alt="Thith THIN KOOMPI"
                            style="width: 150px; height: 255px;">
                        <h2 class="text-lg mt-3 font-bold">គីម ឡេង</h2>
                        {{-- <h3 class="opacity-80">នាយក សាលា</h3> --}}
                        <p class="mt-3">Phone: +1 (123) 456-7890</p>
                    </div>
                </div>
            </div>
            <!-- Add similar cards for other teachers with their names and phone numbers -->
            <div class="col-md-3">
                <div class="card">
                    <div class="p-3 bg-primary/5 rounded-lg text-center">
                        <img src="{{ asset('/dist/img/seanthy.jpg') }}" alt="Teacher 2"
                            style="width: 150px; height: 255px;">
                        <h2 class="text-lg mt-3 font-bold">ស៊ាន ធី</h2>
                        {{-- <h3 class="opacity-80">គ្រូវិន័យ</h3> --}}
                        <p class="mt-3">Phone: +1 (987) 654-3210</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="p-3 bg-primary/5 rounded-lg text-center">
                        <img src="{{ asset('/dist/img/eamphireeak.jpg') }}" alt="Teacher 3"
                            style="width: 150px; height: 255px;">
                        <h2 class="text-lg mt-3 font-bold">អៀម ភារៈ</h2>
                        {{-- <h3 class="opacity-80">Math Teacher</h3> --}}
                        <p class="mt-3">Phone: +1 (555) 123-4567</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="p-3 bg-primary/5 rounded-lg text-center">
                        <img src="{{ asset('/dist/img/user2-160x160.jpg') }}" alt="Thith THIN KOOMPI"
                            style="width: 150px; height: 255px;">
                        <h2 class="text-lg mt-3 font-bold">គីម ឡេង</h2>
                        {{-- <h3 class="opacity-80">នាយក សាលា</h3> --}}
                        <p class="mt-3">Phone: +1 (123) 456-7890</p>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <div class="container m-auto">
        <!-- Use Bootstrap classes to adjust the text size -->
        <h3 class="display-5 font-weight-bold text-gray text-center mt-5">Our Gallery</h3>
        <h3 class="display-6 text-black text-center text-uppercase">Activities</h3>
        <div class="row grid md:grid-cols-5 grid-cols-2 gap-2 m-8">
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u1.jpg') }}" data-toggle="lightbox"
                    data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u1.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u2.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u2.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u12.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u12.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u13.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u13.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u3.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u3.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u5.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u5.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u7.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u7.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u9.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u9.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u10.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u10.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u11.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u11.jpg') }}">
                </a>
            </div>
            <div class="col-md-3 col-6 mb-4">
                <a href="{{ asset('/dist/img/ourgallary/u6.jpg') }}" data-toggle="lightbox"
                data-title="sample 1 - white" data-gallery="gallery">
                <img class="img-fluid rounded-lg custom-img" alt=""
                    src="{{ asset('/dist/img/ourgallary/u6.jpg') }}">
                </a>
            </div>

        </div>
    </div>

    <!-- Contact Section -->
    <section class="container mt-4" id="about-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="display-5 font-weight-bold text-gray text-center mt-5">Contact Us</h2>
                <div class="row">
                    <div class="col-md-6">
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam ut nulla
                            vel dapibus. Nullam venenatis, eros sit amet varius fringilla, nisi eros mattis ipsum, eu
                            ultrices nisl est id lectus.</p>
                        <p class="lead">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                            curae; Ut ac tincidunt dolor. Donec ut tortor laoreet, luctus odio eget, euismod eros. Aenean
                            fringilla diam nec congue rutrum. Etiam convallis venenatis dolor nec pharetra. In nec eros
                            nunc.</p>
                        <p class="lead">Vivamus lobortis risus in lacinia facilisis. Sed volutpat est eu eros
                            pellentesque, vitae efficitur lectus euismod.</p>
                    </div>
                    <div class="col-md-6">
                        <form action="#" method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" id="message" name="message" rows="5" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Send Report</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="container mt-4" id="contact-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="display-5 font-weight-bold text-gray text-center mt-5">About Us</h2>
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ asset('/dist/img/user2-160x160.jpg') }}" class="img-fluid rounded" alt="About Us">
                    </div>
                    <div class="col-md-6">
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquam ut nulla
                            vel dapibus. Nullam venenatis, eros sit amet varius fringilla, nisi eros mattis ipsum, eu
                            ultrices nisl est id lectus.</p>
                        <p class="lead">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                            curae; Ut ac tincidunt dolor. Donec ut tortor laoreet, luctus odio eget, euismod eros. Aenean
                            fringilla diam nec congue rutrum. Etiam convallis venenatis dolor nec pharetra. In nec eros
                            nunc.</p>
                        <p class="lead">Vivamus lobortis risus in lacinia facilisis. Sed volutpat est eu eros
                            pellentesque, vitae efficitur lectus euismod.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    {{-- footer --}}
    {{-- footer --}}
    <div class="bg-black relative">
        <footer class="footer container mx-auto p-10 text-neutral-content">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="display-8 font-bold text-gray text-center mt-5">{{ __('10 Makra 1979 High School') }}</h3>
                    <!-- Add your social media icons or other content here -->
                    <div class="d-flex align-items-center gap-4 mb-4" style="margin-left: 15px">
                        <div class="link link-hover" style="margin-left: 15px ;"><i class="fab fa-facebook-f"></i></div>
                        <div class="link link-hover" style="margin-left: 15px"><i class="fab fa-linkedin-in"></i></div>
                        <div class="link link-hover" style="margin-left: 15px"><i class="fab fa-youtube"></i></div>
                        <div class="link link-hover" style="margin-left: 15px"><i class="fab fa-telegram"></i></div>
                        <div class="link link-hover" style="margin-left: 15px"><i class="fab fa-twitter"></i></div>
                    </div>
                </div>
                {{-- <div class="col-md-4">
        <h3 class="font-bold text-lg">Legal</h3>
        <div class="link link-hover my-2">Terms &amp; Conditions</div>
        <div class="link link-hover my-2">Privacy Policy</div>
        <div class="link link-hover my-2">Sales Policy</div>
      </div>
      <div class="col-md-4">
        <h3 class="font-bold text-lg">News and Events</h3>
        <div class="link link-hover my-2">Event Calendar</div>
        <div class="link link-hover my-2">Latest News</div>
        <div class="link link-hover my-2">Press Releases</div>
      </div> --}}

            </div>
        </footer>
    </div>




    <div class="bg-white flex gap-2 justify-center p-2">


    </div>
@endsection
<style>
    .landing-card {
        height: 300px;
        position: relative;
        overflow: hidden;
        border-radius: 10px;
    }

    .landing-card img {
        object-fit: cover;
        height: 100%;
        width: 100%;
        opacity: 0.7;
    }

    .landing-card-overlay {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(0, 0, 0, 0.7);
        padding: 15px;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
    }

    .landing-card-title {
        font-size: 24px;
        font-weight: bold;
    }

    .landing-card-text {
        font-size: 14px;
        line-height: 1.6;
    }

    .landing-card-link {
        font-size: 12px;
        text-decoration: none;
        opacity: 0.8;
        transition: opacity 0.3s ease;
    }

    .landing-card-link:hover {
        opacity: 1;
    }

    .custom-img {
        max-width: 375px;
        height: 380px;
    }
</style>
