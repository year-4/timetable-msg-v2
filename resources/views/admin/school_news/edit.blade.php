@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Edit School News') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <form method="POST" action="{{ route('admin.school_news.update', $news->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('School News Information') }}</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Title') }}<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" value="{{ $news->title }}" name="title" placeholder="{{ __('Enter Title') }}">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>{{ __('Description') }}<span style="color: red;">*</span></label>
                                            <textarea class="form-control summernote" name="description" placeholder="{{ __('Enter Description') }}">{!! $news->description !!}</textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Image') }}</label>
                                            <input type="file" name="image" class="form-control" id="exampleInputFile">
                                            @if($news->image)
                                                <p><img src="{{ asset('storage/upload/school_news/' . $news->image) }}" alt="News Image" width="50"></p>
                                            @else
                                                <p>No image</p>
                                            @endif
                                        </div>
                                        <!-- Any other fields you want to include for editing -->
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
