<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('News Detail') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <!-- Display News Details -->
            <div>
                {{-- @dd($news) --}}
                <p><strong>{{ __('Title') }}:</strong> {{ $news->title }}</p>
                <p><strong>{{ __('Description') }}:</strong> {{ $news->description }}</p>
                <p><strong>{{ __('Create By') }}:</strong> {{ $news->create_by }}</p>
                @if ($news->image)
                    <img src="{{ asset('storage/upload/school_news/' . $news->image) }}" alt="News Image" style="max-width: 100%;">
                @else
                    No image
                @endif
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
        </div>
    </div>
</div>
