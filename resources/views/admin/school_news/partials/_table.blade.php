<div class="card-body p-0 table-responsive table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Create By') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Image') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($news as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->title }}</td>
                    
                    <td>{{ $value->creator->name ?? 'Unknown User' }}</td>
                    <td>Click detail for show</td>
                    {{-- <td>
                        @if ($value->image)
                        <img src="{{asset('storage/upload/schoolnews/'. $value->image)}}" alt="Profile Pic" width="50">
                    @else
                        No image
                    @endif
                    
                    </td> --}}
                    <td>
                        <img src="{{ $value->image_url }}" alt="Profile Pic" width="50px"
                            height="50px" style="object-fit: cover; border-radius:5px;">
                    </td>
                   
                    <td style="white-space: nowrap">
                        {{-- @if (auth()->user()->can('school_news.view')) --}}
                            <a href="{{ route('front.newsdetail', $value->id) }}"
                                data-container=".modal_form" class="btn btn-info btn-xs btn-view" target="_blank">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Detail') }}
                            </a>
                        {{-- @endif --}}
                        {{-- @if (auth()->user()->can('teacher.edit')) --}}
                            <a
                                href="{{ route('admin.school_news.edit', $value->id) }}"class="btn btn-info btn-xs btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                        {{-- @endif --}}
                        {{-- @if (auth()->user()->can('teacher.delete')) --}}
                            <form action="{{ route('admin.school_news.destroy', $value->id) }}"
                                class="d-inline-block form-delete-{{ $value->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $value->id }}"
                                    data-href="{{ route('admin.school_news.destroy', $value->id) }}"
                                    class="btn btn-danger btn-xs btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>
                            
                        {{-- @endif --}}

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $news->firstItem() }} {{ __('to') }} {{ $news->lastItem() }}
                    {{ __('of') }} {{ $news->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $news->links() }}</div>
            </div>
        </div>
    </div>

    {{-- <div style="padding: 10px; float: right;">

    </div> --}}

</div>
