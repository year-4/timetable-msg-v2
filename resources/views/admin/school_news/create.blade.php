@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Add New') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <!-- Form for creating a User -->
                            <form method="POST" action="{{ route('admin.school_news.store') }}" enctype="multipart/form-data">

                                @csrf
                                <div class="card-header">
                                    <h1 class="card-title">{{ __('User Information') }}</h1>
                                    <span class="badge bg-warning total-count"></span>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        {{-- <textarea class="summernote" name="title" id="title"></textarea> --}}
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Title') }}<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" value="{{ old('title') }}"
                                                name="title" placeholder="{{ __('Enter Title') }}">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>{{ __('Description') }}<span style="color: red;">*</span></label>
                                            <textarea class="form-control summernote" name="description" placeholder="{{ __('Enter Description') }}">{{ old('description') }}</textarea>
                                        </div>
                                        {{-- <div class="form-group col-md-6">
                                            <label>{{ __('Image') }}<span style="color: red;">*</span></label>
                                            <input type="file" name="image" class="form-control" id="exampleInputFile">
                                        </div> --}}
                                        <div class="form-group col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputFile">{{__('Image')}}</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="hidden" name="image_names" class="image_names_hidden">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image" accept="image/png, image/jpeg">
                                                        <label class="custom-file-label" for="exampleInputFile">{{ __('Choose file') }}</label>
                                                    </div>
                                                </div>
                                                <div class="preview preview-multiple text-center border rounded mt-2" style="height: 150px">
                                                    <img src="{{ asset('images/default.svg') }}" alt="" height="100%">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Existing Form -->
                                        <div class="form-group col-md-6">
                                            <label>{{ __('Created By') }}<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" value="{{ auth()->user()->name }}"
                                                disabled>
                                            <input type="hidden" name="create_by" value="{{ auth()->user()->id }}">
                                        </div>
                                        

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                                </div>
                            </form>
                            <!-- End of User Creation Form -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('script')
    <script>
        const compressor = new window.Compress();
        $('.custom-file-input').change(function (e) {
            compressor.compress([...e.target.files], {
                size: 4,
                quality: 0.75,
            }).then((output) => {
                var files = Compress.convertBase64ToFile(output[0].data, output[0].ext);
                var formData = new FormData();

                var image_names_hidden = $(this).closest('.custom-file').find('input[type=hidden]');
                var container = $(this).closest('.form-group').find('.preview');
                if (container.find('img').attr('src') === `{{ asset('images/default.svg') }}`) {
                    container.empty();
                }
                var _token = `{{ csrf_token() }}`;
                formData.append('image', files);
                formData.append('_token', _token);

                $.ajax({
                    url: "{{ route('save_temp_file') }}",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        if (response.status == 0) {
                            toastr.error(response.msg);
                        }
                        if (response.status == 1) {
                            container.empty();
                            var temp_file = response.temp_files;
                            var img_container = $('<div></div>').addClass('img_container');
                            var img = $('<img>').attr('src', "{{ asset('upload/temp') }}" +'/'+ temp_file);
                            img_container.append(img);
                            container.append(img_container);

                            var new_file_name = temp_file;
                            console.log(new_file_name);

                            image_names_hidden.val(new_file_name);
                        }
                    }
                });
            });
        });
    </script>
@endpush