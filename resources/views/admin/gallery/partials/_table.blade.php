<div class="card-body p-0 table-responsive table-wrapper">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Type') }}</th>
                <th>{{ __('Image') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($galleries as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->type }}</td>

                    <td>
                        @php
                            if (!empty($image_names) && file_exists(public_path('upload/gallery/' . $image_names))) {
                                $image_url = asset('/upload/gallery/' . rawurlencode($image_names));
                            } else {
                                $image_url = asset('images/default.svg');
                            }
                        @endphp
                        <img src="{{ $value->image_url }}" alt="Profile Pic" width="50px" height="50px"
                            style="object-fit: cover; border-radius:5px;">
                    </td>

                    <td style="white-space: nowrap">

                        <a href="#" data-href="{{ route('admin.gallery.show', $value->id) }}"
                            data-container=".modal_form" class="btn btn-info btn-xs btn-view">
                            <i class="fas fa-pencil-alt"></i>
                            {{ __('Detail') }}
                            <a href="{{ route('admin.gallery.edit', $value->id) }}"class="btn btn-info btn-xs btn-edit">
                                <i class="fas fa-pencil-alt"></i>
                                {{ __('Edit') }}
                            </a>
                            <form action="{{ route('admin.gallery.destroy', $value->id) }}"
                                class="d-inline-block form-delete-{{ $value->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" data-id="{{ $value->id }}"
                                    data-href="{{ route('admin.gallery.destroy', $value->id) }}"
                                    class="btn btn-danger btn-xs btn-delete">
                                    <i class="fas fa-trash"></i>
                                    {{ __('Delete') }}
                                </button>
                            </form>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 d-flex flex-row flex-wrap">
            {{-- <div class="row" style="width: -webkit-fill-available;">
                <div class="col-12 col-sm-6 text-center text-sm-left pl-3" style="margin-block: 20px">
                    {{ __('Showing') }} {{ $news->firstItem() }} {{ __('to') }} {{ $news->lastItem() }}
                    {{ __('of') }} {{ $news->total() }} {{ __('entries') }}
                </div>
                <div class="col-12 col-sm-6 pagination-nav pr-3"> {{ $news->links() }}</div>
            </div> --}}
        </div>
    </div>

    {{-- <div style="padding: 10px; float: right;">

    </div> --}}

</div>
