<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">{{ __('Gallery Detail') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <!-- Display News Details -->
            <div>
                <p><strong>{{ __('Type') }}:</strong> {{ $gallery->type }}</p>
                
                <img src="{{ $gallery->image_url }}" alt="Profile Pic" width="100%"  style="object-fit: cover; border-radius:5px;" >
               
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
        </div>
    </div>
</div>
