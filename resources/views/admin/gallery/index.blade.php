@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>{{ __('Gallery') }}</h3>
                </div>
                <div class="col-sm-6" style="text-align: right">
                    {{-- @if (auth()->user()->can('user.create')) --}}
                        <a href="{{ route('admin.gallery.create') }}" class="btn btn-primary">{{ __('Upload Gallery') }}</a>
                    {{-- @endif --}}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('Gallery List')}}</h3>
                            {{-- <span class="badge bg-warning total-count">{{ $news->total() }}</span> --}}
                        </div>
                        <!-- /.card-header -->
                        @include('admin.gallery.partials._table')
                        <!-- /.card-body -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <div class="modal fade modal_form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"></div>

    </div>
@endsection

@push('script')
    <script>
        $(document).on('click', '.btn-view', function(){
            $("div.modal_form").load($(this).data('href'), function(){

                $(this).modal('show');

            });
        });
        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();

            Confirmation.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    console.log(`.form-delete-${$(this).data('id')}`);
                    var data = $(`.form-delete-${$(this).data('id')}`).serialize();
                    console.log(data);
                    $.ajax({
                        type: "post",
                        url: $(this).data('href'),
                        data: data,
                        // dataType: "json",
                        success: function (response) {
                            console.log(response);
                            // $('div.modal_form').modal('hide');
                            $('.table-wrapper').replaceWith(response.view);
                            $('.total-count').html(response.total);

                            Toast.fire({
                                icon: 'success',
                                title: `{{ __('Deleted Successfully') }}`
                            });
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                    )
                }
            });
        });
    </script>
@endpush
