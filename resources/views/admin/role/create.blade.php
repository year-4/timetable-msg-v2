@extends('layouts.app')
@push('style')
    <style>
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 22px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 18px;
            width: 18px;
            left: 2px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #4b54bc;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #4b54bc;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(18px);
            -ms-transform: translateX(18px);
            transform: translateX(18px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 22px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .checkIP {
            width: 20px;
        }
    </style>
@endpush
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ __('Create Role') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-material form-horizontal" action="{{ route('admin.roles.store') }}"
                                    method="POST">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label for="name">@lang('Name Position')</label>
                                                <div class="input-group mb-3">
                                                    <input type="text" id="name" name="name"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        placeholder="@lang('Type name permission')">
                                                    @error('name')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <label style="font-size: 16px;" for="">{{ __('Select Permission') }}</label>
                                    @error('permissions')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                    <hr class="mt-0">
                                    <br>

                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Grade') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_grade"
                                                                    name="permissions[]" value="grade.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_grade">{{ __('View grade') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="grade_create"
                                                                    name="permissions[]" value="grade.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="grade_create">{{ __('Create grade') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="grade_edit"
                                                                    name="permissions[]" value="grade.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="grade_edit">{{ __('Edit grade') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="grade_delete"
                                                                    name="permissions[]" value="grade.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="grade_delete">{{ __('Delete grade') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('User') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_user"
                                                                    name="permissions[]" value="user.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_user">{{ __('View user') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="user_create"
                                                                    name="permissions[]" value="user.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="user_create">{{ __('Create user') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="user_edit"
                                                                    name="permissions[]" value="user.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="user_edit">{{ __('Edit user') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="user_delete"
                                                                    name="permissions[]" value="user.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="user_delete">{{ __('Delete user') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Subject Type') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_subject_type"
                                                                    name="permissions[]" value="subject_type.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_subject_type">{{ __('View subject_type') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_type_create"
                                                                    name="permissions[]" value="subject_type.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_type_create">{{ __('Create subject_type') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_type_edit"
                                                                    name="permissions[]" value="subject_type.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_type_edit">{{ __('Edit subject_type') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_type_delete"
                                                                    name="permissions[]" value="subject_type.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_type_delete">{{ __('Delete subject_type') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Class Room') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_class_room"
                                                                    name="permissions[]" value="class_room.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_class_room">{{ __('View class_room') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="class_room_create"
                                                                    name="permissions[]" value="class_room.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="class_room_create">{{ __('Create class_room') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="class_room_edit"
                                                                    name="permissions[]" value="class_room.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="class_room_edit">{{ __('Edit class_room') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="class_room_delete"
                                                                    name="permissions[]" value="class_room.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="class_room_delete">{{ __('Delete class_room') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Subject') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_subject"
                                                                    name="permissions[]" value="subject.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_subject">{{ __('View subject') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_create"
                                                                    name="permissions[]" value="subject.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_create">{{ __('Create subject') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_edit"
                                                                    name="permissions[]" value="subject.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_edit">{{ __('Edit subject') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="subject_delete"
                                                                    name="permissions[]" value="subject.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="subject_delete">{{ __('Delete subject') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Teacher') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_teacher"
                                                                    name="permissions[]" value="teacher.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_teacher">{{ __('View teacher') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="teacher_create"
                                                                    name="permissions[]" value="teacher.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="teacher_create">{{ __('Create teacher') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="teacher_edit"
                                                                    name="permissions[]" value="teacher.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="teacher_edit">{{ __('Edit teacher') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="teacher_delete"
                                                                    name="permissions[]" value="teacher.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="teacher_delete">{{ __('Delete teacher') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Academic Year') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_academic_year"
                                                                    name="permissions[]" value="academic_year.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_academic_year">{{ __('View academic_year') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="academic_year_create"
                                                                    name="permissions[]" value="academic_year.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="academic_year_create">{{ __('Create academic_year') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="academic_year_edit"
                                                                    name="permissions[]" value="academic_year.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="academic_year_edit">{{ __('Edit academic_year') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="academic_year_delete"
                                                                    name="permissions[]" value="academic_year.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="academic_year_delete">{{ __('Delete academic_year') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Time Slot') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_time_slot"
                                                                    name="permissions[]" value="time_slot.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_time_slot">{{ __('View time_slot') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="time_slot_create"
                                                                    name="permissions[]" value="time_slot.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="time_slot_create">{{ __('Create time_slot') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="time_slot_edit"
                                                                    name="permissions[]" value="time_slot.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="time_slot_edit">{{ __('Edit time_slot') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="time_slot_delete"
                                                                    name="permissions[]" value="time_slot.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="time_slot_delete">{{ __('Delete time_slot') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div>
                                        <div class="d-flex">
                                            <label for="" class="mr-2 mb-3">{{ __('Timetable') }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="view_timetable"
                                                                    name="permissions[]" value="timetable.view">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="view_timetable">{{ __('View timetable') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="timetable_create"
                                                                    name="permissions[]" value="timetable.create">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="timetable_create">{{ __('Create timetable') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <!-- Rounded switch -->

                                                            <label class="switch">
                                                                <input type="checkbox" id="timetable_edit"
                                                                    name="permissions[]" value="timetable.edit">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="timetable_edit">{{ __('Edit timetable') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="d-flex align-items-center">
                                                            <label class="switch">
                                                                <input type="checkbox" id="timetable_delete"
                                                                    name="permissions[]" value="timetable.delete">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <label class="ml-2"
                                                                for="timetable_delete">{{ __('Delete timetable') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    {{-- <div>
                                <div class="d-flex">
                                    <label for="" class="mr-2 mb-3">{{ __('Setting') }}</label>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="d-flex align-items-center">
                                                <label class="switch">
                                                    <input type="checkbox" id="view_menu" name="permissions[]" value="menu.view">
                                                    <span class="slider round"></span>
                                                </label>
                                                <label class="ml-2" for="view_menu">{{ __('View menu') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="d-flex align-items-center">
                                                    <label class="switch">
                                                    <input type="checkbox" id="menu_create" name="permissions[]" value="menu.create">
                                                    <span class="slider round"></span>
                                                    </label>
                                                    <label class="ml-2" for="menu_create">{{ __('Create menu') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="d-flex align-items-center">
                                                    <!-- Rounded switch -->

                                                    <label class="switch">
                                                    <input type="checkbox" id="menu_edit" name="permissions[]"  value="menu.edit">
                                                    <span class="slider round"></span>
                                                    </label>
                                                    <label class="ml-2" for="menu_edit">{{ __('Edit menu') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="d-flex align-items-center">
                                                <label class="switch">
                                                    <input type="checkbox" id="menu_delete" name="permissions[]" value="menu.delete">
                                                    <span class="slider round"></span>
                                                </label>
                                                <label class="ml-2" for="menu_delete">{{ __('Delete menu') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div> --}}


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-3 col-form-label"></label>
                                                <div class="col-md-8">
                                                    <input type="submit" value="{{ __('Submit') }}"
                                                        class="btn btn-outline btn-primary btn-lg" />
                                                    {{-- <a href="{{ route('position-of-staff.index') }}" class="btn btn-outline btn-danger btn-lg">{{ __('Cancel') }}</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
