<div class="card-body p-0 table-wrapper">
    <table class="table dataTable">
        <thead>
            <tr>
                <th>{{ __('Name') }} </th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @if($roles)
                @foreach ($roles as $row)
                    <tr>
                        <td class="text-capitalize">{{$row->name}}</td>
                        <td>
                            @if ($row->name != 'admin')
                                @if (auth()->user()->can('role.edit'))
                                    <a href="{{route('admin.roles.edit',$row->id)}}" class="btn btn-sm btn-info"><i class="fas fa-pencil-alt"></i> {{ __('Edit') }}</a>
                                @endif

                                @if (auth()->user()->can('role.delete') && !in_array($row->name, ['admin', 'teacher']))
                                    <form action="{{ route('admin.roles.destroy', $row->id) }}" class="d-inline-block form-delete-{{ $row->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" data-id="{{ $row->id }}" data-href="{{ route('admin.roles.destroy', $row->id) }}" class="btn btn-danger btn-sm btn-delete">
                                            <i class="fa fa-trash-alt"></i>
                                            {{ __('Delete') }}
                                        </button>
                                    </form>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach

            @endif
        </tbody>
    </table>
    <div class="col-xs-12 text-right">
    {!! $roles->links() !!}
    </div>
</div>
