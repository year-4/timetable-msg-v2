<nav class="main-header navbar navbar-expand {{ config('app.dark_mode') == 1 ? 'navbar-dark' : 'navbar-light' }}" style="height: 50px">
    <div class="container-fluid">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <li class="nav-item d-flex align-items-center" style="">
                <a href="#" class="theme-toggle nav-link" id="theme-toggle" title="Toggles light & dark"
                    aria-label="auto" aria-live="polite">
                    @include('icons.sun-moon')
                </a>
            </li>
            <!-- Language Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="flag-icon flag-icon-{{ $current_locale == 'en' ? 'gb' : $current_locale }}"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right p-0">
                    @foreach ($available_locales as $locale_name => $available_locale)
                        @if ($available_locale === $current_locale)
                            <a href="{{ route('change_language', $available_locale) }}"
                                class="dropdown-item text-capitalize active">
                                <i
                                    class="flag-icon flag-icon-{{ $available_locale == 'en' ? 'gb' : $available_locale }} mr-2"></i>
                                {{ $locale_name }}
                            </a>
                        @else
                            <a href="{{ route('change_language', $available_locale) }}"
                                class="dropdown-item text-capitalize">
                                <i
                                    class="flag-icon flag-icon-{{ $available_locale == 'en' ? 'gb' : $available_locale }} mr-2"></i>
                                {{ $locale_name }}
                            </a>
                        @endif
                    @endforeach
                    {{-- <a href="#" class="dropdown-item">
            <i class="flag-icon flag-icon-kh mr-2"></i> Khmer
          </a> --}}
                    {{-- <a href="#" class="dropdown-item">
            <i class="flag-icon flag-icon-fr mr-2"></i> French
          </a>
          <a href="#" class="dropdown-item">
            <i class="flag-icon flag-icon-es mr-2"></i> Spanish
          </a> --}}
                </div>
            </li>
            {{-- <li class="nav-item d-flex align-items-center" style="">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <span class="user-img">
                    @if (Auth::user()->image)
                        <img src="{{ asset('upload/users/' . Auth::user()->image) }}" class="rounded-circle"
                            width="30" height="30" alt="User Image">
                    @else
                        <img src="{{ asset('images/default.svg') }}" class="rounded-circle" width="30"
                            alt="User Image">
                    @endif
                    
                </span>
                <span><a href="#"data-toggle="dropdown" class="d-block">{{ Auth::user()->name }}</a></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right p-0">
                <a class="dropdown-item" href="{{ route('admin.user.profile') }}"
                class="nav-link @if (request()->routeIs('admin.profile*')) active @endif">My Profile</a>
                <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
                <a class="dropdown-item" href="settings.html">Settings</a>
                <a class="dropdown-item" href="login.html">Logout</a>

            </div>

            
        </li> --}}
            <li class="nav-item dropdown">
                <a href="#" class="nav-link" data-toggle="dropdown" style="width: 55px">
                    @if (Auth::user()->image)
                        <img src="{{ asset('upload/users/' . Auth::user()->image) }}" class="rounded-circle"
                            width="100%" alt="User Image">
                    @else
                        <img src="{{ asset('images/default.svg') }}" class="rounded-circle" width="100%" 
                            alt="User Image">
                    @endif
                    {{-- <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> --}}
                </a>
                <div class="dropdown-menu dropdown-menu-right p-0">
                    <a class="dropdown-item" href="{{ route('admin.user.profile') }}"
                    class="nav-link @if (request()->routeIs('admin.profile*')) active @endif">{{__("My Profile")}}</a>
                    
                    <a class="dropdown-item" href="{{ url('admin/change_password') }}"
                    class="nav-link @if (Request::segment(2) == 'change_password') active @endif">{{__("Change Password")}}</a>
                    <a class="dropdown-item"href="{{ url('logout') }}" class="nav-link">{{__('Logout')}}</a>
    
                </div>
                
            </li>
        </ul>
    </div>
</nav>
<!-- Navbar -->
