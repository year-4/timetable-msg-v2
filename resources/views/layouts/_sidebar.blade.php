<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-{{ config('app.dark_mode') == 1 ? 'dark' : 'light' }}-primary elevation-4">
    <!-- Brand Logo -->
    {{-- <a href="index3.html" class="brand-link text-center">
        <span class="brand-text font-weight-light">{{ __('10 Makra 1979') }}</span>
    </a> --}}

        <a href="{{ route('front.index') }}" class="brand-link text-center">
            <img src="{{ asset('front/images/logo.png') }}" alt="MyApp Logo" style="width: 50px;"><br>
            <span class="brand-text font-weight-light"  style="font-size: 15px">{{session()->get('app_name')}}</span>
        </a>


    <!-- Sidebar -->
    <div class="sidebar" style="margin-top:100px ">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @if (Auth::user()->image)
                    <img  src="{{ asset('upload/users/' . Auth::user()->image) }}"
                        style="object-fit:cover; border-radius: 50%; width: 40px; height: 40px;" alt="User Image">
                @else
                    <img  src="{{ asset('images/default.svg') }}" class="img-circle elevation-2" width="50"
                        alt="User Image">
                @endif
            </div>

            <div class="info">
                <a href="{{ route('admin.user.profile') }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                {{-- @if (Auth::user()->user_type == 1) --}}
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}"
                        class="nav-link @if (request()->routeIs('admin.dashboard')) active @endif">

                        <i class=" nav-icon fas fa-chart-line"></i>
                        <p>
                            {{ __('Dashboard') }}

                        </p>
                    </a>
                </li>
                @if (auth()->user()->can('roles.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.roles.index') }}"
                            class="nav-link @if (request()->routeIs('admin.roles*')) active @endif">

                            <i class="nav-icon fas fa-shield-alt"></i>
                            <p>
                                {{ __('Role') }}
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->can('teacher.view'))
                <li class="nav-item">
                    <a href="{{ route('admin.teacher.index') }}"
                        class="nav-link @if (request()->routeIs('admin.teacher*')) active @endif">

                        <i class="nav-icon fas fa-chalkboard-teacher"></i>
                        <p>
                            {{ __('Teacher') }}
                        </p>
                    </a>
                </li>
                @endif
                {{-- <li class="nav-item">
                    <a href="{{ url('admin/admin/list') }}"
                        class="nav-link @if (Request::segment(2) == 'admin') active @endif">

                          <i class="nav-icon fas fa-user-shield"></i>
                          <p>
                              Admin
                          </p>
                      </a>
                  </li> --}}
                @if (auth()->user()->can('user.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.user.index') }}"
                            class="nav-link @if (request()->routeIs('admin.user*')) active @endif">

                            <i class="nav-icon fas fa-user-edit"></i>
                            <p>
                                {{__('User')}}
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->can('grade.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.grade.index') }}"
                            class="nav-link @if (request()->routeIs('admin.grade*')) active @endif">

                            <i class="nav-icon fas fa-user-graduate"></i>
                            <p>
                                {{ __('Grade') }}
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->can('subject.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.subject.index') }}"
                            class="nav-link @if (request()->routeIs('admin.subject.*')) active @endif">
                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                {{ __('Subject') }}
                            </p>
                        </a>
                    </li>
                @endif
                {{-- @if (auth()->user()->can('subject_type.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.subject_type.index') }}"
                            class="nav-link @if (request()->routeIs('admin.subject_type*')) active @endif">

                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                {{ __('Subject Types') }}
                            </p>
                        </a>
                    </li>
                @endif --}}

                @if (auth()->user()->can('class_room.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.class_room.index') }}"
                            class="nav-link @if (request()->routeIs('admin.class_room*')) active @endif">
                            <i class="nav-icon fab fa-buromobelexperte"></i>

                            <p>
                                {{ __('Class Room') }}
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->can('time_slot.view'))
                <li class="nav-item">
                    <a href="{{ route('admin.time_slot.index') }}"
                        class="nav-link @if (request()->routeIs('admin.time_slot.index*')) active @endif">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            {{ __('Time Slot') }}
                        </p>
                    </a>
                </li>
                @endif
                @if (auth()->user()->can('academic_year.view'))
                    <li class="nav-item">
                        <a href="{{ route('admin.academic_year.index') }}"
                            class="nav-link @if (request()->routeIs('admin.academic_year.index*')) active @endif">
                            <i class="nav-icon far fa-calendar-alt"></i>
                            <p>
                                {{ __('Academic Year') }}
                            </p>
                        </a>
                    </li>
                @endif

                {{-- @dd(auth()->user()) --}}
                @if (auth()->user()->can('timetable.view') || auth()->user()->hasRole('teacher'))
                    <li class="nav-item">
                        <a href="{{ route('admin.timetable.index') }}"
                            class="nav-link @if (request()->routeIs('admin.timetable.*')) active @endif">
                            <i class="nav-icon far fa-calendar-alt"></i>
                            <p>
                                {{ __('Timetable') }}
                            </p>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->hasRole('admin'))
                    <li class="nav-item">
                        <a href="{{ route('admin.gallery.index') }}"
                        class="nav-link @if (request()->routeIs('admin.gallery.*')) active @endif">
                        <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                {{ __('Gallery') }}
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.school_news.index') }}"
                        class="nav-link @if (request()->routeIs('admin.school_news.*')) active @endif">
                        <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                {{ __('School News') }}
                            </p>
                        </a>
                    </li>
                @endif
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Front
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="{{ route('admin.school_news.index') }}"
                        class="nav-link @if (request()->routeIs('admin.school_news.*')) active @endif">School News</a>
                        <a class="dropdown-item"  href="{{ route('admin.gallery.index') }}"
                        class="nav-link @if (request()->routeIs('admin.gallery.*')) active @endif">Gallery</a>
                        <a class="dropdown-item" href="#">About</a>
                        <a class="dropdown-item" href="#">Video</a>
                    </div>
                </li> --}}
                @if (auth()->user()->hasRole('admin'))
                    <li class="nav-item">
                        <a href="{{ route('admin.setting.index') }}"
                            class="nav-link @if (request()->routeIs('admin.setting.*')) active @endif">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                {{ __('Setting') }}
                            </p>
                        </a>
                    </li>
                @endif

                <li class="nav-item @if(request()->routeIs('admin.report*')) menu-open @endif">
                    <a href="#" class="nav-link @if(request()->routeIs('admin.report*')) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ __('Report') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @if (auth()->user()->hasRole('admin'))
                            <li class="nav-item">
                                <a href="{{ route('admin.report.teacher') }}" class="nav-link @if(request()->routeIs('admin.report.teacher*')) active @endif">
                                    <i class="@if(request()->routeIs('admin.report.teacher*')) fas @else far @endif fa-circle nav-icon"></i>
                                    <p>{{ __('Teacher Report') }}</p>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a href="{{ route('admin.report.class_teacher') }}" class="nav-link @if(request()->routeIs('admin.report.class_teacher*')) active @endif">
                                <i class="@if(request()->routeIs('admin.report.class_teacher*')) fas @else far @endif fa-circle nav-icon"></i>
                                <p>{{ __('Class Teacher') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>



                {{-- <li class="nav-item">
                    <a href="{{ url('admin/timetable/welcome') }}"
                        class="nav-link @if (Request::segment(2) == 'admin.timetable.welcome*') active @endif">
                        <i class="nav-icon far fa-user"></i>
                        <p>
                            {{ __('Welcome') }}
                        </p>
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a href="{{ url('admin/change_password') }}"
                        class="nav-link @if (Request::segment(2) == 'change_password') active @endif">

                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            {{__('Change Password')}}
                        </p>
                    </a>
                </li>


                {{-- @elseif(Auth::user()->user_type == 2) --}}
                @if (Auth::user()->user_type == 2)
                    <li class="nav-item">
                        <a href="{{ url('teacher/dashboard') }}"
                            class="nav-link @if (Request::segment(2) == 'dashboard') active @endif">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard

                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('teacher/change_password') }}"
                            class="nav-link @if (Request::segment(2) == 'change_password') active @endif">
                            <i class="nav-icon far fa-user"></i>
                            <p>
                                Change Password
                            </p>
                        </a>
                    </li>
                @elseif(Auth::user()->user_type == 3)
                    <li class="nav-item">
                        <a href="{{ url('student/dashboard') }}"
                            class="nav-link @if (Request::segment(2) == 'dashboard') active @endif">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard

                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('student/change_password') }}"
                            class="nav-link @if (Request::segment(2) == 'change_password') active @endif">
                            <i class="nav-icon far fa-user"></i>
                            <p>
                                Change Password
                            </p>
                        </a>
                    </li>
                @elseif(Auth::user()->user_type == 4)
                    <li class="nav-item">
                        <a href="{{ url('parent/dashboard') }}"
                            class="nav-link @if (Request::segment(2) == 'dashboard') active @endif">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Dashboard

                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('parent/change_password') }}"
                            class="nav-link @if (Request::segment(2) == 'change_password') active @endif">
                            <i class="nav-icon far fa-user"></i>
                            <p>
                                Change Password
                            </p>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>
                </li>


                <!-- /.sidebar-menu
        <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon far fa-envelope"></i>
            <p>
            Mailbox
            <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="pages/mailbox/mailbox.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Inbox</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/mailbox/compose.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Compose</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="pages/mailbox/read-mail.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Read</p>
            </a>
            </li>
        </ul>
        </li>
-->
            </ul>
        </nav>

    </div>
    <!-- /.sidebar -->
</aside>
