@extends('front.layouts.app')

@section('content')
    @push('style')
        <style>
            .untree_co-sections {
                height: 10vh;
                min-height:140px;
            }

            @media (max-width: 779px) {
                .untree_co-sectiona,
                .untree_co-hero>.container>.row,
                .bg-img,
                .bg-img>.container>.row {
                    min-height: 140px;
                }
            }
        </style>
    @endpush
    @php
        if (!empty($banner_image_names) && file_exists(public_path('upload/settings/' . $banner_image_names))) {
            $image_url = asset('/upload/settings/' . rawurlencode($banner_image_names));
        } else {
            $image_url = asset('images/default.svg');
        }
    @endphp
  <div class="untree_co-sectiona inner-page overlay" style="background-color: #009D8A">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-12">
          <div class="row justify-content-center ">
            <div class="col-lg-6 text-center ">
              {{-- <h1 class="mb-4 heading text-white" data-aos="fade-up" data-aos-delay="100">Login</h1> --}}

            </div>
          </div>
        </div>
      </div> <!-- /.row -->
    </div> <!-- /.container -->

  </div> <!-- /.untree_co-hero -->




  <div class="untree_co-sectiona" style="margin-top: 300px;" >
    <div class="container">
      <div class="row mb-5 justify-content-center">
        @include('_message')
        <div class="col-lg-5 mx-auto order-1" data-aos="fade-up" data-aos-delay="200">
          
          <form action="{{url('register')}}" method="post" class="form-box" >
            @csrf
            <div class="row">
              <div class="col-12 mb-3">
                <h1 h1 class="mb-4 heading text-center">
                  <span class="caption">Register</span>
                  
                </h1>
              </div>
              <div class="col-12 mb-3">
                <input type="name" class="form-control" required name="name" placeholder="Username">
               
              </div>
              <div class="col-12 mb-3">
                <input type="email" class="form-control" required name="email" placeholder="Email">
               
              </div>
              <div class="col-12 mb-3">
                <input type="password" class="form-control" required name="password"placeholder="Password">
                
              </div>

              <!-- <div class="col-12 mb-3">
                <label class="control control--checkbox">
                  <span class="caption">Remember me</span>
                  <input type="checkbox"  />
                  <div class="control__indicator" style="background-color: #009D8A"></div>
                </label>
              </div> -->

              <div class="col-12 mb-3">
                  <p class="mb-1">
                    <a href="{{ route('login') }}" style="color: gray">Already have an account? Login</a>
                  </p>

              </div>

              <div class="col-12">
                <input type="submit" value="Register" class="btn btn-secondary">
              </div>
               
            </div>
          </form>

        </div>
      </div>

      
    </div>
  </div> <!-- /.untree_co-section -->

  @endsection