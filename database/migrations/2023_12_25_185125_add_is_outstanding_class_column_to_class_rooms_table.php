<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('class_rooms', function (Blueprint $table) {
            $table->integer('is_outstanding_class')->default(0);
            $table->integer('extra_hours')->nullable();
            $table->unsignedBigInteger('outstanding_subject')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('class_rooms', function (Blueprint $table) {
            $table->dropColumn('is_outstanding_class');
            $table->dropColumn('extra_hours');
            $table->dropColumn('outstanding_subject');
        });
    }
};
