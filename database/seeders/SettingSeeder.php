<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'type'   => 'language',
                'value' => '[{"id":"1","name":"english","direction":"ltr","code":"en","status":1,"default":true},{"id":"2","name":"ខ្មែរ","direction":"ltr","code":"kh","status":1,"default":false}]',
            ],
            [
                'type'   => 'pnc_language',
                'value' => '["en"]',
            ],
        ];
        Setting::INSERT($data);
    }
}
