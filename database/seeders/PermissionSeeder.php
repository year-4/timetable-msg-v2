<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'id'         => 1,
                'name'       => 'grade.view',
                'guard_name' => 'web',
            ],
            [
                'id'     => 2,
                'name'   => 'grade.create',
                'guard_name' => 'web',
            ],
            [
                'id'     => 3,
                'name'   => 'grade.edit',
                'guard_name' => 'web',
            ],
            [
                'id'     => 4,
                'name'   => 'grade.delete',
                'guard_name' => 'web',
            ]
        ];
        Permission::INSERT($data);
    }
}
