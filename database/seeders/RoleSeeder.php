<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'id'         => 1,
                'name'       => 'admin',
                'guard_name' => 'web',
            ],
            [
                'id'         => 2,
                'name'       => 'teacher',
                'guard_name' => 'web',
            ],
        ];
        Role::INSERT($data);
        $role = Role::where('name', 'admin')->first();
        // $role->giveAllPermissions();
        $role->givePermissionTo(Permission::all());
    }
}
