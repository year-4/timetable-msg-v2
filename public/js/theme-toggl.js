const storageKey = 'theme-preference'

const onClick = () => {
  // flip current value
  theme.value = theme.value === 'light'
    ? 'dark'
    : 'light'

  setPreference()
}

const getColorPreference = () => {
  if (localStorage.getItem(storageKey))
    return localStorage.getItem(storageKey)
  else
    return window.matchMedia('(prefers-color-scheme: dark)').matches
      ? 'dark'
      : 'light'
}

const setPreference = () => {
  localStorage.setItem(storageKey, theme.value)
  reflectPreference()
}

const reflectPreference = () => {
  document.firstElementChild
    .setAttribute('data-theme', theme.value);

    if (theme.value == 'dark') {
        $.cookie("dark_mode" , 1 , { expires: new Date().getTime() + (10 * 365 * 24 * 60 * 60 * 1000) }, '/');
        document.body.classList.add('dark-mode');
        $('.main-sidebar').removeClass('sidebar-light-primary');
        $('.main-sidebar').addClass('sidebar-dark-primary');
        $('.main-header').removeClass('navbar-light');
        $('.main-header').addClass('navbar-dark');
    } else if (document.querySelector('body').classList.contains('dark-mode')) {
        $.cookie("dark_mode" , 0 , { expires: new Date().getTime() + (10 * 365 * 24 * 60 * 60 * 1000) }, '/');
        document.body.classList.remove('dark-mode');
        $('.main-sidebar').addClass('sidebar-light-primary');
        $('.main-sidebar').removeClass('sidebar-dark-primary');
        $('.main-header').addClass('navbar-light');
        $('.main-header').removeClass('navbar-dark');
    }

    if (document.querySelector('body').classList.contains('dark-mode')) {


    } else {

    }

  document
    .querySelector('#theme-toggle')
    ?.setAttribute('aria-label', theme.value)
}

const theme = {
  value: getColorPreference(),
}

// set early so no page flashes / CSS is made aware
reflectPreference()

window.onload = () => {
  // set on load so screen readers can see latest value on the button
  reflectPreference()

  // now this script can find and listen for clicks on the control
  document
    .querySelector('#theme-toggle')
    .addEventListener('click', onClick)
}

// sync with system changes
window
  .matchMedia('(prefers-color-scheme: dark)')
  .addEventListener('change', ({matches:isDark}) => {
    theme.value = isDark ? 'dark' : 'light';
    setPreference()
})
