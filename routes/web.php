<?php

use App\Models\Grade;
use App\Models\Setting;
use App\Models\Teacher;
use Barryvdh\DomPDF\PDF;
use App\Models\ClassRoom;
use App\Models\TimeTable;
use App\Models\AcademicYear;
use App\Models\TimetableSlot;
use App\Models\ClassSubjectModel;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
// use App\Models\Timetable;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\SlotTimeController;
use App\Http\Controllers\ClassRoomController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TimeTableController;
use App\Http\Controllers\FileManagerController;
use App\Http\Controllers\SubjectTypeController;
use App\Http\Controllers\AcademicYearController;
use App\Http\Controllers\ClassSubjectController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TimetableSlotController;

// Route::get('/', function () {
//     return view('welcome');
// });

// change language
Route::get('language/{locale}', function ($locale) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    $language = \App\Models\Setting::where('type', 'language')->first();
    session()->put('language_settings', $language);
    return redirect()->back();
})->name('change_language');

Route::post('save_temp_file', [FileManagerController::class, 'saveTempFile'])->name('save_temp_file');
Route::get('timetable/timetablepdf/{id}', [FrontController::class, 'timetablepdf'])->name('timetable.timetablepdf');

// Route::get('/', function() {
//     return view('front.index');
// });

Route::group(['middleware' => ['SetFrontSessionData']], function () {
    Route::get('/', [FrontController::class, 'index'])->name('front.index');

    Route::group(['prefix' => 'front', 'as' => 'front.'], function () {
        Route::get('news', [FrontController::class, 'news'])->name('news');
        Route::get('gallery', [FrontController::class, 'gallery'])->name('gallery');
        Route::get('newsdetail/{id}', [FrontController::class, 'newsdetail'])->name('newsdetail');
        Route::get('about', [FrontController::class, 'about'])->name('about');
        Route::get('contact', [FrontController::class, 'contact'])->name('contact');

        Route::get('timetable', [FrontController::class, 'timetable'])->name('timetable');
        Route::get('timetable/{id}/show', [FrontController::class, 'timetableDetail'])->name('timetable.detail');




    });

    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('login', [AuthController::class, 'AuthLogin']);
    Route::get('register', [AuthController::class, 'register'])->name('register');
    Route::post('register', [AuthController::class, 'store']);
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('forgot-password', [AuthController::class, 'forgotpassword']);
    Route::post('forgot-password', [AuthController::class, 'PostForgotPassword']);
    Route::get('reset/{token}', [AuthController::class, 'reset']);
    Route::post('reset/{token}', [AuthController::class, 'PostReset']);
});



// Route::get('admin/dashboard', function () {
//     return view('admin.dashboard');
// })->name('admin.dashboard');



Route::group(['middleware' => ['admin', 'auth', 'SetSessionData']], function () {

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/dashboard' , [DashboardController::class,'dashboard'])->name('dashboard');
        Route::get('/admin/dashboard', [DashboardController::class, 'dashboard'])->name('admin.dashboard');
        Route::get('/user/profile', [DashboardController::class, 'profile'])->name('user.profile');
        Route::get('/user/profile_edit/{id}', [DashboardController::class, 'edit'])->name('user.profile_edit');
        Route::put('/user/profile_update/{id}', [DashboardController::class, 'update'])->name('user.profile_update');
        // Route::get('admin/user/profile_edit/{id}', [DashboardController::class, 'edit'])->name('admin.user.profile_edit');
        // Route::get('/dashboard', function () {

        //     $data['header_title'] = 'Dashboard';
        //     $classRoomsCount = ClassRoom::count();
        //     // $timetableCount = Timetable::count();
        //     $teachersCount = Teacher::count('id');
        //     return view('admin/dashboard',compact('data','classRoomsCount', 'teachersCount'));
        //     // return view('admin.dashboard');
        // })->name('dashboard');

        Route::get('admin/list', [AdminController::class, 'list'])->name('admin.list');
        Route::get('admin/add', [AdminController::class, 'add']);
        Route::post('admin/add', [AdminController::class, 'insert']);
        Route::get('admin/edit/{id}', [AdminController::class, 'edit']);
        Route::post('admin/edit/{id}', [AdminController::class, 'update']);
        Route::get('admin/delete/{id}', [AdminController::class, 'delete']);


        //change_password
        Route::get('change_password', [UserController::class, 'change_password']);
        Route::post('change_password', [UserController::class, 'update_change_password']);

        //Student
        Route::get('student/list', [StudentController::class, 'list']);
        Route::get('student/add', [StudentController::class, 'add']);
        Route::post('student/add', [StudentController::class, 'insert']);

        Route::get('timetable/welcome', function (){
            return view('admin.timetable.welcome');
        });


        Route::resource('grade', GradeController::class);

        Route::get('class_room/update_is_outstanding', [ClassRoomController::class, 'updateIsOutstanding'])->name('class_room.update_is_outstanding');
        Route::resource('class_room', ClassRoomController::class);

        Route::resource('subject_type', SubjectTypeController::class);

        Route::resource('subject', SubjectController::class);
        Route::get('teacher/loadImportForm',[TeacherController::class,'loadImportForm'])->name('teacher.loadImportForm');
        Route::post('teacher/import',[TeacherController::class,'ImportTeacher'])->name('teacher.import');
        Route::get('teacher/export', [TeacherController::class, 'ExportTeacher'])->name('teacher.export');
        Route::get('teacher/{id}/timetable', [TeacherController::class, 'timetable'])->name('teacher.timetable');
        Route::resource('teacher', TeacherController::class);
        Route::resource('user', UserController::class);

        Route::get('timetable/generate', [TimeTableController::class, 'generateTimetable'])->name('timetable.generate');
        // Route::match(['get', 'post'], 'timetable/show_popup_create',[TimeTableController::class, "showPopupCreate"])->name('timetable.showPopupCreate');
        Route::get('timetable/show_popup_create',[TimeTableController::class, "showPopupCreate"])->name('timetable.showPopupCreate');
        Route::get('timetable/{id}/show_popup_edit',[TimeTableController::class, "showPopupEdit"])->name('timetable.showPopupEdit');
        // Route::get('timetable/create_slot',[TimeTableController::class, "showPopupCreate"])->name('timetable.create_slot');
        Route::get('timetable/create_academic_year',[TimeTableController::class, "academicYearCreate"])->name('timetable.academicYearCreate');
        Route::get('timetable/grade_list', [TimeTableController::class, 'getGradeList'])->name('timetable.grade_list');
        Route::post('timetable_slot', [TimetableSlotController::class, 'store'])->name('timetable_slot.store');
        Route::put('timetable_slot/{id}/update', [TimetableSlotController::class, 'update'])->name('timetable_slot.update');
        Route::get('timetable/{id}/adjustment', [TimeTableController::class, 'adjustment'])->name('timetable.adjustment');
        Route::get('timetable/{id}/weekly_adjustment', [TimeTableController::class, 'weeklyAdjustment'])->name('timetable.weekly_adjustment');
        Route::get('timetable_slot/{id}/popup_confirm', [TimetableSlotController::class, 'popupConfirm'])->name('timetable_slot.popupConfirm');
        Route::resource('timetable', TimeTableController::class);


        Route::resource('academic_year', AcademicYearController::class);

        //time_slot
        Route::resource('time_slot', SlotTimeController::class);

        Route::resource('roles', RoleController::class);

        // settings
        Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {

            Route::get('/', [SettingController::class, 'index'])->name('index');
            Route::put('/save', [SettingController::class, 'save'])->name('save');

            Route::get('/general', [SettingController::class, 'general'])->name('general.index');
            Route::put('/general/save', [SettingController::class, 'generalSave'])->name('general.save');

            // language setup
            Route::group(['prefix' => 'language', 'as' => 'language.'], function () {
                Route::get('/', [LanguageController::class, 'index'])->name('index');
                Route::get('/create', [LanguageController::class, 'create'])->name('create');
                Route::post('/', [LanguageController::class, 'store'])->name('store');
                Route::get('/edit', [LanguageController::class, 'edit'])->name('edit');
                Route::put('/update', [LanguageController::class, 'update'])->name('update');
                Route::delete('delete/', [LanguageController::class, 'delete'])->name('delete');

                Route::get('/update-status', [LanguageController::class, 'updateStatus'])->name('update-status');
                Route::get('/update-default-status', [LanguageController::class, 'update_default_status'])->name('update-default-status');
                Route::get('/translate', [LanguageController::class, 'translate'])->name('translate');
                Route::post('translate-submit/{lang}', [LanguageController::class, 'translate_submit'])->name('translate.submit');
            });
        });

        Route::group(['prefix' => 'report', 'as' => 'report.'], function () {
            Route::get('teacher', [ReportController::class, 'teacher'])->name('teacher');
            Route::get('class_teacher', [ReportController::class, 'classTeacher'])->name('class_teacher');
            Route::get('report/export', [ReportController::class, 'export'])->name('export');
            Route::get('report/class_teacher_export', [ReportController::class, 'classTeacherExport'])->name('class_teacher_export');
        });

        //for front
        Route::resource('school_news', NewsController::class);

        Route::resource('gallery', GalleryController::class);
    });

});






Route::group(['middleware' => 'teacher'], function () {
    Route::get('teacher/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('teacher/change_password', [UserController::class, 'change_password']);
    Route::post('teacher/change_password', [UserController::class, 'update_change_password']);
});

Route::group(['middleware' => 'student'], function () {
    Route::get('student/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('student/change_password', [UserController::class, 'change_password']);
    Route::post('student/change_password', [UserController::class, 'update_change_password']);
});

Route::group(['middleware' => 'parent'], function () {
    Route::get('parent/dashboard', [DashboardController::class, 'dashboard']);

    Route::get('parent/change_password', [UserController::class, 'change_password']);
    Route::post('parent/change_password', [UserController::class, 'update_change_password']);
});
