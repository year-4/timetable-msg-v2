<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'code',
        'gender',
        'phone',
        'email',
        'password',
        'address',
        'image',
        'type',
        'khmer_lname',
        'khmer_fname'
    ];

    protected $appends = ['khmer_fullname', 'full_name', 'image_url'];

    public function getImageUrlAttribute()
    {
        if (!empty($this->image) && file_exists(public_path('upload/users/' . $this->image))) {
            $image_url = asset('/upload/users/' . rawurlencode($this->image));
        } else {
            $image_url = asset('images/default.svg');
        }
        return $image_url;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
    public function getKhmerFullNameAttribute()
    {
        return $this->khmer_fname . ' ' . $this->khmer_lname;
    }


    public function classes()
    {
        return $this->hasMany(ClassRoom::class);
    }

    public function classroom()
    {
        return $this->hasOne(ClassRoom::class);
    }

    public function primarySubject()
    {
        return $this->belongsTo(Subject::class, 'primary_subject_id');
    }
    public function secondarySubject()
    {
        return $this->belongsTo(Subject::class, 'secondary_subject_id');
    }
    public function timetableSlots()
    {
        return $this->hasMany(TimetableSlot::class, 'teacher_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
