<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $table = 'gallery'; // Define the table name
    protected $appends = ['image_url'];
    public function getImageUrlAttribute()
    {
        if (!empty($this->image) && file_exists(public_path('upload/gallery/' . $this->image))) {
            $image_url = asset('/upload/gallery/' . rawurlencode($this->image));
        } else {
            $image_url = asset('images/default.svg');
        }
        return $image_url;
    }
    protected $fillable = [
        'image',
        'type',
    ];
}
