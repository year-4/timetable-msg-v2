<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Grade extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    protected $appends = ['subjects'];

    public function classes () {
        return $this->hasMany(ClassRoom::class);
    }
    public function getSubjectsAttribute () {
        $subjects = Subject::where('grade_id', 'LIKE', '%"'.$this->id .'"%')->get();
        return $subjects;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function (Builder $builder) {
            $builder->orderBy('name');
        });
    }

}
