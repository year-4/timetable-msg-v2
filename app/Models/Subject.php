<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    protected $casts = [
        'grade_id' => 'array',
    ];
    public function subjectType()
    {
        return $this->belongsTo(SubjectType::class, 'subject_type_id');
    }

    public function timetableSlots()
    {
        return $this->hasMany(TimetableSlot::class, 'subject_id');
    }
}
