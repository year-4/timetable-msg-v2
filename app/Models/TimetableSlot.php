<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimetableSlot extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * Get the timeSlot that owns the TimeTable
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timeSlot(): BelongsTo
    {
        return $this->belongsTo(SlotTime::class, 'time_slot_id');
    }

    /**
     * Get the subject that owns the TimeTable
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    /**
     * Get the teacher that owns the TimeTable
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    /**
     * Get the timetable that owns the TimetableSlot
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timetable(): BelongsTo
    {
        return $this->belongsTo(TimeTable::class, 'timetable_id');
    }
}
