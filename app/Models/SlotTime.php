<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SlotTime extends Model
{
    use HasFactory;
    protected $fillable = ['start_time', 'end_time', 'order'];
    protected $appends = ['daytime'];

    public function getDaytimeAttribute()
    {
        $time = Carbon::parse($this->start_time);
        $morningBoundary = Carbon::createFromTime(0, 0, 0);
        $afternoonBoundary = Carbon::createFromTime(12, 0, 0);
        $daytime = '';

        if ($time->lessThan($afternoonBoundary)) {
            $daytime = 'morning';
        } else {
            $daytime = 'afternoon';
        }

        return $daytime;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function (Builder $builder) {
            $builder->orderBy('start_time');
        });
    }

}
