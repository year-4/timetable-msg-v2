<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Support\Facades\Request;

class ClassRoom extends Model
{
    use HasFactory;

    protected $fillable = ['grade_id', 'teacher_id', 'name'];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function timetables()
    {
        return $this->hasMany(TimeTable::class, 'class_id');
    }

    public function timetable()
    {
        if (session()->has('current_academic_year')) {
            return $this->hasOne(TimeTable::class, 'class_id')->where('academic_year_id', session('current_academic_year')->id);
        }
        return $this->hasOne(TimeTable::class, 'class_id');
    }

    public function outstandSubject()
    {
        return $this->belongsTo(Subject::class, 'outstanding_subject');
    }

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope('timetable', function (Builder $builder) {
        //     $builder->with(['timetables' => function ($query) {
        //         return $query->where('academic_year_id', session('current_academic_year')->first()->id);
        //     }]);
        // });
    }

}
