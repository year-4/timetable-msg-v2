<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table = 'news';
    protected $appends = ['image_url'];
    public function getImageUrlAttribute()
    {
        if (!empty($this->image) && file_exists(public_path('upload/news/' . $this->image))) {
            $image_url = asset('/upload/news/' . rawurlencode($this->image));
        } else {
            $image_url = asset('images/default.svg');
        }
        return $image_url;
    }
    protected $fillable = [
        'title',
        'description',
        'image',
        'create_by',
    ];
    public function creator()
    {
        return $this->belongsTo(User::class, 'create_by');
    }
}
