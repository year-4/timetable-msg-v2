<?php

namespace App\Providers;

use Carbon\Carbon;
use App\Models\Setting;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Switch language
        view()->composer(['layouts.header', 'front.layouts.header',], function ($view) {
            $today = Carbon::now()->format('Y-m-d');

            $business_setting = new Setting;
            $languages = $business_setting->where('type', 'language')->first()->value;

            $langs = array_reduce(json_decode($languages, true), function ($result, $language) {
                if ($language['status'] == 1) {
                    $result[$language['name']] = $language['code'];
                }
                return $result;

            }, []);

            $view->with('current_locale', app()->getLocale());
            $view->with('available_locales', $langs);
        });

        // Set the app's default theme
        // dd(request()->cookie());
        if (!request()->hasCookie('dark_mode')) {
            config(['app.dark_mode' => config('app.theme')]);
        } else {
            // Rewrite the app's theme with the user's preference
            if (request()->cookie('dark_mode') == 1) {
                config(['app.dark_mode' => 1]);
            } else {
                config(['app.dark_mode' => 0]);
            }
        }

        // Implicitly grant "admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('admin') ? true : null;
        });

        paginator::useBootstrap();
    }
}
