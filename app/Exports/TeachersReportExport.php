<?php

namespace App\Exports;

use App\Models\Teacher;
use Worksheet\PageSetup;
use App\Models\TimeTable;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TeachersReportExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    use Exportable;

    // public function collection()
    // {
    //     $current_academic_year = session()->get('current_academic_year');
    //     $teachers = Teacher::withCount(['timetableSlots as total_teach_hour' => function($query) use ($current_academic_year) {
    //         $query->whereHas('timetable', function($q) use ($current_academic_year) {
    //             $q->where('academic_year_id', $current_academic_year->id);
    //         });
    //     }])
    //     ->with(['timetableSlots' => function ($query) use ($current_academic_year) {
    //         $query->whereHas('timetable', function($q) use ($current_academic_year) {
    //             $q->where('academic_year_id', $current_academic_year->id);
    //         });
    //     }])
    //     ->get()
    //     ->map(function ($row) {
    //         if (!$row->timetableSlots->isEmpty()) {
    //             $timetables_id = $row->timetableSlots->pluck('timetable_id')->toArray();
    //             $timetables_id = array_values(array_unique($timetables_id));
    //             $timetables = TimeTable::find($timetables_id);
    //             $timetables->map(function ($r) {
    //                 $r->room = $r->classroom->name;
    //                 return $r;
    //             });
    //             $row->classroom = $timetables->pluck('room');
    //         }
    //         return $row;
    //     });

    //     return $teachers;
    // }

    // public function headings(): array
    // {
    //     return [
    //         'ID',
    //         'Name',
    //         'Gender',
    //         'Code',
    //         'Type',
    //         'Total Teach (h)',
    //         'class teach',
    //     ];
    // }

    public function collection()
    {
        $current_academic_year = session()->get('current_academic_year');
        $teachers = Teacher::withCount(['timetableSlots as total_teach_hour' => function ($query) use ($current_academic_year) {
            $query->whereHas('timetable', function ($q) use ($current_academic_year) {
                $q->where('academic_year_id', $current_academic_year->id);
            });
        }])
            ->with(['timetableSlots.timetable.classroom'])
            ->get()
            ->map(function ($row) {
                $data = [
                    'ID' => $row->id,
                    'Name' => $row->full_name, // Change 'name' to the actual column name in your database
                    'Gender' => $row->gender == 1 ? __('Male') : __('Femle'), // Change 'gender' to the actual column name in your database
                    'Code' => $row->code,
                    'Type' => $row->type ? ucfirst(str_replace('_', ' ', $row->type)) : null,
                    'Total Teach' => $row->total_teach_hour != 0 ? $row->total_teach_hour : '0',
                ];

                if (!$row->timetableSlots->isEmpty()) {
                    $classrooms = $row->timetableSlots->pluck('timetable.classroom.name')->unique()->toArray();
                    $data['Class Teach'] = implode(', ', $classrooms);
                }

                return $data;
            });

        return $teachers;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Gender',
            'Code',
            'Type',
            'Total Teach',
            'Class Teach',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the header row
            1 => [
                'font' => ['bold' => true, 'color' => ['argb' => 'FFFFFF']],
                'fill' => ['fillType' => 'solid', 'startColor' => ['argb' => '55c94a']],
                'alignment' => ['horizontal' => 'center'], // Center align the header
                'borders' => [
                    'outline' => [
                        'borderStyle' => 'thin',
                        'color' => ['argb' => '000000'],
                    ],
                ],
            ],

            // Style data rows
            'A' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'left']], // Bold and center align the 'ID' column
            'B' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Name' column
            'C' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Gender' column
            'D' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Code' column
            'E' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
            'F' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
            'G' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
            'H' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDefaultRowDimension()->setRowHeight(-1);

                // Set column widths
                $event->sheet->getColumnDimension('A')->setWidth(10);
                $event->sheet->getColumnDimension('B')->setWidth(20);
                $event->sheet->getColumnDimension('C')->setWidth(15);
                $event->sheet->getColumnDimension('D')->setWidth(15);
                $event->sheet->getColumnDimension('E')->setWidth(300);
                $event->sheet->getColumnDimension('F')->setWidth(300);
                $event->sheet->getColumnDimension('G')->setWidth(300);



                // Adjust the row height for the header row
                $event->sheet->getRowDimension(1)->setRowHeight(30); // Adjust the height as needed

                // Set row height for data rows
                for ($row = 2; $row <= $event->sheet->getHighestRow(); ++$row) {
                    $event->sheet->getRowDimension($row)->setRowHeight(40); // Adjust the height as needed
                }
            },
        ];
    }
}


