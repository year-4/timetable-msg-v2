<?php

namespace App\Exports;

use App\Models\Teacher;
use App\Models\TimeTable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Events\AfterSheet;

class ClassTeachersReportExport implements FromCollection, WithHeadings, WithStyles
{
    use Exportable;

    // public function collection()
    // {
    //     $current_academic_year = session()->get('current_academic_year');
    //     $teachers = Teacher::withCount(['timetableSlots as total_teach_hour' => function($query) use ($current_academic_year) {
    //         $query->whereHas('timetable', function($q) use ($current_academic_year) {
    //             $q->where('academic_year_id', $current_academic_year->id);
    //         });
    //     }])
    //     ->with(['timetableSlots' => function ($query) use ($current_academic_year) {
    //         $query->whereHas('timetable', function($q) use ($current_academic_year) {
    //             $q->where('academic_year_id', $current_academic_year->id);
    //         });
    //     }])
    //     ->get()
    //     ->map(function ($row) {
    //         if (!$row->timetableSlots->isEmpty()) {
    //             $timetables_id = $row->timetableSlots->pluck('timetable_id')->toArray();
    //             $timetables_id = array_values(array_unique($timetables_id));
    //             $timetables = TimeTable::find($timetables_id);
    //             $timetables->map(function ($r) {
    //                 $r->room = $r->classroom->name;
    //                 return $r;
    //             });
    //             $row->classroom = $timetables->pluck('room');
    //         }
    //         return $row;
    //     });

    //     return $teachers;
    // }

    // public function headings(): array
    // {
    //     return [
    //         'ID',
    //         'Name',
    //         'Gender',
    //         'Code',
    //         'Type',
    //         'Total Teach (h)',
    //         'class teach',
    //     ];
    // }

    public function collection()
    {
        $current_academic_year = session()->get('current_academic_year');
        $teachers = Teacher::whereHas('classes')->with('classes')
                            ->get()
                            ->map(function ($row) {
                                $data = [
                                    'ID' => $row->id,
                                    'Name' => $row->full_name, // Change 'name' to the actual column name in your database
                                    'Gender' => $row->gender == 1 ? __('Male') : __('Femle'), // Change 'gender' to the actual column name in your database
                                    'Code' => $row->code,
                                    'Type' => $row->type ? ucfirst(str_replace('_', ' ', $row->type)) : null,
                                    'Class' => @$row->classes->first()->name,
                                ];

                                return $data;
                            });

        return $teachers;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Gender',
            'Code',
            'Type',
            'Class',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the header row
            1 => [
                'font' => ['bold' => true, 'color' => ['argb' => 'FFFFFF']],
                'fill' => ['fillType' => 'solid', 'startColor' => ['argb' => '55c94a']],
                'alignment' => ['horizontal' => 'center'], // Center align the header
            ],

            // Style data rows
            'A' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'left']], // Bold and center align the 'ID' column
            'B' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Name' column
            'C' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Gender' column
            'D' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Code' column
            'E' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
            'F' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
            'G' => ['font' => ['bold' => true], 'alignment' => ['horizontal' => 'center']], // Bold and center align the 'Type' column
        ];
    }

    // Set row height to auto
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDefaultRowDimension()->setRowHeight(-1);

                // Set column widths
                $event->sheet->getColumnDimension('A')->setWidth(10);
                $event->sheet->getColumnDimension('B')->setWidth(20);
                $event->sheet->getColumnDimension('C')->setWidth(15);
                $event->sheet->getColumnDimension('D')->setWidth(15);
                $event->sheet->getColumnDimension('E')->setWidth(300);
                $event->sheet->getColumnDimension('F')->setWidth(300);
                $event->sheet->getColumnDimension('G')->setWidth(300);



                // Adjust the row height for the header row
                $event->sheet->getRowDimension(1)->setRowHeight(30); // Adjust the height as needed
            },
        ];
    }
}


