<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SetFrontSessionData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $current_academic_year = AcademicYear::get()->filter(function ($year) {
            $now = Carbon::now();
            $year_array = explode('-', $year->name);
            $start_year = Carbon::parse($year_array[0] . '-01-01 00:00:00');
            $end_year = Carbon::parse($year_array[1] . '-01-01 00:00:00');

            if ($now->between($start_year, $end_year)) {
                return $year;
            }
        })->first();
        session()->put('current_academic_year', $current_academic_year);
        
        return $next($request);
    }
}
