<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateRedirectUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $allowedDomains = ['timetablekh.online'];

        $url = $request->input('url');

        $parsedUrl = parse_url($url);

        if (in_array($parsedUrl['host'], $allowedDomains)) {
            $response = $next($request);
            $response->header('Content-Security-Policy', "default-src 'self'");

            return $response;
        }

        // Handle invalid URL (e.g., redirect to a safe page)
        return redirect()->route('front.index');
    }
}
