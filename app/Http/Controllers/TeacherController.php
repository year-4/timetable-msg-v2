<?php

namespace App\Http\Controllers;

use App\Exports\TeacherExport;
use Exception;
use App\Models\User;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\SlotTime;
use App\Helper\AppHelper;
use App\Models\TimeTable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Imports\TeacherImport;
use App\Models\TimetableSlot;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->can('teacher.view')) {
            abort(403, 'Unauthorized action.');
        }

        $name = $request->name;
        $code = $request->code;
        $search = $request->input('search');

        $query = Teacher::when($name, function ($query) use ($name) {
                        return $query->where(function ($q) use ($name) {
                            $q->where('first_name', 'LIKE', '%'. $name .'%')
                            ->orWhere('last_name', 'LIKE', '%'. $name .'%')
                            ->orWhere('khmer_fname', 'LIKE', '%'. $name .'%')
                            ->orWhere('khmer_lname', 'LIKE', '%'. $name .'%');
                        });
                    })
                    ->when($code, function ($query) use ($code) {
                        // dd($code);
                        return $query->where('code', 'LIKE', '%'. $code . '%');
                    })
                    ->orderBy('first_name', 'ASC');

        // if ($search) {
        //     $query->where(function ($q) use ($search) {
        //         $q->where('first_name', 'LIKE', '%' . $search . '%')
        //             ->orWhere('last_name', 'LIKE', '%' . $search . '%')
        //             ->orWhere('username', 'LIKE', '%' . $search . '%')
        //             ->orWhere('email', 'LIKE', '%' . $search . '%');
        //     });

        //     $teachers = Teacher::orderBy('first_name', 'ASC')->paginate(10);
        //     return view('admin.teacher.index', compact('teachers'));
        // }

        $teachers = $query->paginate();

        if ($request->ajax()) {
            $view = view('admin.teacher.partials._table', compact('teachers'))->render();
            return response()->json([
                'view' => $view,
            ]);
        }

        return view('admin.teacher.index', compact('teachers', 'search'));
    }

    public function create()
    {
        if (!auth()->user()->can('teacher.create')) {
            abort(403, 'Unauthorized action.');
        }
        $subjects = Subject::pluck('name', 'id');
        $teacher_types = [
            'secondary_school' => __('Secondary School Teacher'),
            'high_school' => __('High School Teacher'),
        ];

        return view('admin.teacher.create', compact('subjects', 'teacher_types'));
    }
    public function store(Request $request)
    {
        if (!auth()->user()->can('teacher.create')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            DB::beginTransaction();

            $request->validate([
                'khmer_fname' => 'required',
                'khmer_lname' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'phone' => 'required|unique:teachers,phone|unique:users,email',
                'code' => 'required|unique:teachers,code',
                'email' => 'nullable|unique:users,email',
                'password' => 'required|min:8',
                'image' => 'image',
                'doe' => 'required',
                'primary_subject_id' => 'required',

            ]);

            // Create a new user instance
            $user = new User();
            $user->name = $request->first_name . ' ' . $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            if ($request->filled('image_names')) {
                $user->image = $request->image_names;
                $directory = public_path('upload/users');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/users/'. $request->image_names));

            }
            $user->first_name = trim($request->first_name);
            $user->last_name = trim($request->last_name);
            $user->gender = trim($request->gender);
            $user->save();
            $user->assignRole('teacher');

            $teacher = new Teacher;
            $teacher->khmer_fname = trim($request->khmer_fname);
            $teacher->khmer_lname = trim($request->khmer_lname);
            $teacher->first_name = trim($request->first_name);
            $teacher->last_name = trim($request->last_name);
            $teacher->username = trim($request->username);
            $teacher->code = trim($request->type);
            $teacher->code = trim($request->code);
            $teacher->gender = trim($request->gender);
            $teacher->phone = trim($request->phone);
            $teacher->type = $request->type;
            $teacher->email = trim($request->email);
            $teacher->password = trim($request->password);
            $teacher->address = trim($request->address);
            $teacher->doe = trim($request->doe);
            $teacher->primary_subject_id = $request->primary_subject_id;
            // $teacher->secondary_subject_id = $request->secondary_subject_id;
            $teacher->user_id = $user->id;

            if ($request->filled('image_names')) {
                $teacher->image = $request->image_names;
            }

            $teacher->save();

            DB::commit();

            return redirect()->route('admin.teacher.index')->with('success', 'Teacher Successfully Created');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function show(Teacher $teacher)
    {
        if (!auth()->user()->can('teacher.view')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.teacher.partials._detail', compact('teacher'));
    }

    public function edit($id)
    {
        if (!auth()->user()->can('teacher.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $subjects = Subject::pluck('name', 'id');
        $teacher = Teacher::findOrFail($id);
        $teacher_types = [
            'secondary_school' => __('Secondary School Teacher'),
            'high_school' => __('High School Teacher'),
        ];
        return view('admin.teacher.edit', compact('teacher', 'subjects', 'teacher_types'));
    }

    public function update(Request $request, Teacher $teacher)
    {
        try {
            if (!auth()->user()->can('teacher.edit')) {
                abort(403, 'Unauthorized action.');
            }
            $request->validate([
                'khmer_fname' => 'required',
                'khmer_lname' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'phone' => 'required|unique:teachers,phone,' . $teacher->id,
                'code' => 'required|unique:teachers,code,' . $teacher->id,
                'email' => 'nullable|unique:teachers,email,' . $teacher->id,
                'first_name' => 'required',
                'doe' => 'required',
                'primary_subject_id' => 'required',
            ]);

            DB::beginTransaction();

            $user = $teacher->user ?? new User();
            $user->name = $request->username;
            $user->email = $request->email;
            if ($request->filled('password')) {
                $user->password = Hash::make($request->password);
            }
            if ($request->filled('image_names')) {
                $user->image = $request->image_names;
            }
            $user->first_name = trim($request->first_name);
            $user->last_name = trim($request->last_name);
            $user->gender = trim($request->gender);
            $user->save();
            $user->syncRoles('teacher');

            // Update the teacher's information
            $teacher->khmer_fname = trim($request->khmer_fname);
            $teacher->khmer_lname = trim($request->khmer_lname);
            $teacher->first_name = trim($request->first_name);
            $teacher->last_name = trim($request->last_name);
            $teacher->username = trim($request->username);
            $teacher->code = trim($request->type);
            $teacher->code = trim($request->code);
            $teacher->gender = trim($request->gender);
            $teacher->phone = trim($request->phone);
            $teacher->email = trim($request->email);
            $teacher->doe = trim($request->doe);
            $teacher->primary_subject_id = $request->primary_subject_id;
            // $teacher->secondary_subject_id = $request->secondary_subject_id;
            $teacher->type = $request->type;
            $teacher->user_id = $user->id;
            if ($request->password) {
                $teacher->password = trim($request->password);
            }
            $teacher->address = trim($request->address);
            if ($request->filled('image_names')) {
                $teacher->image = $request->image_names;
                $directory = public_path('upload/users');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/users/'. $request->image_names));

            }

            $teacher->save();

            DB::commit();

            return redirect()->route('admin.teacher.index')->with('success', "Teacher Updated Successfully");
        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function destroy(Teacher $teacher)
    {
        if (!auth()->user()->can('teacher.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $teacher->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }

    public function renderTable()
    {
        $teachers = Teacher::orderBy('first_name', 'ASC')->paginate(10);
        $view = view('admin.teacher.partials._table', compact('teachers'))->render();

        return ['view' => $view, 'total' => $teachers->total()];
    }

    public function loadImportForm(){
        return view('admin.teacher.import-form');
    }
    public function ImportTeacher(Request $request){

        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);

        $file = $request->file('file');

        Excel::import(new TeacherImport, $file);

        return redirect()->route('admin.teacher.index')->with('success', 'Teacher Successfully Created');

    }
    public function ExportTeacher()
    {
        return Excel::download(new TeacherExport, 'teachers.xlsx');
    }

    public function timetable($id)
    {
        $week_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $time_slots = SlotTime::get();
        // dd(session('current_academic_year.id'));
        $timetable = TimeTable::where('academic_year_id', session('current_academic_year.id'))
                                ->where('grade_id', request('grade_id'))
                                ->where('class_id', request('class_id'))
                                ->first();

        $teacher = Teacher::with('timetableSlots')->find($id);
        // dd($timetable);
        $timetable_slots = TimetableSlot::whereHas('timetable', function ($query) {
            $query->where('academic_year_id', session('current_academic_year.id'));
        })
        ->where('teacher_id', $id)
        ->get();

        return view('admin.teacher.timetable', compact('week_days', 'time_slots', 'timetable', 'teacher', 'timetable_slots'));
    }

}
