<?php

namespace App\Http\Controllers;

use Exception;
use Termwind\render;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Teacher;
use App\Helper\AppHelper;
use App\Models\ClassRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class ClassRoomController extends Controller
{

    protected $types;

    public function __construct()
    {
        $this->types = ['real_science' => __('Real Science'), 'social_science' => __('Social Science')];
    }

    public function index()
    {
        if (!auth()->user()->can('class_room.view')) {
            abort(403, 'Unauthorized action.');
        }
        $classRooms = ClassRoom::orderBy('name', 'ASC')->paginate();
        $grades = Grade::orderBy('name', 'ASC')->paginate();
        $teachers = Teacher::orderBy('username', 'ASC')->paginate();
        return view('admin.class_room.index', compact('classRooms', 'grades', 'teachers'));
    }

    public function create()
    {
        if (!auth()->user()->can('class_room.create')) {
            abort(403, 'Unauthorized action.');
        }
        $grades = Grade::all();
        $teachers = Teacher::all();
        $types = $this->types;
        $subjects = Subject::pluck('name', 'id');
        return view('admin.class_room.partials._create', compact('grades', 'teachers', 'types', 'subjects'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('class_room.create')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:class_rooms,name',
            'grade_id' => 'required|exists:grades,id',
            'teacher_id' => 'nullable|unique:class_rooms,teacher_id|exists:teachers,id',
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added'),
            'grade_id.required' => __('Grade is required'),
            'grade_id.exists' => __('Invalid Grade'),
            'teacher_id.exists' => __('Invalid Teacher'),
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        $class_room = new ClassRoom;
        $class_room->grade_id = $request->grade_id;
        $class_room->teacher_id = $request->teacher_id;
        $class_room->name = $request->name;
        $class_room->type = $request->type ?? null;
        if ($request->has('is_outstanding_class')) {
            $class_room->is_outstanding_class = $request->is_outstanding_class ?? 0;
            $class_room->extra_hours = $request->extra_hours ?? 0;
            $class_room->outstanding_subject = $request->outstanding_subject;
        }
        $class_room->save();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }
    public function show(ClassRoom $classroom)
    {
        //
    }
    public function edit(ClassRoom $classRoom)
    {
        if (!auth()->user()->can('class_room.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $teachers = Teacher::all();
        $grades = Grade::all();
        $types = $this->types;
        $subjects = Subject::pluck('name', 'id');
        return view('admin.class_room.partials._edit', compact('classRoom', 'grades', 'teachers', 'types', 'subjects'));
    }

    public function update(Request $request, ClassRoom $classRoom)
    {
        if (!auth()->user()->can('class_room.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:class_rooms,name,' . $classRoom->id,
            'grade_id' => 'required|exists:grades,id',
            'teacher_id' => 'nullable|unique:class_rooms,teacher_id,' . $classRoom->id . '|exists:teachers,id',
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added'),
            'grade_id.required' => __('Grade ID is required'),
            'grade_id.exists' => __('Invalid Grade ID'),
            'teacher_id.exists' => __('Invalid Teacher ID'),
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        $classRoom->name = $request->name;
        $classRoom->grade_id = $request->grade_id;
        $classRoom->teacher_id = $request->teacher_id;
        $classRoom->type = $request->type;
        $classRoom->is_outstanding_class = $request->is_outstanding_class ?? 0;
        if ($request->has('is_outstanding_class')) {
            $classRoom->extra_hours = $request->extra_hours ?? 0;
            $classRoom->outstanding_subject = $request->outstanding_subject;
        }
        $classRoom->save();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }

    public function destroy($id)
    {
        if (!auth()->user()->can('class_room.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $classroom = ClassRoom::findOrFail($id);
        $classroom->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }



    public function renderTable()
    {
        $classRooms = ClassRoom::orderBy('name', 'ASC')->paginate();
        $grades = Grade::orderBy('name', 'ASC')->paginate();
        $teachers = Teacher::orderBy('username', 'ASC')->paginate();
        $view = view('admin.class_room.partials._table', compact('classRooms', 'grades', 'teachers'))->render();

        return ['view' => $view, 'total' => $classRooms->total()];
    }

    public function updateIsOutstanding(Request $request)
    {
        try {
            DB::beginTransaction();
            $classRooms = ClassRoom::findOrFail($request->id);
            $classRooms->is_outstanding_class = $classRooms->is_outstanding_class == 1 ? 0 : 1;
            $classRooms->save();
            $output = ['status' => 1, 'msg' => __('Class room updated')];

            DB::commit();
        } catch (Exception $e) {

            $output = ['status' => 0, 'msg' => __('Something went wrong')];
            DB::rollBack();
        }

        return response()->json($output);
    }
}
