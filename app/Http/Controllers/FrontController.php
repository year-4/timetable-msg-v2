<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Grade;
use App\Models\Gallery;
use App\Models\Setting;
use App\Models\Teacher;
use App\Models\SlotTime;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Models\ClassRoom;

use App\Models\TimeTable;
use Illuminate\View\View;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(Request $request)
    {
        $gradeId = $request->gradeId;
        $grades = Grade::withCount('classes')->get();
        $data['grades'] = $grades;
        $data['classRooms'] = ClassRoom::where('grade_id', $gradeId)->get();

        $data['timetableCount'] = TimeTable::count();
        $data['classRoomCount'] = ClassRoom::count();
        $data['teacherCount'] = Teacher::count('id');
        $data['news'] = News::latest()->paginate(3);
        $data['gallery'] = Gallery::latest()->paginate(6);

        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['vision'] = @$setting->where('type', 'vision')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['url'] = @$setting->where('type', 'url')->first()->value;
        $data['desc'] = @$setting->where('type', 'desc')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['active_image_names'] = @$setting->where('type', 'active_image_names')->first()->value;
        $data['vission_image_names'] = @$setting->where('type', 'vission_image_names')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.index', $data);
    }

    public function news()
    {
        $data['news'] = News::latest()->paginate(10);
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        // $data['vision'] = @$setting->where('type', 'vision')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        // $data['url'] = @$setting->where('type','url')->first()->value;
        // $data['desc'] = @$setting->where('type','desc')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        // $data['active_image_names'] = @$setting->where('type','active_image_names')->first()->value;
        // $data['vission_image_names'] = @$setting->where('type','vission_image_names')->first()->value;
        // $data['about_image_names'] = @$setting->where('type','about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.news', $data);
    }
    public function gallery()
    {
        $data['gallery'] = Gallery::latest()->paginate(12);
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        // $data['vision'] = @$setting->where('type', 'vision')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        // $data['url'] = @$setting->where('type','url')->first()->value;
        // $data['desc'] = @$setting->where('type','desc')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        // $data['active_image_names'] = @$setting->where('type','active_image_names')->first()->value;
        // $data['vission_image_names'] = @$setting->where('type','vission_image_names')->first()->value;
        // $data['about_image_names'] = @$setting->where('type','about_image_names')->first()->value;
        // $data['banner_text'] = @$setting->where('type','banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.gallery', $data);
    }

    public function newsdetail($id)
    {
        $data['news'] = News::latest()->paginate(3);
        $data['newsdetail'] = News::findOrFail($id);
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        // $data['vision'] = @$setting->where('type', 'vision')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        // $data['url'] = @$setting->where('type','url')->first()->value;
        // $data['desc'] = @$setting->where('type','desc')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        // $data['active_image_names'] = @$setting->where('type','active_image_names')->first()->value;
        // $data['vission_image_names'] = @$setting->where('type','vission_image_names')->first()->value;
        // $data['about_image_names'] = @$setting->where('type','about_image_names')->first()->value;
        // $data['banner_text'] = @$setting->where('type','banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.newsdetail', $data);
    }
    public function about()
    {
        $data['timetableCount'] = TimeTable::count();
        $data['classRoomCount'] = ClassRoom::count();
        $data['teacherCount'] = Teacher::count('id');
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.about', $data);
    }
    public function contact()
    {
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('front.contact', $data);
    }
    public function timetable()
    {
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;

        $data['class_rooms'] = ClassRoom::has('timetable')
            ->where('grade_id', request('grade_id'))
            ->get();

        return view('front.timetable.index', $data);
    }

    public function timetableDetail(Request $request, $id)
    {
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;

        $data['week_days'] = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $data['time_slots'] = SlotTime::get();
        if ($id != 0) {
            $data['timetable'] = $timetable = TimeTable::find($id);

            $data['timetable_slots'] = $timetable->slots;
        }

        return view('front.timetable.detail', $data);
    }
    public function timetablepdf(Request $request, $id)
    {
        $setting = new Setting();
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;

        $data['week_days'] = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $data['time_slots'] = SlotTime::get();
        $data['imagePath'] = public_path('images/logo.png');

        if ($id != 0) {
            $data['timetable'] = $timetable = TimeTable::find($id);
            $data['timetable_slots'] = $timetable->slots;

            // Load the view with data
            $view = view('front.timetable.timetablepdf', $data)->render();

            $pdf = PDF::loadHTML($view)->setOptions(['defaultFont' => 'sans-serif']);
            $pdf->setPaper('a4', 'landscape');
            $pdf->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

            return $pdf->stream('timetable_report.pdf');
        }
        return view('front.timetable.timetablepdf', $data);
    }
}
