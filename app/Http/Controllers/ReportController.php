<?php

namespace App\Http\Controllers;

use App\Exports\ClassTeachersReportExport;
use App\Models\Teacher;
use App\Models\TimeTable;
use Illuminate\Http\Request;
use App\Exports\TeacherExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TeachersReportExport;

class ReportController extends Controller
{
    public function teacher(Request $request)
    {
        $name = $request->name;
        $code = $request->code;

        $current_academic_year = session()->get('current_academic_year');
        $teachers = Teacher::withCount(['timetableSlots as total_teach_hour' => function($query) use ($current_academic_year) {
                                    $query->whereHas('timetable', function($q) use ($current_academic_year) {
                                        $q->where('academic_year_id', $current_academic_year->id);
                                    });
                                }])
                                ->with(['timetableSlots' => function ($query) use ($current_academic_year) {
                                    $query->whereHas('timetable', function($q) use ($current_academic_year) {
                                        $q->where('academic_year_id', $current_academic_year->id);
                                    });
                                }])
                                ->when($name, function ($query) use ($name) {
                                    return $query->where(function ($q) use ($name) {
                                        $q->where('first_name', 'LIKE', '%'. $name .'%')
                                        ->orWhere('last_name', 'LIKE', '%'. $name .'%')
                                        ->orWhere('khmer_fname', 'LIKE', '%'. $name .'%')
                                        ->orWhere('khmer_lname', 'LIKE', '%'. $name .'%');
                                    });
                                })
                                ->when($code, function ($query) use ($code) {
                                    // dd($code);
                                    return $query->where('code', 'LIKE', '%'. $code . '%');
                                })
                                ->paginate(10);

        $teachers->getCollection()->map(function ($row) {
                                    if (!$row->timetableSlots->isEmpty()) {
                                        $timetables_id = $row->timetableSlots->pluck('timetable_id')->toArray();
                                        $timetables_id = array_values(array_unique($timetables_id));
                                        $timetables = TimeTable::find($timetables_id);
                                        $timetables->map(function ($r) {
                                            $r->room = $r->classroom->name;
                                            return $r;
                                        });
                                        $row->classroom = $timetables->pluck('room');
                                    }
                                    return $row;
                                });

        if ($request->ajax()) {
            $view = view('admin.reports.partials._teacher_table', compact('teachers'))->render();
            return response()->json([
                'view' => $view,
            ]);
        }

        return view('admin.reports.teacher', compact('teachers'));
    }

    public function classTeacher(Request $request)
    {
        $name = $request->name;
        $code = $request->code;

        $teachers = Teacher::has('classroom')
                            ->when($name, function ($query) use ($name) {
                                return $query->where(function ($q) use($name) {
                                    $q->where('first_name', 'LIKE', '%'. $name .'%')
                                    ->orWhere('last_name', 'LIKE', '%'. $name .'%')
                                    ->orWhere('khmer_fname', 'LIKE', '%'. $name .'%')
                                    ->orWhere('khmer_lname', 'LIKE', '%'. $name .'%');
                                });
                            })
                            ->when($code, function ($query) use ($code) {
                                return $query->where('code', 'LIKE', '%'. $code .'%');
                            })
                            ->paginate(10);

        if ($request->ajax()) {
            // dd($teachers);
            // dd($request->all());
            $view = view('admin.reports.partials._class_teacher', compact('teachers'))->render();
            return response()->json([
                'view' => $view,
            ]);
        }

        return view('admin.reports.class_teacher', compact('teachers'));
    }

    public function classTeacherExport()
    {
        return Excel::download(new ClassTeachersReportExport(), 'ClassTeacher_Report.xlsx');
    }

    public function export()
    {
        return Excel::download(new TeachersReportExport(), 'Teacher_Report.xlsx');
    }
}
