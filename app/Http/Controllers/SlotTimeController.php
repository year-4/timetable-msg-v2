<?php

namespace App\Http\Controllers;

use App\Models\SlotTime;
use App\Helper\AppHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SlotTimeController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('time_slot.view')) {
            abort(403, 'Unauthorized action.');
        }
        $timeslots = SlotTime::orderBy('start_time', 'ASC')->paginate();

        return view('admin.time_slot.index', compact('timeslots'));
    }

    public function create()
    {

        if (!auth()->user()->can('time_slot.create')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.time_slot.partials._create');
    }
    public function store(Request $request)
    {
        if (!auth()->user()->can('time_slot.create')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'order' => 'nullable|integer',
        ], [
            'start_time.required' => __('Start Time is required'),
            'start_time.date_format' => __('Start Time should be in the format HH:MM'),
            'end_time.required' => __('End Time is required'),
            'end_time.date_format' => __('End Time should be in the format HH:MM'),
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        $timeSlot = SlotTime::create([
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'order' => $request->order,
        ]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $timeSlot,
            'total' => $total,
        ]);
    }

    public function show(SlotTime $slotTime)
    {

    }
    public function edit(String $id)
    {
        if (!auth()->user()->can('time_slot.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $timeslot = SlotTime::findOrFail($id);

        return view('admin.time_slot.partials._edit', compact('timeslot'));
    }

    public function update(Request $request, string $id)
    {
        if (!auth()->user()->can('time_slot.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $timeSlot = SlotTime::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
            'order' => 'nullable|integer',
        ], [
            'start_time.required' => __('Start Time is required'),
            'start_time.date_format' => __('Start Time should be in the format HH:MM'),
            'end_time.required' => __('End Time is required'),
            'end_time.date_format' => __('End Time should be in the format HH:MM'),
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $timeSlot->update([
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'order' => $request->order,
        ]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $timeSlot,
            'total' => $total,
        ]);
    }

    public function destroy(String $id)
    {
        if (!auth()->user()->can('time_slot.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $timeslot = SlotTime::findOrFail($id);

        $timeslot->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function renderTable()
    {
        $timeslots = SlotTime::orderBy('start_time', 'ASC')->paginate();
        $view = view('admin.time_slot.partials._table', compact('timeslots'))->render();

        return ['view' => $view, 'total' => $timeslots->total()];
    }
}
