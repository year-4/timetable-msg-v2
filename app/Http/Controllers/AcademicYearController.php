<?php

namespace App\Http\Controllers;
use App\Helper\AppHelper;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function Termwind\render;
class AcademicYearController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('academic_year.view')) {
            abort(403, 'Unauthorized action.');
        }
        $academicyears = AcademicYear::orderBy('name', 'ASC')->paginate();

        return view('admin.academic_year.index', compact('academicyears'));
    }
    public function create()
    {
        if (!auth()->user()->can('academic_year.create')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.academic_year.partials._create');
    }
    public function store(Request $request)
    {
        if (!auth()->user()->can('academic_year.create')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:academic_years,name',
        ], [
            'name.required' => __('Academic Year Name is required'),
            'name.unique'   => __('This Academic Year is already added')
        ]);

        if ($validator->errors()->count() > 0) {
          
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        AcademicYear::Create(['name' => $request->name]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }
    public function show(string $id)
    {
        
    }
    public function edit(string $id)
    {
        if (!auth()->user()->can('academic_year.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $academicyear = AcademicYear::findOrFail($id);

        return view('admin.academic_year.partials._edit', compact('academicyear'));
    }

    
    public function update(Request $request, string $id)
    {
        if (!auth()->user()->can('academic_year.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $academicyear = AcademicYear::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:academic_years,name,' . $academicyear->id,
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added')
        ]);
    
        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }
    
        $academicyear->name = $request->name;
        $academicyear->save();
    
        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];
    
        return response()->json([
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);

    }
    public function destroy(string $id)
    {
        if (!auth()->user()->can('academic_year.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $academicyear = AcademicYear::findOrFail($id);

        $academicyear->delete();
    
        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];
    
        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function renderTable () {
        $academicyears = AcademicYear::orderBy('name', 'ASC')->paginate();
        $view = view('admin.academic_year.partials._table', compact('academicyears'))->render();

        return ['view' => $view, 'total' => $academicyears->total()];
    }
}
