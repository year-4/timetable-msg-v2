<?php

namespace App\Http\Controllers;
use Exception;
use App\Models\Grade;
use App\Models\Subject;
use App\Helper\AppHelper;
use App\Models\SubjectType;
use App\Models\SubjectModel;
use Illuminate\Http\Request;
use function Termwind\render;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class SubjectController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('subject.view')) {
            abort(403, 'Unauthorized action.');
        }
        $subjects = Subject::orderBy('name', 'ASC')->paginate();

        return view('admin.subject.index', compact('subjects'));
    }
    public function create()
    {
        if (!auth()->user()->can('subject.create')) {
            abort(403, 'Unauthorized action.');
        }
        $grades = Grade::orderBy('name', 'ASC')->pluck('name', 'id');
        $subject_types = SubjectType::pluck('name', 'id');
        return view('admin.subject.partials._create', compact('subject_types', 'grades'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('subject.create')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:subjects,name',
                // 'subject_type_id' => 'required',
                'grade' => 'required',
            ], [
                'name.required' => __('Subject Name is required'),
                // 'subject_type_id.required' => __('Subject Type is required'),
                'grade.required' => __('Grade is required'),

            ]);

            if ($validator->errors()->count() > 0) {
                return response()->json(['errors' => AppHelper::error_processor($validator)]);
            }

            DB::beginTransaction();
            $subject = new Subject();
            $subject->name = $request->name;
            $subject->grade_id = $request->grade;
            $subject->subject_type_id = $request->subject_type_id;
            $subject->save();

            DB::commit();

            $table = $this->renderTable();
            $view = $table['view'];
            $total = $table['total'];

            return response()->json([
                'success' => true,
                'view' => $view,
                'data' => $request->all(),
                'total' => $total,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json([
                'errors' => [
                    [
                        'message' => __('Something went wrong')
                    ]
                ],
            ]);
        }

        Subject::create(['name' => $request->name]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }

    public function show(Grade $grade)
    {
        //
    }

    public function edit(Subject $subject)
    {
        if (!auth()->user()->can('subject.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $grades = Grade::orderBy('name', 'ASC')->pluck('name', 'id');
        $subject_types = SubjectType::pluck('name', 'id');

        return view('admin.subject.partials._edit', compact('subject', 'subject_types', 'grades'));
    }

    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('subject.edit')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:subjects,name,' . $id,
                // 'subject_type_id' => 'required',
                'grade' => 'required',
            ], [
                'name.required' => __('Subject Name is required'),
                // 'subject_type_id.required' => __('Subject Type is required'),
                'grade.required' => __('Grade is required'),

            ]);

            if ($validator->errors()->count() > 0) {
                return response()->json(['errors' => AppHelper::error_processor($validator)]);
            }

            DB::beginTransaction();

            $subject = Subject::findOrFail($id);
            $subject->name = $request->name;
            $subject->grade_id = $request->grade;
            $subject->subject_type_id = $request->subject_type_id;
            $subject->save();

            DB::commit();

            $table = $this->renderTable();
            $view = $table['view'];
            $total = $table['total'];

            return response()->json([
                'success' => true,
                'view' => $view,
                'data' => $request->all(),
                'total' => $total,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json([
                'errors' => [
                    [
                        'message' => __('Something went wrong')
                    ]
                ],
            ]);
        }

        Subject::create(['name' => $request->name]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);

    }

    public function destroy(Subject $subject)
    {
        if (!auth()->user()->can('subject.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $subject->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }

    public function renderTable () {
        $subjects = Subject::orderBy('name', 'ASC')->paginate(10);
        $view = view('admin.subject.partials._table', compact('subjects'))->render();

        return ['view' => $view, 'total' => $subjects->total()];
    }
}
