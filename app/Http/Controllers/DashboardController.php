<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Setting;
use App\Models\Subject;
use App\Models\Teacher;

use App\Models\ClassRoom;
use App\Models\TimeTable;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use function Termwind\render;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{

    public function dashboard()
    {
        $classRoomsCount = ClassRoom::count('id');
        $timetableCount = TimeTable::count('id'); // Fix typo: Change TimeTable to Timetable
        $teachersCount = Teacher::count('id');
        $academicyearsCount = AcademicYear::count('id');



        // $userData = User::select(
        //     DB::raw("COUNT(*) as count"),
        //     DB::raw("YEAR(created_at) as year"),
        //     DB::raw("MONTH(created_at) as month")
        // )
        //     ->groupBy('year', 'month')
        //     ->orderBy('year', 'asc')
        //     ->orderBy('month', 'asc')
        //     ->get();

        // $userData = $userData->map(function ($row) {
        //     $row->count = intval($row->count);
        //     $row->year = intval($row->year);
        //     $row->month = intval($row->month);
        //     return $row;
        // });



        // $userData = User::select(DB::raw("COUNT(*) as count"))
        // ->whereYear('created_at', date('Y'))
        // ->groupBy(DB::raw("Month(created_at)"))
        // ->pluck('count');

        // $userGroupedData = $userData->groupBy('year');
        // $allYears = $userData->pluck('year')->unique()->sort()->toArray();
        // $allYears = array_values($allYears);
        // $allYears = collect($allYears)->map(function ($value) {
        //     return intval($value);
        // })->all();

        $userData = User::select(
            DB::raw("COUNT(*) as count"),
            DB::raw("YEAR(created_at) as year"),
            DB::raw("MONTH(created_at) as month")
        )
        ->groupBy('year', 'month')
        ->orderBy('year', 'asc')
        ->orderBy('month', 'asc')
        ->get()
        ->map(function ($row) {
            $row->count = intval($row->count);
            return $row;
        });

        $userGroupedData = $userData->groupBy('year');
        $allYears = $userData->pluck('year')->unique()->sort()->toArray();
        $allYears = array_values($allYears);
        $allYears = collect($allYears)->map(function ($value) {
            return intval($value);
        })->all();

        $teacherLabels = [];
        $userSeries = [];

        $userSeries = User::select(\DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(\DB::raw("MONTH(created_at)"))
            ->pluck('count')
            ->map(function ($row) {
                return intval($row);
            })
            ->toArray();

        // If you want to ensure you have counts for all months (even if they are zero), you can initialize an array with zero counts for each month
        $allMonths = [];
        for ($month = 0; $month < 12; $month++) {
            $allMonths[$month] = 0;
        }

        // Assign counts from $userSeries to corresponding months
        foreach ($userSeries as $month => $count) {
            $allMonths[$month] = $count;
        }
        $userSeries = array_values($allMonths);

        // $teachersData = Teacher::get();
        $teachersData = Teacher::withTrashed()->select(\DB::raw("COUNT(*) as count"), 'doe')
            ->where('doe', '!=', null)
            ->orderBy('doe')
            ->groupBy('doe')
            ->pluck('count', 'doe')
            ->toArray();

        $first_year = array_key_first($teachersData);
        $allTeachersYears = [];
        for ($i = $first_year; $i <= date('Y'); $i++) {
            $allTeachersYears[$i] = 0;
        }
        // dd($allTeachersYears);

        $teacherSeries = [];

        // dd($teachersData[1975]);
        foreach ($allTeachersYears as $year => $teacher_count) {
            $teacherSeries[$year] = intval($teachersData[$year] ?? 0);
        }

        // dd($teacherSeries, $teacherLabels);
        $timetableCount = TimeTable::count('id');
        $classRoomsCount = ClassRoom::count('id');

        $timetableData = TimeTable::select(
            'academic_year_id',
            'grade_id',
            'class_id',
            'created_by',
            'created_at',
            'updated_at'
        )->get();

        $classRoomsData = ClassRoom::select(
            'grade_id',
            'teacher_id',
            'name',
            'created_at',
            'updated_at',
            'order',
            'type',
            'is_outstanding_class',
            'extra_hours',
            'outstanding_subject'
        )->get();
        // return $teacherSeries;
        return view('admin.dashboard', compact('userData','allYears','teacherSeries','teacherLabels',
            'classRoomsCount',
            'teachersCount',
            'academicyearsCount',
            'timetableCount',
            'timetableData',
            'classRoomsData' ,
            'userSeries'
        ));
    }

    public function profile()
    {
        $user = User::with('teacher')->find(auth()->id());

        return view('admin/user/profile', compact('user'));
    }


    public function edit($id)
    {
        $subjects = Subject::pluck('name', 'id');
        $user = User::find($id);
        // $teacher = Teacher::findOrFail($id);
        $teacher_types = [
            'secondary_school' => __('Secondary School Teacher'),
            'hight_school' => __('High School Teacher'),
        ];
        return view('admin.user.profile_edit', compact('user', 'subjects', 'teacher_types'));
    }

    public function update(Request $request, $id)
    {


        $request->validate([

            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required',
            // 'type' => 'required',
            'password' => 'nullable|min:8',
            'image' => 'image',
        ]);

        try {
            DB::beginTransaction();

            // Update the user's information
            $user = User::find($id);
            $user->name = trim($request->name);
            $user->first_name = trim($request->first_name);
            $user->last_name = trim($request->last_name);
            $user->gender = trim($request->gender);
            $user->email  = trim($request->email);
            $user->type = trim($request->type);
            if ($request->password) {
                $user->password = trim($request->password);
            }
            if ($request->filled('image_names')) {
                $user->image = $request->image_names;
                $directory = public_path('upload/users');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/users/'. $request->image_names));

            }

            $role_id        = $request->role;
            $user_role      = $user->roles->first();
            $previous_role  = !empty($user_role->id) ? $user_role->id : 0;


            $user->save();

            DB::commit();

            return redirect()->route('admin.user.profile')->with('success', "User Updated Successfully");
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
}
