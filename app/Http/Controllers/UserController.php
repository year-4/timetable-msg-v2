<?php

namespace App\Http\Controllers;

use File;
use Exception;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(Request $request)
    {
        if (!auth()->user()->can('user.view')) {
            abort(403, 'Unauthorized action.');
        }
        $search = $request->input('search');
        $name = $request->name;

        $query = User::when($name, function ($query) use ($name) {
                        return $query->where(function ($q) use ($name) {
                            $q->where('first_name', 'LIKE', '%'. $name .'%')
                            ->orWhere('last_name', 'LIKE', '%'. $name .'%')
                            ->orWhere('name', 'LIKE', '%'. $name .'%');
                        });
                    })
                    ->orderBy('name', 'ASC');

        $users = $query->paginate();

        if ($request->ajax()) {
            $view = view('admin.user.partials._table', compact('users'))->render();
            return response()->json([
                'view' => $view,
            ]);
        }

        return view('admin.user.index', compact('users', 'search'));
    }

    public function create()
    {
        if (!auth()->user()->can('user.create')) {
            abort(403, 'Unauthorized action.');
        }
        $roles = Role::pluck('name', 'id');
        return view('admin.user.create',compact('roles'));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('user.create')) {
            abort(403, 'Unauthorized action.');
        }
        $request->validate([
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required',
            // 'type' => 'required',
            'password' => 'required|min:8',
            'image' => 'image',
        ]);

        try {
            DB::beginTransaction();

            $user = new User;
            $user->name = trim($request->name);
            $user->first_name = trim($request->first_name);
            $user->last_name = trim($request->last_name);
            $user->gender = trim($request->gender);
            $user->email  = trim($request->email);
            $user->type = trim($request->type);
            $user->password = trim($request->password);

            if ($request->filled('image_names')) {
                $user->image = $request->image_names;
                $directory = public_path('upload/users');
                if (!File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/users/'. $request->image_names));

            }

            $role = Role::findOrFail($request->role);
            $user->assignRole($role->name);

            $user->save();

            DB::commit();

            DB::commit();

            return redirect()->route('admin.user.index')->with('success', 'User Successfully Created');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function edit($id)
    {
        if (!auth()->user()->can('user.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id');
        return view('admin.user.edit', compact('user', 'roles'));
    }
    public function update(Request $request, User $user)
    {
        if (!auth()->user()->can('user.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $request->validate([

            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'email' => 'required',
            // 'type' => 'required',
            'password' => 'nullable|min:8',
            'image' => 'image',
        ]);

        try {
            DB::beginTransaction();

            // Update the user's information
            $user->name = trim($request->name);
            $user->first_name = trim($request->first_name);
            $user->last_name = trim($request->last_name);
            $user->gender = trim($request->gender);
            $user->email  = trim($request->email);
            $user->type = trim($request->type);
            if ($request->password) {
                $user->password = trim($request->password);
            }
            if ($request->filled('image_names')) {
                $user->image = $request->image_names;
                $directory = public_path('upload/users');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/users/'. $request->image_names));

            }

            $role_id        = $request->role;
            $user_role      = $user->roles->first();
            $previous_role  = !empty($user_role->id) ? $user_role->id : 0;
            if ($previous_role != $role_id) {
                if (!empty($previous_role)) {
                    $user->removeRole($user_role->name);
                }

                $role = Role::findOrFail($role_id);
                $user->assignRole($role->name);
            }

            $user->save();

            DB::commit();

            return redirect()->route('admin.user.index')->with('success', "User Updated Successfully");
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }


    public function show(User $user)
    {
        if (!auth()->user()->can('user.view')) {
            abort(403, 'Unauthorized action.');
        }

        return view('admin.user.partials._detail', compact('user'));

    }

    public function renderTable()
    {
        $users = User::orderBy('first_name', 'ASC')->paginate(10);
        $view = view('admin.user.partials._table', compact('users'))->render();

        return ['view' => $view, 'total' => $users->total()];
    }


    public function change_password()
    {
        $data['header_title'] = "Change Password";
        return view("profile.change_password", $data);
    }
    public function destroy(User $user)
    {
        if (!auth()->user()->can('user.delete')) {
            abort(403, 'Unauthorized action.');
        }

        $user->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];
        // $grades

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function update_change_password(Request $request)
    {

        $user = User::getSingle(Auth::user()->id);
        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->new_password);
            $user->save();

            return redirect()->back()->with('success', "Password successfully updated");
        } else {
            return redirect()->back()->with('error', "Old Password is not correct");
        }
    }
}
