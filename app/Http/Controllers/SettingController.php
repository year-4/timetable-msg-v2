<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Grade;
use App\Models\Setting;
use App\Models\Subject;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function index()
    {
        if (!auth()->user()->hasRole('admin')) {
            abort(403, 'Unauthorized action.');
        }
        $data['grades'] = Grade::orderBy('name')->get();
        $data['subjects'] = Subject::get();

        $setting = new Setting();
        $data['teacher_limit'] = null;
        $data['subject_limit'] = null;
        $data['limit_hour'] = @$setting->where('type', 'limit_hour')->first()->value;
        if ($data['limit_hour']) {
            $data['teacher_limit'] = json_decode($data['limit_hour'], true)['teacher'];
            $data['subject_limit'] = json_decode($data['limit_hour'], true)['subject'];
        }
        return view('admin.setting.index', $data);
    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            $all_input = $request->all();
            foreach ($all_input as $input_name => $input_value) {
                if (!in_array($input_name, ['_token', '_method'])) {
                    Setting::updateOrCreate(
                        [
                            'type' => $input_name,
                        ],
                        [
                            'value' => is_array($input_value) ? json_encode($input_value) : $input_value,
                        ]
                    );
                }
            }
            DB::commit();

            return redirect()->back()->with('success', "Setting Updated Successfully");
        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();

            return redirect()->back()->with('error', __('Sumething went wrong'))->withInput();
        }
    }

    public function general()
    {
        if (!auth()->user()->hasRole('admin')) {
            abort(403, 'Unauthorized action.');
        }

        $language = Setting::where('type', 'language')->first();
        $data['language'] = $language->value ?? null;
        $default_lang = 'en';
        $data['default_lang'] = json_decode($data['language'], true)[0]['code'];

        $setting = new Setting();
        $data['settings'] = Setting::withoutGlobalScopes()->with('translations')->get();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['vision'] = @$setting->where('type', 'vision')->first()->value;
        $data['app_name'] = @$setting->where('type', 'app_name')->first()->value;
        $data['url'] = @$setting->where('type', 'url')->first()->value;
        $data['desc'] = @$setting->where('type', 'desc')->first()->value;
        $data['copyright'] = @$setting->where('type', 'copyright')->first()->value;
        $data['address'] = @$setting->where('type', 'address')->first()->value;
        $data['phone_number'] = @$setting->where('type', 'phone_number')->first()->value;
        $data['phone_number1'] = @$setting->where('type', 'phone_number1')->first()->value;
        $data['email'] = @$setting->where('type', 'email')->first()->value;
        $data['facebook'] = @$setting->where('type', 'facebook')->first()->value;
        $data['telegram'] = @$setting->where('type', 'telegram')->first()->value;
        $data['active_image_names'] = @$setting->where('type', 'active_image_names')->first()->value;
        $data['vission_image_names'] = @$setting->where('type', 'vission_image_names')->first()->value;
        $data['about_image_names'] = @$setting->where('type', 'about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type', 'banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type', 'banner_image_names')->first()->value;
        return view('admin.setting.general', $data);
    }

    public function generalSave(Request $request)
    {
        try {
            DB::beginTransaction();
            $all_input = $request->all();
            foreach ($all_input as $input_name => $input_value) {

                //Save Image
                if (in_array($input_name, ['active_image_names','vission_image_names','about_image_names','banner_image_names'])) {
                    if ($request->filled($input_name)) {
                        $directory = public_path('upload/settings');
                        if (!\File::exists($directory)) {
                            \File::makeDirectory($directory, 0777, true);
                        }
                        $image = \File::move(public_path('upload/temp/' . $request->$input_name), public_path('upload/settings/' . $request->$input_name));
                        Setting::updateOrCreate(
                            [
                                'type' => $input_name,
                            ],
                            [
                                'value' => $request->$input_name,
                            ]
                        );
                    }
                    continue;
                }
                // save text
                if (!in_array($input_name, ['_token', '_method', 'lang'])) {
                    if (in_array($input_name, ['about_us', 'vision', 'app_name', 'desc', 'copyright', 'banner_text'])) {

                        Setting::updateOrCreate(
                            [
                                'type' => $input_name,
                            ],
                            [
                                'value' => $input_value[array_search('en', $request->lang)],
                            ]
                        );
                        $setting = Setting::where('type', $input_name)->first();

                        $data = [];
                        foreach ($request->lang as $index => $key) {
                            Translation::updateOrInsert(
                                [
                                    'translationable_type' => 'App\Models\Setting',
                                    'translationable_id' => $setting->id,
                                    'locale' => $key,
                                    'key' => $input_name
                                ],
                                ['value' => $request->$input_name[$index]]
                            );
                        }
                        Translation::insert($data);
                    } else {
                        Setting::updateOrCreate(
                            [
                                'type' => $input_name,
                            ],
                            [
                                'value' => $input_value,
                            ]
                        );
                    }
                }
            }
            DB::commit();

            return redirect()->back()->with('success', "Setting Updated Successfully");
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();

            return redirect()->back()->with('error', __('Sumething went wrong'))->withInput();
        }
    }
}
