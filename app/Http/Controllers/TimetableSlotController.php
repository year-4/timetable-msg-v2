<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Subject;
use App\Helper\AppHelper;
use App\Models\ClassRoom;
use App\Models\Grade;
use App\Models\Setting;
use App\Models\Teacher;
use App\Models\TimeTable;
use Illuminate\Http\Request;
use App\Models\TimetableSlot;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TimetableSlotController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'teacher' => 'required',
            ], [
                'subject.required' => __('Subject is required'),
                'teacher.required' => __('Teacher is required'),
            ]);

            if ($validator->errors()->count() > 0) {
                return response()->json(['errors' => AppHelper::error_processor($validator)]);
            }

            DB::beginTransaction();

            $timetable = Timetable::where([
                'academic_year_id' => $request->academic_year_id,
                'grade_id' => $request->grade_id,
                'class_id' => $request->class_id,
            ])->first();

            // check if doesn't have timetabel then create one
            if (!$timetable) {
                $timetable = new TimeTable;
                $timetable->academic_year_id = $request->academic_year_id;
                $timetable->grade_id = $request->grade_id;
                $timetable->class_id = $request->class_id;
                $timetable->created_by = auth()->user()->id;
                $timetable->save();
            }


            $subjects = Subject::withCount(['timetableSlots' => function($query) use ($timetable, $request) {
                            $query->whereHas('timetable', function($q) use ($request, $timetable) {
                                $q->where('academic_year_id', $request->academic_year_id)
                                ->where('timetable_id', $timetable->id);
                            });
                        }])->get();

            $subject_count = [];
            foreach ($subjects as $subject) {
                $subject_count[$subject->id] = $subject->timetable_slots_count;
            }

            $teachers = Teacher::withCount(['timetableSlots' => function($query) use ($timetable, $request) {
                                    $query
                                    ->whereHas('timetable', function($q) use ($request, $timetable) {
                                        $q->where('academic_year_id', $request->academic_year_id);
                                    });

                                }])->get();

            $teacher_count = [];
            foreach ($teachers as $teacher) {
                $teacher_count[$teacher->id] = $teacher->timetable_slots_count;
            }
            // dd($teacher_count);
            $setting = new Setting();
            $data['teacher_limit'] = null;
            $data['subject_limit'] = null;
            $limit_hour = @$setting->where('type', 'limit_hour')->first()->value;
            if($limit_hour) {
                $data['teacher_limit'] = json_decode($limit_hour, true)['teacher'];
                $data['subject_limit'] = json_decode($limit_hour, true)['subject'];
            }

            $teacher_today = TimetableSlot::whereHas('timetable', function ($query) use ($request) {
                                                $query->where('academic_year_id', $request->academic_year_id);
                                            })
                                            ->where([
                                                'week_day' => $request->week_day,
                                                'time_slot_id' => $request->time_slot,
                                                'teacher_id' => $request->teacher,
                                            ])
                                            ->get()
                                            ->count();
            if ($teacher_today >= 1) {
                return response()->json([
                    'errors' => [
                        [
                            'message' => __('This Teacher not available')
                        ]
                    ],
                ]);
            }

            if ($teacher_count[$request->teacher] >= $data['teacher_limit'][$teacher->type]) {

                if (request('ignore_teacher_limit') != 1) {
                    return response()->json([
                        'status' => 'teacher_limit_warning',
                    ]);
                }
            }

            $subject_limit_hour = $data['subject_limit'][$request->subject]['grade_'. $request->grade_id];
            $grade = Grade::find($request->grade_id);
            $class_room = ClassRoom::find($request->class_id);
            if ($class_room->is_outstanding_class == 1 && $class_room->outstanding_subject == $request->subject) {
                $subject_limit_hour += $class_room->extra_hours;
            }

            if ($grade->name == 12) {
                $class_room_type = $class_room->type;

                if ($subject_count[$request->subject] >= $subject_limit_hour) {
                    return response()->json([
                        'errors' => [
                            [
                                'message' => __('The selected subject has reached the limit hours')
                            ]
                        ],
                    ]);
                }

            } else {
                if ($subject_count[$request->subject] >= $subject_limit_hour) {
                    return response()->json([
                        'errors' => [
                            [
                                'message' => __('The selected subject has reached the limit hours')
                            ]
                        ],
                    ]);
                }
            }

            $timetable_slot = new TimetableSlot;
            $timetable_slot->timetable_id = $timetable->id;
            $timetable_slot->week_day = $request->week_day;
            $timetable_slot->time_slot_id = $request->time_slot;
            $timetable_slot->teacher_id = $request->teacher;
            $timetable_slot->subject_id = $request->subject;
            $timetable_slot->save();

            DB::commit();
            $data['day'] = $request->week_day;
            $data['time_id'] = $request->time_slot;
            $data['timetable_slot'] = $timetable_slot;
            $view = view('admin.timetable.partials._timetable_slot', $data)->render();
            // $total = $table['total'];

            return response()->json([
                'success' => true,
                'view' => $view,
                'id' => $request->week_day . '-' . $request->time_slot,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json([
                'errors' => [
                    [
                        'message' => __('Something went wrong')
                    ]
                ],
            ]);
        }

    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'teacher' => 'required',
            ], [
                'subject.required' => __('Subject is required'),
                'teacher.required' => __('Teacher is required'),
            ]);

            if ($validator->errors()->count() > 0) {
                return response()->json(['errors' => AppHelper::error_processor($validator)]);
            }

            DB::beginTransaction();

            $subjects = Subject::withCount(['timetableSlots' => function($query) use ($request, $id) {
                            $query->whereNot('id', $id)->whereHas('timetable', function($q) use ($request) {
                                $q->where('academic_year_id', $request->academic_year_id);
                            });
                        }])->get();

            $subject_count = [];
            foreach ($subjects as $subject) {
                $subject_count[$subject->id] = $subject->timetable_slots_count;
            }

            $teachers = Teacher::withCount(['timetableSlots' => function($query) use ($request, $id) {
                                    $query
                                    ->whereNot('id', $id)
                                    ->whereHas('timetable', function($q) use ($request) {
                                        $q->where('academic_year_id', $request->academic_year_id);
                                    });
                                    // ->where('timetable_id', $timetable->id);
                                }])->get();

            $teacher_count = [];
            foreach ($teachers as $teacher) {
                $teacher_count[$teacher->id] = $teacher->timetable_slots_count;
            }
            $setting = new Setting();
            $data['teacher_limit'] = null;
            $data['subject_limit'] = null;
            $limit_hour = @$setting->where('type', 'limit_hour')->first()->value;
            if($limit_hour) {
                $data['teacher_limit'] = json_decode($limit_hour, true)['teacher'];
                $data['subject_limit'] = json_decode($limit_hour, true)['subject'];
            }

            $teacher_today = TimetableSlot::whereHas('timetable', function ($query) use ($request) {
                                                $query->where('academic_year_id', $request->academic_year_id);
                                            })
                                            ->whereNot('id', $id)
                                            ->where([
                                                'week_day' => $request->week_day,
                                                'time_slot_id' => $request->time_slot,
                                                'teacher_id' => $request->teacher,
                                            ]);
            if ($teacher_today->count() >= 1) {
                if (request('is_confirmed') == 1) {
                    $timetable_slot = TimetableSlot::find($id);

                    $other_timetable_slot = TimetableSlot::find($teacher_today->first()->id);
                    $other_timetable_slot->teacher_id = $timetable_slot->teacher_id;
                    $other_timetable_slot->subject_id = $timetable_slot->subject_id;
                    $other_timetable_slot->save();

                    $timetable_slot->teacher_id = $request->teacher;
                    $timetable_slot->subject_id = $request->subject;
                    $timetable_slot->save();

                    DB::commit();

                    $data['day'] = $request->week_day;
                    $data['time_id'] = $request->time_slot;
                    $data['timetable_slot'] = $timetable_slot;
                    $view = view('admin.timetable.partials._timetable_slot', $data)->render();
                    return response()->json([
                        'success' => true,
                        'view' => $view,
                        'id' => $request->week_day . '-' . $request->time_slot
                    ]);
                }

                if (request('is_adjust') == 1) {
                    session()->put('switch_teacher_input', $request->all());
                    return response()->json([
                        'status' => 'adjustment_warning',
                        'href' => route('admin.timetable_slot.popupConfirm', $id),
                    ]);
                }

                return response()->json([
                    'errors' => [
                        [
                            'message' => __('This Teacher not available')
                        ]
                    ],
                ]);
            }
            if (!$request->has('ignore_teacher_limit')) {
                if ($teacher_count[$request->teacher] >= $data['teacher_limit'][$teacher->type]) {
                    return response()->json([
                        'type' => 'teacher_limit_hours',
                        'errors' => [
                            [
                                'message' => __('The selected Teacher has reached the limit hours')
                            ]
                        ],
                    ]);
                }
            }

            $grade = Grade::find($request->grade_id);
            if ($grade->name == 12) {
                $class_room = ClassRoom::find($request->class_id);
                $class_room_type = $class_room->type;

                if ($subject_count[$request->subject] >= $data['subject_limit'][$request->subject]['grade_'. $request->grade_id][$class_room_type]) {
                    return response()->json([
                        'errors' => [
                            [
                                'message' => __('The selected subject has reached the limit hours')
                            ]
                        ],
                    ]);
                }
            } else {
                if ($subject_count[$request->subject] >= $data['subject_limit'][$request->subject]['grade_'. $request->grade_id]) {
                    return response()->json([
                        'errors' => [
                            [
                                'message' => __('The selected subject has reached the limit hours')
                            ]
                        ],
                    ]);
                }
            }
            // dd(1);

            $timetable_slot = TimetableSlot::find($id);
            $timetable_slot->week_day = $request->week_day;
            $timetable_slot->time_slot_id = $request->time_slot;
            $timetable_slot->teacher_id = $request->teacher;
            $timetable_slot->subject_id = $request->subject;
            $timetable_slot->save();

            DB::commit();

            $data['day'] = $request->week_day;
            $data['time_id'] = $request->time_slot;
            $data['timetable_slot'] = $timetable_slot;
            $view = view('admin.timetable.partials._timetable_slot', $data)->render();

            return response()->json([
                'success' => true,
                'view' => $view,
                'id' => $request->week_day . '-' . $request->time_slot
            ]);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json([
                'errors' => [
                    [
                        'message' => __('Something went wrong')
                    ]
                ],
            ]);
        }
    }

    public function popupConfirm(Request $request, $id)
    {
        $switch_teacher_input = session()->get('switch_teacher_input') ?? null;
        $timetable_slot = TimetableSlot::find($id);
        return view('admin.timetable.partials._comfirm_switch_teacher_modal', compact('timetable_slot', 'switch_teacher_input'));
    }
}
