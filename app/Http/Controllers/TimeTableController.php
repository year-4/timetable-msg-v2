<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\SlotTime;
use App\Models\ClassRoom;
use App\Models\AcademicYear;
use App\Models\TimeTable;
use App\Models\TimetableSlot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TimeTableController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('teacher')) {
            $week_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
            $time_slots = SlotTime::get();
            $timetable = TimeTable::where('academic_year_id', request('academic_year_id'))
                                    ->where('grade_id', request('grade_id'))
                                    ->where('class_id', request('class_id'))
                                    ->first();
            $teacher = Teacher::with('timetableSlots')->find(auth()->user()->teacher->id);
            $timetable_slots = $teacher->timetableSlots;
            return view('admin.teacher.timetable', compact('week_days', 'time_slots', 'timetable', 'teacher', 'timetable_slots'));
        }
        $academicyears = AcademicYear::orderBy('name', 'ASC')->paginate(10);
        return view('admin.timetable.index', compact('academicyears'));
    }

    public function getGradeList(Request $request)
    {
        $grade_id = $request->grade_id;

        $grades = Grade::all();
        $data['grades'] = $grades;
        $data['year_id'] = $request->year_id;
        // return ClassRoom::all();
        $class_rooms = $data['class_rooms'] = ClassRoom::has('grade')
                                ->orderBy('name')
                                ->with('timetables')
                                ->when($grade_id,
                                    function ($query) use ($grade_id) {
                                        $query->where('grade_id', $grade_id);
                                    }, function ($query) use ($grades) {
                                        $query->where('grade_id', $grades[0]->id);
                                    }
                                )
                                ->get();

        if (request()->ajax()) {
            $view = view('admin.timetable.partials._class_rooms', compact('class_rooms', 'grade_id'))->render();
            return response()->json([
                'view' => $view,
            ]);
        }

        $data['grade_id'] = $grades[0]->id;

        return view('admin.timetable.grades_list', $data);
    }

    public function create()
    {

        $subject_id = request('subject_id');

        $week_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $time_slots = SlotTime::get();
        $timetable = TimeTable::where('academic_year_id', request('academic_year_id'))
                                ->where('grade_id', request('grade_id'))
                                ->where('class_id', request('class_id'))
                                ->first();

        $timetable_slots = null;
        if ($timetable){
            $timetable_slots = $timetable->slots;
        }

        if (request()->ajax()) {
            if (request()->filled('subject_id')) {
                $teachers = Teacher::whereHas('primarySubject', function ($query) use ($subject_id) {
                                $query->where('id', $subject_id);
                            })
                            ->get()
                            ->pluck('full_name', 'id');
                $view = view('admin.timetable.partials._teacher_select', compact('teachers'))->render();
                return response()->json([
                    'view' => $view
                ]);
            }
        }

        return view('admin.timetable.create', compact('timetable', 'timetable_slots', 'week_days', 'time_slots'));
    }

    public function showPopupCreate()
    {
        $data['academic_year_id'] = request('academic_year_id');
        $data['grade_id'] = request('grade_id');
        $data['class_id'] = request('class_id');
        $data['week_day'] = request('week_day');
        $data['time_slot'] = request('time_slot');
        $data['subjects'] = Subject::pluck('name', 'id');
        $data['teachers'] = Teacher::get()->pluck('full_name', 'id');
        return view('admin.timetable.partials._create', $data);
    }

    public function showPopupEdit($id)
    {
        $data['timetable_slot'] = TimetableSlot::find($id);

        $data['academic_year_id'] = request('academic_year_id');
        $data['grade_id'] = request('grade_id');
        $data['class_id'] = request('class_id');
        $data['week_day'] = request('week_day');
        $data['time_slot'] = request('time_slot');
        $data['is_adjust'] = request('is_adjust') ?? 0;
        $data['subjects'] = Subject::pluck('name', 'id');
        $data['teachers'] = Teacher::whereHas('primarySubject', function ($query) use ($data) {
                                $query->where('id', $data['timetable_slot']->subject_id);
                            })
                            ->get()
                            ->pluck('full_name', 'id');
        return view('admin.timetable.partials._edit', $data);
    }

    public function academicYearCreate()
    {
        return view('admin.timetable.partials._create_academic');
    }

    public function adjustment(Request $request, $id)
    {
        $subject_id = request('subject_id');

        $week_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $time_slots = SlotTime::get();
        $timetable = TimeTable::find($id);

        $timetable_slots = null;
        if ($timetable){
            $timetable_slots = $timetable->slots;
        }

        if (request()->ajax()) {
            if (request()->filled('subject_id')) {
                $teachers = Teacher::whereHas('primarySubject', function ($query) use ($subject_id) {
                                $query->where('id', $subject_id);
                            })
                            ->get()
                            ->pluck('full_name', 'id');
                $view = view('admin.timetable.partials._teacher_select', compact('teachers'))->render();
                return response()->json([
                    'view' => $view
                ]);
            }
        }

        return view('admin.timetable.adjust', compact('timetable', 'timetable_slots', 'week_days', 'time_slots'));
    }

    public function weeklyAdjustment(Request $request, $id)
    {
        $subject_id = request('subject_id');

        $week_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $time_slots = SlotTime::get();
        $timetable = TimeTable::find($id);

        $timetable_slots = null;
        if ($timetable){
            $timetable_slots = $timetable->slots;
        }

        if (request()->ajax()) {
            if (request()->filled('subject_id')) {
                $teachers = Teacher::whereHas('primarySubject', function ($query) use ($subject_id) {
                                $query->where('id', $subject_id);
                            })
                            ->get()
                            ->pluck('full_name', 'id');
                $view = view('admin.timetable.partials._teacher_select', compact('teachers'))->render();
                return response()->json([
                    'view' => $view
                ]);
            }
        }

        return view('admin.timetable.weekly_adjust', compact('timetable', 'timetable_slots', 'week_days', 'time_slots'));
    }

    /**
     * Display the specified resource.
     */
    public function show($id, Request $request)
    {
        $data['grade'] = Grade::find($request->grade_id);
        $data['class'] = ClassRoom::find($request->class_id);
        $data['year']  = AcademicYear::find($request->academic_year_id);
        $data['week_days'] = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
        $data['time_slots'] = SlotTime::get();
        if ($id != 0) {
            $data['timetable'] = $timetable = TimeTable::find($id);

            $data['timetable_slots'] = $timetable->slots;
        }
        // return $data['timetable'];
        return view('admin.timetable.detail', $data);
    }

    public function generateTimetable(Request $request)
    {
        $grades = Grade::all();
        $classrooms = ClassRoom::all();
        $subjects = Subject::all();
        $teachers = Teacher::all();
        $week_days = ['Monday', 'Tudeday', 'Wednesday', 'Thirseday', 'Friday', 'Satureday'];
        $time_slots = [
            [
                'start' => '7:00',
                'end' => '8:00',
            ],
            [
                'start' => '8:00',
                'end' => '9:00',
            ],
            [
                'start' => '9:00',
                'end' => '10:00',
            ],
            [
                'start' => '10:00',
                'end' => '11:00',
            ],
            [
                'start' => '11:00',
                'end' => '12:00',
            ],
        ];

        $timetabl_data = [];
        foreach($grades as $grade) {
            $classrooms_by_grade = $classrooms->where('grade_id', $grade->id)->all();

            foreach($classrooms_by_grade as $room) {
                $assinged_teacher = [];
                $timetabl_data[$room->name] = [];
                $subject_index = 0;
                $subjects = $subjects->shuffle();
                $subject_limit_times = [
                    [
                        'subject_id' => 10,
                        'limit_time' => 6,
                    ],
                    [
                        'subject_id' => 11,
                        'limit_time' => 3,
                    ],
                    [
                        'subject_id' => 12,
                        'limit_time' => 3,
                    ],
                    [
                        'subject_id' => 13,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 14,
                        'limit_time' => 4,
                    ],
                    [
                        'subject_id' => 15,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 16,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 17,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 18,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 19,
                        'limit_time' => 2,
                    ],
                    [
                        'subject_id' => 20,
                        'limit_time' => 2,
                    ],
                ];
                foreach($week_days as $day_index => $day) {
                    $timetabl_data[$room->name][$day] = [];
                    foreach($time_slots as $key => $time) {
                        $start_time = $time['start'];
                        $end_time = $time['end'];

                        $timetabl_data[$room->name][$day][$start_time . ' - ' . $end_time] = [];

                        // reset subject_index
                        if ($subject_index == $subjects->count()) {
                            $subject_index = 0;
                        }
                        $random_subject = $subjects[$subject_index];
                        $random_teacher = $teachers->where('primary_subject_id', $random_subject->id)->random();
                        if (!empty($assinged_teacher) && $assinged_teacher['subject'] == $random_subject->name) {
                            $random_teacher = $teachers->where('id', $assinged_teacher['teacher_id'])->first();
                        }
                        $teacher_name = $random_teacher->first_name . ' ' . $random_teacher->last_name;

                        $assinged_teacher = [
                            'subject' => $random_subject->name,
                            'teacher_name' => $teacher_name,
                            'teacher_id' => $random_teacher->id,
                        ];

                        $timetabl_data[$room->name][$day][$start_time . ' - ' . $end_time]['subject'] = $random_subject->name;
                        $timetabl_data[$room->name][$day][$start_time . ' - ' . $end_time]['teacher'] = $teacher_name;

                        $subject_limit_time = collect($subject_limit_times)
                                            ->where('subject_id', $random_subject->id)
                                            ->first();
                        $decreased_time = $subject_limit_time['limit_time'] > 0 ? $subject_limit_time['limit_time'] - 1 : 0;
                        $subject_limit_times = collect($subject_limit_times)
                            ->map(function ($item) use ($random_subject, $decreased_time) {
                                if ($item['subject_id'] == $random_subject['id']) {
                                    $item['limit_time'] = $decreased_time;
                                }
                                return $item;
                            });

                        if ($subject_limit_time['limit_time'] == 1) {
                            $subject_index++;

                        }
                    }
                }
            }
        }

        dd($timetabl_data);
        return $timetabl_data;
    }
}
