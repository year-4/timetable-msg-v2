<?php

namespace App\Http\Controllers;
use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use  Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class AuthController extends Controller
{
    public function login()
    {
        
        
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type','app_name')->first()->value;
        $data['copyright'] = @$setting->where('type','copyright')->first()->value;
        $data['address'] = @$setting->where('type','address')->first()->value;
        $data['phone_number'] = @$setting->where('type','phone_number')->first()->value;
        $data['email'] = @$setting->where('type','email')->first()->value;
        $data['facebook'] = @$setting->where('type','facebook')->first()->value;
        $data['telegram'] = @$setting->where('type','telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type','phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type','about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type','banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type','banner_image_names')->first()->value;
        return view('auth.login',$data);
    }
    public function AuthLogin(Request $request)
    {
        $remember = !empty($request->remember) ? true : false;

        $credentials = [
            'password' => $request->password
        ];
        if (filter_var($request->name, FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $request->name;
        } else {
            $credentials['name'] = $request->name;
        }

        if (Auth::attempt($credentials, $remember)) {
            
            return redirect()->route('admin.dashboard');
        } else {
            
            return redirect()->back()->with('error', 'Login failed');
        }
    }

    public function reset($remember_token)
    {
        $user = User::getTokenSingle($remember_token);
        if (!empty($user)) {
            $data['user'] = $user;
            return view('auth.reset', $data);
        } else {
            abort(404);
        }
    }
    public function PostReset($token, Request $request)
    {

        if ($request->password == $request->cpassword) {

            $user = User::getTokenSingle($token);
            $user->password = Hash::make($request->password);

            $user->remember_token = Str::random(30);
            $user->save();
            return redirect(url(''))->with('success', 'Password Successfully Reset. ');
        } else {
            return redirect()->back()->with('error', 'Please and Confirm password does not match. ');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function forgotpassword()
    {

        return view('auth.forgot');
    }
    public function PostForgotPassword(Request $request)
    {
        $user = User::getEmailSingle($request->email);
        if (!empty($user)) {

            $user->remember_token = Str::random(30);
            $user->save();
            Mail::to($user->email)->send(new ForgotPasswordMail($user));
            return redirect()->back()->with('success', "Please check your email and reset you password.");
        } else {
            return redirect()->back()->with('error', "Email not found in the system.");
        }
    }
    public function register()
    {
        $setting = new Setting();
        $data['about_us'] = @$setting->where('type', 'about_us')->first()->value;
        $data['app_name'] = @$setting->where('type','app_name')->first()->value;
        $data['copyright'] = @$setting->where('type','copyright')->first()->value;
        $data['address'] = @$setting->where('type','address')->first()->value;
        $data['phone_number'] = @$setting->where('type','phone_number')->first()->value;
        $data['email'] = @$setting->where('type','email')->first()->value;
        $data['facebook'] = @$setting->where('type','facebook')->first()->value;
        $data['telegram'] = @$setting->where('type','telegram')->first()->value;
        $data['phone_number1'] = @$setting->where('type','phone_number1')->first()->value;
        $data['about_image_names'] = @$setting->where('type','about_image_names')->first()->value;
        $data['banner_text'] = @$setting->where('type','banner_text')->first()->value;
        $data['banner_image_names'] = @$setting->where('type','banner_image_names')->first()->value;
        
        return view('auth.register',$data);
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        $defaultRole = Role::find(6);
        $user->roles()->attach($defaultRole);
        Auth::login($user);
        return redirect()->route('login')->with('success', 'Registration successful. You can now log in.');
    }
}
