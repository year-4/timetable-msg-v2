<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::orderBy('type', 'ASC')->paginate();
        return view('admin.gallery.index', compact('galleries'));
    }

    public function create()
    {
        return view('admin.gallery.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image',
            'type' => 'required',
        ]);
        try {
            DB::beginTransaction();

            $galleries = new Gallery;
            $galleries->type = trim($request->type);

            if ($request->filled('image_names')) {
                $galleries->image = $request->image_names;
                $directory = public_path('upload/gallery');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/gallery/'. $request->image_names));

            }


            $galleries->save();

            DB::commit();

            return redirect()->route('admin.gallery.index')->with('success', 'Gallery Successfully Upload');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function show(string $id)
    {
        $gallery = Gallery::findOrFail($id);

    return view('admin.gallery.partials._detail', compact('gallery'));

    }

    public function edit(string $id)
    {
        $galleries = Gallery::findOrFail($id);

        return view('admin.gallery.edit', compact('galleries'));
    }

    public function update(Request $request, string $id)
    {
        try {
            $galleries = Gallery::findOrFail($id);

            $validator = Validator::make($request->all(), [

                'image' => 'image',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }
            $galleries->type = $request->type;


            if ($request->filled('image_names')) {
                $galleries->image = $request->image_names;
                $directory = public_path('upload/gallery');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/gallery/'. $request->image_names));

            }

            $galleries->save();

            $table = $this->renderTable();
            $view = $table['view'];
            $total = $table['total'];

            return redirect()->route('admin.gallery.index')->with('success', 'Gallery Successfully Updated');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function destroy(Gallery $gallery)
    {
        $gallery->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];
        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function renderTable()
    {
        $galleries = Gallery::orderBy('type', 'ASC')->paginate(10);
        $view = view('admin.gallery.partials._table', compact('galleries'))->render();

        return ['view' => $view, 'total' => $galleries->total()];
    }
}
