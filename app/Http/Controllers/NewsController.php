<?php

namespace App\Http\Controllers;

use App\Models\News;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $news = News::orderBy('title', 'ASC')->paginate();
        return view('admin.school_news.index', compact('news'));
    }

    public function create()
    {
        if (!auth()->user()->can('user.create')) {
            abort(403, 'Unauthorized action.');
        }
        // $roles = Role::pluck('name', 'id');
        return view('admin.school_news.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image',
            'create_by' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $news = new News;
            $news->title = trim($request->title);
            $news->description = trim($request->description);
            $news->create_by = auth()->id();
            if ($request->filled('image_names')) {
                $news->image = $request->image_names;
                $directory = public_path('upload/news');
                if (!\File::exists($directory)) {
                    \File::makeDirectory($directory, 0777, true);
                }

                $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/news/'. $request->image_names));

            }


            $news->save();

            DB::commit();

            return redirect()->route('admin.school_news.index')->with('success', 'New Entity Successfully Created');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function show($id)
    {
        $news = News::findOrFail($id);
        return view('admin.school_news.partials._detail', compact('news'));
    }

    public function edit(String $id)
    {
        if (!auth()->user()->can('time_slot.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $news = News::findOrFail($id);

        return view('admin.school_news.edit', compact('news'));
    }
    public function update(Request $request, string $id)
    {
        $news = News::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->messages()->first())->withInput();
        }

        $news->title = $request->title;
        $news->description = $request->description;

        if ($request->filled('image_names')) {
            $news->image = $request->image_names;
            $directory = public_path('upload/news');
            if (!\File::exists($directory)) {
                \File::makeDirectory($directory, 0777, true);
            }

            $image = \File::move(public_path('upload/temp/' . $request->image_names), public_path('upload/news/'. $request->image_names));

        }

        $news->save();

        // $table = $this->renderTable();
        // $view = $table['view'];
        // $total = $table['total'];

        // return response()->json([
        //     'success' => true,
        //     'view' => $view,
        //     'data' => $news,
        //     'total' => $total,
        // ]);
        return redirect()->route('admin.school_news.index')->with('success', 'New Entity Successfully Updaated');
    }


    public function destroy($id)
    {
        if (!auth()->user()->can('teacher.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $news = News::findOrFail($id);

        $news->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function renderTable()
    {
        $news = News::orderBy('title', 'ASC')->paginate(10);
        $view = view('admin.school_news.partials._table', compact('news'))->render();

        return ['view' => $view, 'total' => $news->total()];
    }
}
