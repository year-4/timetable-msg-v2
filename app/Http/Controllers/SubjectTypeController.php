<?php

namespace App\Http\Controllers;
use App\Helper\AppHelper;
use App\Models\SubjectType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function Termwind\render;

class SubjectTypeController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('subject_type.view')) {
            abort(403, 'Unauthorized action.');
        }
        $subjectTypes = SubjectType::orderBy('name', 'ASC')->paginate();
        return view('admin.subject_type.index', compact('subjectTypes'));
    }

    public function create()
    {
        if (!auth()->user()->can('subject_type.create')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.subject_type.partials._create');
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('subject_type.create')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:subject_types,name',
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added')
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        SubjectType::create(['name' => $request->name]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }

    public function show(SubjectType $subjectType)
    {
        //
    }

    public function edit(SubjectType $subjectType)
    {
        if (!auth()->user()->can('subject_type.edit')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.subject_type.partials._edit', compact('subjectType'));
    }

    public function update(Request $request, SubjectType $subjectType)
    {
        if (!auth()->user()->can('subject_type.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:subject_types,name,' . $subjectType->id,
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added')
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        $subjectType->name = $request->name;
        $subjectType->save();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }

    public function destroy(SubjectType $subjectType)
    {
        if (!auth()->user()->can('subject_type.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $subjectType->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }
    public function renderTable () {
        $subjectTypes = SubjectType::orderBy('name', 'ASC')->paginate();
        $view = view('admin.subject_type.partials._table', compact('subjectTypes'))->render();

        return ['view' => $view, 'total' => $subjectTypes->total()];
    }
}
