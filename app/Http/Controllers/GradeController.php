<?php

namespace App\Http\Controllers;

use App\Helper\AppHelper;
use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function Termwind\render;

class GradeController extends Controller
{
    
    public function index()
    {
        if (!auth()->user()->can('grade.view')) {
            abort(403, 'Unauthorized action.');
        }
        $grades = Grade::orderBy('name', 'ASC')->paginate();

        return view('admin.grade.index', compact('grades'));
    }
    public function create()
    {
        if (!auth()->user()->can('grade.create')) {
            abort(403, 'Unauthorized action.');
        }
        return view('admin.grade.partials._create');
    }
    public function store(Request $request)
    {
        if (!auth()->user()->can('grade.create')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:grades,name',
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added')
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }


        Grade::create(['name' => $request->name]);

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'success' => true,
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);
    }
    public function show(Grade $grade)
    {
        //
    }

    public function edit(Grade $grade)
    {
        if (!auth()->user()->can('grade.edit')) {
            abort(403, 'Unauthorized action.');
        }

        return view('admin.grade.partials._edit', compact('grade'));
    }
    public function update(Request $request, Grade $grade)
    {
        if (!auth()->user()->can('grade.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:grades,name,' . $grade->id,
        ], [
            'name.required' => __('Class Name is required'),
            'name.unique'   => __('This Class is already added')
        ]);

        if ($validator->errors()->count() > 0) {
            return response()->json(['errors' => AppHelper::error_processor($validator)]);
        }

        $grade->name = $request->name;
        $grade->save();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];

        return response()->json([
            'view' => $view,
            'data' => $request->all(),
            'total' => $total,
        ]);

    }
    public function destroy(Grade $grade)
    {
        if (!auth()->user()->can('grade.delete')) {
            abort(403, 'Unauthorized action.');
        }
        $grade->delete();

        $table = $this->renderTable();
        $view = $table['view'];
        $total = $table['total'];
        // $grades

        return response()->json([
            'view' => $view,
            'total' => $total,
        ]);
    }

    public function renderTable () {
        $grades = Grade::orderBy('name', 'ASC')->paginate();
        $view = view('admin.grade.partials._table', compact('grades'))->render();

        return ['view' => $view, 'total' => $grades->total()];
    }
}
