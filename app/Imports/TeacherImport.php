<?php

namespace App\Imports;

use App\Models\User;
use App\Models\Teacher;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TeacherImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     */
    /**
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
{
    // Find an existing User instance based on email
    $user = User::where('email', $row['email'])->first();

    // If the user doesn't exist, create a new User instance
    if (!$user) {
        $user = new User([
            'name' => $row['name'],
            'password' => bcrypt($row['password']),
            'email' => $row['email'],
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            // Add other user-related fields here
        ]);

        $user->save();
    }

    // Create a Teacher instance and associate it with the User
    $teacher = new Teacher([
        'username' => $row['username'],
        'first_name' => $row['first_name'],
        'last_name' => $row['last_name'],
        'gender' => $row['gender'],
        'email' => $row['email'],
        'phone' => $row['phone'],
        'code' => $row['code'],
        'status' => $row['status'],
    ]);

    // Associate the Teacher with the User
    $user->teacher()->save($teacher);

    // Return the User instance (or Teacher if you prefer)
    return $user;
}

}