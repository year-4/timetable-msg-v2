-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2024 at 07:22 AM
-- Server version: 10.6.17-MariaDB-cll-lve
-- PHP Version: 8.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timeofnd_timetabledb`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_years`
--

CREATE TABLE `academic_years` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academic_years`
--

INSERT INTO `academic_years` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2023-2024', '2024-01-19 00:28:12', '2024-01-19 00:28:12', NULL),
(6, '2024-2025', '2024-03-22 18:43:52', '2024-03-22 18:43:52', NULL),
(7, '2025-2026', '2024-03-23 07:34:08', '2024-03-23 13:55:16', '2024-03-23 13:55:16'),
(8, '2022-2023', '2024-03-23 11:51:57', '2024-03-23 11:51:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class_rooms`
--

CREATE TABLE `class_rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `type` enum('real_science','social_science') DEFAULT NULL,
  `is_outstanding_class` int(11) NOT NULL DEFAULT 0,
  `extra_hours` int(11) DEFAULT NULL,
  `outstanding_subject` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_rooms`
--

INSERT INTO `class_rooms` (`id`, `grade_id`, `teacher_id`, `name`, `created_at`, `updated_at`, `order`, `type`, `is_outstanding_class`, `extra_hours`, `outstanding_subject`) VALUES
(13, 6, 128, '9A', '2024-03-22 18:34:42', '2024-03-22 18:34:42', 0, NULL, 0, NULL, NULL),
(14, 6, 129, '9B', '2024-03-22 18:34:56', '2024-03-22 18:34:56', 0, NULL, 0, NULL, NULL),
(15, 10, 130, '10A', '2024-03-22 18:35:13', '2024-03-23 14:02:21', 0, NULL, 1, 2, 25),
(16, 10, 155, '10B', '2024-03-22 18:35:30', '2024-03-23 18:16:27', 0, NULL, 0, NULL, NULL),
(17, 2, 132, '11A', '2024-03-22 18:35:43', '2024-03-22 18:35:43', 0, NULL, 0, NULL, NULL),
(18, 2, 133, '11B', '2024-03-22 18:35:58', '2024-03-22 18:35:58', 0, NULL, 0, NULL, NULL),
(19, 3, 134, '12A', '2024-03-22 18:36:28', '2024-03-22 18:36:28', 0, NULL, 0, NULL, NULL),
(20, 3, 135, '12B', '2024-03-22 18:36:50', '2024-03-22 18:36:50', 0, NULL, 0, NULL, NULL),
(21, 10, 137, '10C', '2024-03-22 23:56:47', '2024-03-22 23:57:33', 0, NULL, 1, 2, 21),
(22, 11, 140, '8A', '2024-03-23 11:48:29', '2024-03-23 11:48:56', 0, NULL, 1, 2, 21),
(23, 12, 156, '7A', '2024-03-23 18:10:25', '2024-03-23 18:10:25', 0, NULL, 0, NULL, NULL),
(24, 12, 161, '7B', '2024-03-23 19:03:43', '2024-03-23 19:04:32', 0, NULL, 1, 2, 26),
(25, 3, 8, '12K', '2024-03-23 19:15:38', '2024-03-23 19:15:38', 0, 'real_science', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `type`, `created_at`, `updated_at`) VALUES
(11, '2023-12-09-65735063e01e2.png', 'Student', '2023-11-21 11:56:16', '2023-12-09 05:20:50'),
(12, '2024-03-22-65fd813fe5eee.png', 'Sport', '2023-11-21 12:03:37', '2024-03-23 00:02:03'),
(13, '2024-03-23-65fe8e3735966.png', 'Sport', '2023-11-21 12:03:48', '2024-03-23 19:09:34'),
(14, '2023-12-09-65735147e064b.png', 'Student', '2023-12-09 05:24:28', '2023-12-09 05:24:28'),
(15, '2023-12-09-65735154a8716.png', 'Teacher', '2023-12-09 05:24:39', '2023-12-09 05:24:39'),
(16, '2023-12-09-657351604a86e.png', 'Teacher', '2023-12-09 05:24:53', '2023-12-09 05:24:53'),
(17, '2024-03-19-65f98ce2d16bc.png', 'Sport', '2024-03-20 00:02:46', '2024-03-20 00:02:46'),
(18, '2024-03-23-65fe283540003.png', 'Student', '2024-03-23 11:55:05', '2024-03-23 11:55:05');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 11, '2023-09-16 20:23:45', '2023-09-16 20:23:45'),
(3, 12, '2023-09-16 20:23:48', '2023-09-16 20:23:48'),
(6, 9, '2024-01-19 00:32:19', '2024-01-19 00:32:19'),
(10, 10, '2024-03-22 18:26:08', '2024-03-22 18:26:08'),
(11, 8, '2024-03-23 11:46:35', '2024-03-23 11:46:35'),
(12, 7, '2024-03-23 18:06:49', '2024-03-23 18:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_07_02_170240_create_grades_table', 1),
(6, '2023_07_04_163606_create_class_rooms_table', 1),
(7, '2023_07_05_171142_create_teacher_table', 1),
(8, '2023_07_05_172616_create_subjects_table', 1),
(9, '2023_07_09_112551_chage_datatype_in_grades_table', 1),
(10, '2023_07_16_021514_create_subject_types_table', 1),
(11, '2023_07_16_022256_add_order_to_class_rooms_table', 1),
(12, '2023_08_05_170357_change_column_grade_id_in_subjects_table', 1),
(13, '2023_08_13_034046_create_time_tables_table', 1),
(14, '2023_08_13_041148_create_academic_years_table', 1),
(15, '2023_09_02_025636_create_slot_times_table', 1),
(16, '2023_09_10_023949_add_subject_id_column_to_teachers_table', 1),
(19, '2023_09_17_032913_create_timetable_slots_table', 2),
(20, '2023_10_08_034724_create_permission_tables', 3),
(21, '2023_10_11_150328_add_userimage_to_users', 4),
(22, '2023_10_16_124349_add_first_name_last_name_to_users', 5),
(23, '2023_10_17_112636_add_gender_to_users', 5),
(24, '2023_10_22_040519_create_settings_table', 6),
(26, '2023_10_23_120916_add_teacher_type_to_teachers_table', 7),
(27, '2023_10_28_141800_create_news_table', 8),
(28, '2023_10_29_090601_create_gallery_table', 8),
(29, '2023_11_06_184822_create_translatoins_table', 9),
(30, '2023_11_21_184822_add_class_room_type_to_class_rooms_table', 10),
(31, '2023_12_25_185125_add_is_outstanding_class_column_to_class_rooms_table', 11),
(32, '2024_01_10_190425_add_field_khmer_name_to_teachers_table', 12),
(33, '2024_03_01_183618_add_doe_to_teachers_table', 13),
(34, '2024_03_04_193402_add_softdelete_to_teachers_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 3),
(1, 'App\\Models\\User', 4),
(1, 'App\\Models\\User', 26),
(1, 'App\\Models\\User', 146),
(2, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 15),
(2, 'App\\Models\\User', 29),
(2, 'App\\Models\\User', 31),
(2, 'App\\Models\\User', 39),
(2, 'App\\Models\\User', 130),
(2, 'App\\Models\\User', 131),
(2, 'App\\Models\\User', 132),
(2, 'App\\Models\\User', 133),
(2, 'App\\Models\\User', 134),
(2, 'App\\Models\\User', 135),
(2, 'App\\Models\\User', 136),
(2, 'App\\Models\\User', 137),
(2, 'App\\Models\\User', 138),
(2, 'App\\Models\\User', 139),
(2, 'App\\Models\\User', 143),
(2, 'App\\Models\\User', 144),
(2, 'App\\Models\\User', 145),
(2, 'App\\Models\\User', 147),
(2, 'App\\Models\\User', 164),
(11, 'App\\Models\\User', 171),
(11, 'App\\Models\\User', 172);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `image`, `create_by`, `created_at`, `updated_at`) VALUES
(16, 'បវេសនកាលឆ្នាំសិក្សាថ្មី២០២៣-២០២៤', '<p>បវេសនកាលឆ្នាំសិក្សាថ្មី២០២៣-២០២៤<br></p>', '2023-12-09-65734da735636.png', '3', '2023-12-09 05:09:02', '2023-12-09 05:09:02'),
(17, 'សេចក្តីជូនដំណឹង', '<p>សេចក្តីជូនដំណឹង</p><p>គណៈគ្រប់គ្រងវិទ្យាល័យ 10 មករា 1979 សូមជម្រាបជូនដំណឹងដល់សិស្សានុសិស្សវិទ្យាល័យ 10 មករា 1979 បានដាក់ពាក្យចូលរៀន(សិស្សចាស់និងសិស្សថ្មី)ទាំងអស់ថា:នៅព្រឹកថ្ងៃទី01 ខែធ្នូ ឆ្នាំ 2023 សាលាបានប្រារព្ធពិធីបើកបវេសនកាលឆ្នាំសិក្សាថ្មី 2023_ 2024។</p><p>សូមសិស្សានុសិស្សទាំងអស់ស្លៀកពាក់ឯកសណ្ឋានខៀវសមកចូលរួមកម្មវិធីបើកបវេសនកាលនៅក្នុងវិទ្យាល័យ 10 មករា1979 នៅវេលាម៉ោង ៧:០០នាទី ព្រឹកជាកំណត់។</p><p>បញ្ជាក់:ក្រោយពីកម្មវិធីត្រូវបានបញ្ចប់លោកគ្រូនិងបិទឈ្មោះសិស្សទៅតាមថ្នាក់រៀនរៀងៗខ្លួន និងតំណាងថ្នាក់មកបើកសម្ភារៈសម្រាប់សម្អាតថ្នាក់រៀន។<span style=\"white-space:pre\">	</span></p>', '2023-12-09-65734e3ebe6de.png', '3', '2023-12-09 05:11:43', '2023-12-09 05:11:43'),
(18, 'សូមអបអរសាទរ និង កោតសរសើរសិស្សានុសិស្សថ្នាក់ទី១២ ចំនួនរហូតដល់ ៤៦នាក់ ក្នុងឆ្នាំសិក្សា២០២៣-២០២៤ នៃវិទ្យាល័យ១០មករា១៩៧៩ ខេត្តសៀមរាប ដែលទទួលបានលទ្ធផល និទ្ទេស A នៃលទ្ធផលប្រឡងសញ្ញាបត្រមធ្យមសិក្សាទុតិយភូមិ សម័យប្រឡង ០៦ វិច្ឆិកា ២០២៣។', '<p>សូមអបអរសាទរ និង កោតសរសើរសិស្សានុសិស្សថ្នាក់ទី១២ ចំនួនរហូតដល់ ៤៦នាក់ ក្នុងឆ្នាំសិក្សា២០២២-២០២៣ នៃវិទ្យាល័យ១០មករា១៩៧៩ ខេត្តសៀមរាប ដែលទទួលបានលទ្ធផល និទ្ទេស A នៃលទ្ធផលប្រឡងសញ្ញាបត្រមធ្យមសិក្សាទុតិយភូមិ សម័យប្រឡង ០៦ វិច្ឆិកា ២០២៤។<br></p>', '2023-12-09-657350c0e9636.png', '3', '2023-12-09 05:22:30', '2024-03-23 00:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'grade.view', 'web', NULL, NULL),
(2, 'grade.create', 'web', NULL, NULL),
(3, 'grade.edit', 'web', NULL, NULL),
(4, 'grade.delete', 'web', NULL, NULL),
(5, 'blog.view', 'web', '2023-10-17 05:14:29', '2023-10-17 05:14:29'),
(6, 'blog.create', 'web', '2023-10-17 05:17:52', '2023-10-17 05:17:52'),
(7, 'timetable.view', 'web', '2023-12-10 23:54:24', '2023-12-10 23:54:24'),
(8, 'class_room.view', 'web', '2024-03-19 23:12:08', '2024-03-19 23:12:08'),
(9, 'subject.view', 'web', '2024-03-19 23:12:08', '2024-03-19 23:12:08'),
(10, 'teacher.view', 'web', '2024-03-19 23:12:08', '2024-03-19 23:12:08'),
(11, 'academic_year.view', 'web', '2024-03-19 23:12:08', '2024-03-19 23:12:08'),
(12, 'subject_type.view', 'web', '2024-03-22 23:19:09', '2024-03-22 23:19:09'),
(13, 'subject.create', 'web', '2024-03-22 23:47:14', '2024-03-22 23:47:14'),
(14, 'user.view', 'web', '2024-03-23 14:46:32', '2024-03-23 14:46:32'),
(15, 'user.create', 'web', '2024-03-23 14:46:32', '2024-03-23 14:46:32'),
(16, 'user.edit', 'web', '2024-03-23 14:46:32', '2024-03-23 14:46:32'),
(17, 'user.delete', 'web', '2024-03-23 14:46:32', '2024-03-23 14:46:32'),
(18, 'timetable.create', 'web', '2024-03-23 19:32:59', '2024-03-23 19:32:59'),
(19, 'timetable.edit', 'web', '2024-03-23 19:32:59', '2024-03-23 19:32:59'),
(20, 'timetable.delete', 'web', '2024-03-23 19:32:59', '2024-03-23 19:32:59');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', NULL, NULL),
(2, 'teacher', 'web', NULL, NULL),
(11, 'Assistant', 'web', '2024-03-23 18:54:52', '2024-03-23 18:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 11),
(2, 1),
(3, 1),
(3, 2),
(3, 11),
(4, 1),
(7, 11),
(9, 11),
(18, 11),
(19, 11),
(20, 11);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(3, 'language', '[{\"id\":\"1\",\"name\":\"english\",\"direction\":\"ltr\",\"code\":\"en\",\"status\":1,\"default\":true},{\"id\":\"2\",\"name\":\"\\u1781\\u17d2\\u1798\\u17c2\\u179a\",\"direction\":\"ltr\",\"code\":\"kh\",\"status\":1,\"default\":false}]', NULL, '2023-12-05 23:50:48'),
(4, 'pnc_language', '[\"en\"]', NULL, NULL),
(5, 'about_us', 'This is about us', '2023-11-13 11:52:03', '2024-03-23 00:07:20'),
(6, 'files', NULL, '2023-11-13 11:52:03', '2023-11-13 11:52:03'),
(7, 'vision', '<h1 class=\"\" style=\"font-family: Inter, &quot;Kantumruy Pro&quot;, sans-serif;\">ចក្ខុវិស័យ<br></h1><h1 class=\"\"></h1><h6 class=\"\"></h6><h3 class=\"\"></h3><h1 class=\"\"><blockquote class=\"blockquote\"><ul style=\"\"><li style=\"\">អប់រំចំណេះសមូហភាព</li></ul><ul style=\"\"><li>អប់រំចំណេះអប់រំឥរិយាបថ</li></ul><ul style=\"\"><li>អប់រំចំណេះធ្វើ</li></ul><h6 class=\"\"><ul style=\"\"><li style=\"\">អប់រំចំណេះដឹង</li></ul></h6></blockquote></h1>', '2023-11-13 12:10:14', '2024-03-23 15:05:00'),
(8, 'app_name', '10 Makra 1979 High School', '2023-11-13 12:32:00', '2023-11-13 12:32:00'),
(9, 'url', 'https://www.youtube.com/watch?v=ajH4n-IB_Wc', '2023-11-13 13:11:29', '2023-12-09 05:18:11'),
(10, 'desc', 'បទបង្ហាញ របស់សិស្ស ថ្នាក់ទី 12C ភាសាអង់គ្លេស', '2023-11-14 12:51:41', '2024-03-23 00:07:20'),
(11, 'image_names', NULL, '2023-11-14 12:51:41', '2023-11-14 12:51:41'),
(12, 'image', NULL, '2023-11-14 12:51:41', '2023-11-20 12:50:19'),
(13, 'copyright', 'Copyright © 2023 10 Makra 1979 High School All rights reserved.', '2023-11-14 12:51:41', '2023-11-14 12:51:41'),
(14, 'address', '22-23 វិថី ព្រះសង្ឃរាជ ទេព វង្ស, ក្រុងសៀមរាប', '2023-11-14 13:34:22', '2023-11-15 11:38:25'),
(15, 'phone_number', '+855966106833', '2023-11-14 13:34:22', '2023-11-15 11:57:42'),
(16, 'email', 'admin@admin.com', '2023-11-14 13:34:22', '2023-11-15 11:38:25'),
(17, 'facebook', 'https://www.facebook.com/10makara1979highschool', '2023-11-14 13:34:22', '2023-11-15 11:38:25'),
(18, 'telegram', 'https://t.me/teacher10mk', '2023-11-14 13:34:22', '2023-11-15 11:42:04'),
(19, 'full_mobile', '+25796 610 6833', '2023-11-14 13:46:36', '2023-11-14 13:47:09'),
(20, 'phone_number1', '+855555555555', '2023-11-15 11:55:37', '2023-11-15 11:57:42'),
(21, 'phone_number2', '+855555555555', '2023-11-15 11:55:37', '2023-11-15 11:55:37'),
(22, 'active_image_names', '2023-12-09-65734fba6f68c.png', '2023-11-20 12:30:11', '2023-12-09 05:18:11'),
(23, 'vission_image_names', '2023-11-20-655b5453a8fe6.png', '2023-11-20 12:43:03', '2023-11-20 12:43:03'),
(24, 'about_image_names', '2023-11-20-655b560853185.png', '2023-11-20 12:50:19', '2023-11-20 12:50:19'),
(25, 'banner_image_names', '2024-01-08-659bf04870130.png', '2023-11-20 12:56:19', '2024-01-09 00:53:36'),
(26, 'banner_text', 'Welcome to 10 Makra 1979 High School Website.', '2023-11-20 13:05:48', '2024-03-23 00:07:20'),
(28, 'grade_type', 'real_science', '2024-03-22 18:53:55', '2024-03-23 07:33:11'),
(29, 'limit_hour', '{\"teacher\":{\"secondary_school\":\"8\",\"high_school\":\"10\"},\"subject\":{\"21\":{\"grade_12\":\"4\",\"grade_11\":null,\"grade_6\":\"4\",\"grade_10\":\"6\",\"grade_2\":\"6\",\"grade_3\":{\"real_science\":\"4\",\"social_science\":\"6\"}},\"25\":{\"grade_12\":\"6\",\"grade_11\":null,\"grade_6\":\"6\",\"grade_10\":\"6\",\"grade_2\":\"6\",\"grade_3\":{\"real_science\":\"6\",\"social_science\":\"2\"}},\"26\":{\"grade_12\":\"3\",\"grade_11\":null,\"grade_6\":\"2\",\"grade_10\":\"2\",\"grade_2\":\"2\",\"grade_3\":{\"real_science\":\"2\",\"social_science\":\"2\"}},\"27\":{\"grade_12\":\"2\",\"grade_11\":null,\"grade_6\":\"2\",\"grade_10\":\"2\",\"grade_2\":\"2\",\"grade_3\":{\"real_science\":\"2\",\"social_science\":\"2\"}},\"28\":{\"grade_12\":\"2\",\"grade_11\":null,\"grade_6\":\"4\",\"grade_10\":\"4\",\"grade_2\":\"4\",\"grade_3\":{\"real_science\":\"4\",\"social_science\":\"1\"}},\"29\":{\"grade_12\":\"2\",\"grade_11\":null,\"grade_6\":\"1\",\"grade_10\":\"1\",\"grade_2\":\"1\",\"grade_3\":{\"real_science\":\"1\",\"social_science\":\"3\"}},\"30\":{\"grade_12\":\"1\",\"grade_11\":null,\"grade_6\":\"1\",\"grade_10\":\"1\",\"grade_2\":\"1\",\"grade_3\":{\"real_science\":\"1\",\"social_science\":\"2\"}},\"31\":{\"grade_12\":\"1\",\"grade_11\":\"1\",\"grade_6\":null,\"grade_10\":null,\"grade_2\":null,\"grade_3\":{\"real_science\":null,\"social_science\":null}}}}', '2024-03-22 18:56:50', '2024-03-23 18:09:23');

-- --------------------------------------------------------

--
-- Table structure for table `slot_times`
--

CREATE TABLE `slot_times` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slot_times`
--

INSERT INTO `slot_times` (`id`, `start_time`, `end_time`, `order`, `created_at`, `updated_at`) VALUES
(2, '08:00:00', '09:00:00', 1, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(3, '09:00:00', '10:00:00', 2, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(4, '10:00:00', '11:00:00', 3, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(5, '11:00:00', '12:00:00', 4, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(6, '12:00:00', '13:00:00', 5, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(7, '13:00:00', '14:00:00', 6, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(8, '14:00:00', '15:00:00', 7, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(9, '15:00:00', '16:00:00', 8, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(10, '16:00:00', '17:00:00', 9, '2023-09-18 16:32:42', '2023-09-18 16:32:42'),
(22, '07:00:00', '08:00:00', NULL, '2024-03-21 23:33:32', '2024-03-21 23:33:32');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` text DEFAULT NULL,
  `subject_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `grade_id`, `subject_type_id`, `name`, `created_at`, `updated_at`) VALUES
(21, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'ភាសារខ្មែរ', '2024-03-22 18:31:52', '2024-03-23 18:08:19'),
(25, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'គណិតវិទ្យា', '2024-03-22 18:32:36', '2024-03-23 18:07:15'),
(26, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'រូបវិទ្យា', '2024-03-22 18:33:00', '2024-03-23 18:08:35'),
(27, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'ជីវវិទ្យា', '2024-03-22 18:33:43', '2024-03-23 18:07:49'),
(28, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'អង្គគ្លេស', '2024-03-22 18:34:20', '2024-03-23 18:08:42'),
(29, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'ភូមិវិទ្យា', '2024-03-22 18:34:20', '2024-03-23 18:08:26'),
(30, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'ផែនដីវិទ្យា', '2024-03-22 18:54:58', '2024-03-23 18:08:02'),
(31, '[\"12\",\"11\",\"6\",\"10\",\"2\",\"3\"]', NULL, 'គេហះវិទ្យា', '2024-03-23 11:47:35', '2024-03-23 18:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `subject_types`
--

CREATE TABLE `subject_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_types`
--

INSERT INTO `subject_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'វិទ្យសាស្រ្ត', '2023-10-22 04:52:25', '2023-10-22 04:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `primary_subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `secondary_subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'high_school',
  `khmer_fname` varchar(255) NOT NULL,
  `khmer_lname` varchar(255) NOT NULL,
  `doe` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `user_id`, `first_name`, `last_name`, `gender`, `phone`, `username`, `password`, `email`, `code`, `address`, `image`, `status`, `created_at`, `updated_at`, `primary_subject_id`, `secondary_subject_id`, `type`, `khmer_fname`, `khmer_lname`, `doe`, `deleted_at`) VALUES
(8, 29, 'Chan', 'Darong', '1', '0888296818', 'rong', '11112222', 'rong@gmail.com', 'M90', '', '2024-03-19-65f981c1d4a3f.png', 1, '2023-08-05 02:35:53', '2024-03-22 17:58:50', 11, 10, 'secondary_school', 'ដារុង', 'ចាន់', '2015', NULL),
(9, NULL, 'Tola', 'Ken', '2', '010921298', 'Tola', '11112222', 'tola@gmail.com', 'T-05', 'Siem Reap', 'dsfzv2jclw3nkfrflqsi.png', 1, '2023-09-09 01:34:39', '2024-03-22 17:58:32', 18, 14, 'high_school', '', '', NULL, '2024-03-22 17:58:32'),
(11, NULL, 'Kendall', 'Martin', '1', '+1 (724) 176-2838', 'gitepoqyta', '11112222', 'racisex@mailinator.com', 'Accusamus omnis at u', 'Voluptatem quas est', NULL, 1, '2023-09-09 01:38:42', '2024-03-22 17:57:24', 20, 19, 'high_school', '', '', NULL, '2024-03-22 17:57:24'),
(13, NULL, 'Lois', 'Logan', '1', '+1 (555) 925-1566', 'nidapoqa', '11112222', 'zusoc@mailinator.com', 'Aut lorem qui quam e', 'Id nemo voluptatem a', NULL, 1, '2023-09-09 01:39:11', '2024-03-22 17:57:28', 16, 15, 'high_school', '', '', NULL, '2024-03-22 17:57:28'),
(17, 31, 'NARIN', 'MEN', '1', '096855788', 'MEN Narin', '11112222', 'Narin@mail.com', 'M3', '', NULL, 1, '2023-09-09 01:39:55', '2024-03-22 17:58:27', 10, 10, 'high_school', 'ម៉ែន', 'ណារិន្ទ', '2010', '2024-03-22 17:58:27'),
(18, NULL, 'Odessa', 'Solis', '1', '+1 (598) 682-9844', 'wemonybuto', '11112222', 'jufejav@mailinator.com', 'Maxime quia dicta en', 'Asperiores officia q', NULL, 1, '2023-09-09 01:40:10', '2024-03-22 17:58:13', 13, 12, 'high_school', '', '', NULL, '2024-03-22 17:58:13'),
(20, 6, 'Remedios', 'Strong', '1', '+1 (331) 808-2274', NULL, NULL, NULL, 'Aut minima error fac', 'Nostrum anim sit re', NULL, 1, '2023-10-24 04:16:28', '2024-03-22 17:58:16', NULL, NULL, 'secondary_school', '', '', NULL, '2024-03-22 17:58:16'),
(25, 14, 'Ly', 'Layheng', '1', '088824311', 'Layheng', NULL, 'lylayheng@10mk.com', 'SR0001', NULL, NULL, 1, '2023-12-16 01:27:22', '2024-03-22 17:57:33', NULL, NULL, 'high_school', '', '', NULL, '2024-03-22 17:57:33'),
(26, 15, 'Chy', 'Samnang', '1', '088824453', 'CHY SAMNANG', '11112222', 'samnangchy@10mk.com', 'C1', '', NULL, 1, '2023-12-16 01:27:22', '2024-03-22 17:57:20', 12, 12, 'secondary_school', 'ជី', 'សំណាង', '1998', '2024-03-22 17:57:20'),
(27, 16, 'Men', 'Narin', '1', '088824315', 'Narin', NULL, 'narinmen@10mk.com', 'SR0003', '', NULL, 1, '2023-12-16 01:27:22', '2024-03-22 17:57:37', 10, 10, 'high_school', '', '', NULL, '2024-03-22 17:57:37'),
(28, 17, 'Sin', 'Vanna', '1', '088824311', 'Vanna', NULL, 'vannasin@10mk.com', 'SR0004', NULL, NULL, 1, '2023-12-16 01:27:22', '2024-03-22 17:58:24', NULL, NULL, 'high_school', '', '', NULL, '2024-03-22 17:58:24'),
(31, 24, 'Vannak', 'Torn', '1', '445222331', 'Vannak', '11112222', 'Vannak@gmail.com', 'K1', 'Siem Reap, Siem Reap, Cambodia', NULL, 1, '2024-01-03 19:03:34', '2024-03-22 17:58:37', 14, 14, 'secondary_school', 'តន់', 'វណ្ណះ', NULL, '2024-03-22 17:58:37'),
(32, 26, 'Tuy', 'Chhunleang', '1', '0966106833', 'Tuy Chhunleang', '11112222', 'tuychhunleang@gmail.com', 'M91', '', '2024-01-14-65a37117afcd1.png', 1, '2024-01-14 17:20:51', '2024-03-23 07:39:29', 21, 21, 'high_school', 'ទុយ', 'ឈុនលាង', '2020', NULL),
(33, 27, 'Kanha', 'Mao', '2', '11113372', 'Kanha', '11112222', 'Kanha@mail.com', 'B1', 'Siem Reap, Siem Reap, Cambodia', NULL, 1, '2024-01-15 14:09:48', '2024-03-22 15:56:06', NULL, NULL, 'high_school', 'កញ្ញា', 'ម៉ៅ', NULL, '2024-03-22 15:56:06'),
(34, 36, 'John', 'Doe', 'Male', '1234567890', 'johndoe', NULL, 'john@example.com', '1', NULL, NULL, 1, '2024-03-22 16:14:45', '2024-03-22 16:15:55', NULL, NULL, 'high_school', 'Doe', 'Doe', NULL, '2024-03-22 16:15:55'),
(35, 37, 'Jane', 'Smith', 'Female', '1286608618', 'janesmith', NULL, 'jane@example.com', '2', NULL, NULL, 1, '2024-03-22 16:14:45', '2024-03-22 16:15:51', NULL, NULL, 'high_school', 'Smith', 'Smith', NULL, '2024-03-22 16:15:51'),
(36, 38, 'Alex', 'Johnson', 'Male', '1260588259', 'alexjohn', NULL, 'alex@example.com', '3', NULL, NULL, 1, '2024-03-22 16:14:45', '2024-03-22 16:15:46', NULL, NULL, 'high_school', 'Johnson', 'Johnson', NULL, '2024-03-22 16:15:46'),
(37, 39, 'sei', 'la', '1', '012345678', 'seila', '11112222', 'seila@gmail.com', 'k100', 'ជ្រាវ', NULL, 1, '2024-03-22 16:24:55', '2024-03-22 17:58:20', NULL, NULL, 'secondary_school', 'សី', 'ឡា', '2020', '2024-03-22 17:58:20'),
(128, 130, 'LAYHENG', 'LY', '1', '12345678', 'LY LAYHENG', NULL, 'LY.LAYHENG@10mkr79.com', 'M1', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:38:13', 27, 27, 'secondary_school', 'លី', 'ឡៃហេង', '2019', NULL),
(129, 131, 'SAMNANG', 'CHY', '1', '87654321', 'CHY SAMNANG', NULL, 'CHY.SAMNANG@10mkr79.com', 'M2', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:40:26', 28, 21, 'high_school', 'ជី', 'សំណាង', '1990', NULL),
(130, 132, 'NARIN', 'MEN', '1', '93210987', 'MEN NARIN', NULL, 'MEN.NARIN@10mkr79.com', 'M6', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:38:25', 28, 28, 'high_school', 'ម៉ែន', 'ណារិន្ទ', '1980', NULL),
(131, 133, 'VANNA', 'SIN', '1', '76543210', 'SIN VANNA', NULL, 'SIN.VANNA@10mkr79.com', 'M4', 'Siem Reap', NULL, 1, '2024-03-22 18:16:43', '2024-03-23 12:02:42', 25, NULL, 'secondary_school', 'ស៊ិន', 'វណ្ណា', '2023', '2024-03-23 12:02:42'),
(132, 134, 'ROATRASMEY', 'CHHENG', '1', '10101010', 'CHHENG ROATRASMEY', NULL, 'CHHENG.ROATRASMEY@10mkr79.com', 'M12', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:40:09', 25, 25, 'secondary_school', 'ឆេង', 'រ័ត្នរស្មី', '2019', NULL),
(133, 135, 'VANDA', 'SUN', '1', '88888888', 'SUN VANDA', NULL, 'SUN.VANDA@10mkr79.com', 'M8', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:20:26', 11, 11, 'high_school', 'ស៊ុន', 'វ៉ាន់ដា', '2006', NULL),
(134, 136, 'KEOVIRAK', 'SREY', '1', '99999999', 'SREY KEOVIRAK', NULL, 'SREY.KEOVIRAK@10mkr79.com', 'M7', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:37:46', 25, 25, 'high_school', 'ស្រី', 'កែវវីរ:', '2010', NULL),
(135, 137, 'TECHSONG', 'LAM', '1', '77777777', 'LAM TECHSONG', NULL, 'LAM.TECHSONG@10mkr79.com', 'M5', '', NULL, 1, '2024-03-22 18:16:43', '2024-03-22 18:20:00', 13, 14, 'high_school', 'ឡាំ', 'តិចសុង', '2019', NULL),
(136, 138, 'KIMCHO', 'LEAM', '1', '85123456', 'LEAM KIMCHO', NULL, 'LEAM.KIMCHO@10mkr79.com', 'M10', '', NULL, 1, '2024-03-22 18:16:44', '2024-03-22 18:37:58', 25, 25, 'high_school', 'លៀម', 'គឹមចូរ', '2020', NULL),
(137, 139, 'Som', 'Dara', '1', '0987654333', 'Som Dara', '11112222', 'som.dara@gmail.com', 'K-12', '', '2024-03-22-65fd79d6f28e0.png', 1, '2024-03-22 23:25:49', '2024-03-22 23:30:20', NULL, NULL, 'high_school', 'សំ', 'ដារា', '2010', NULL),
(138, 143, 'Chum', 'Cheat', '1', '0919281281', 'cheat', '11112222', 'cheat@gmail.com', 'M-10', 'Siem Reap', NULL, 1, '2024-03-22 23:51:59', '2024-03-22 23:51:59', NULL, NULL, 'high_school', 'ជំ', 'ជាតិ', '2002', NULL),
(139, 144, 'Sor', 'Sey', '1', '0192919291', 'sey', '11112222', 'sey@gmail.com', 'K-11', 'Siem Reap', NULL, 1, '2024-03-22 23:53:54', '2024-03-22 23:54:28', NULL, NULL, 'high_school', 'សោ', 'សី', '2019', '2024-03-22 23:54:28'),
(140, 145, 'Thoeun', 'Khunhy', '1', '012408523', 'Thoeun Khunhy', '11112222', 'thoeunkhunhy@gmail.com', 'K-13', 'Siem Reap', '2024-03-23-65fe2554bc1c7.png', 1, '2024-03-23 11:42:23', '2024-03-23 12:03:15', 21, NULL, 'high_school', 'ធឿន', 'ឃុនហឺ', '2023', NULL),
(141, 147, 'Prach', 'Penh', '1', '01255568', 'Penh Penh', '11112222', 'penh@mail.com', 'K99', 'siemreap', '2024-03-23-65fe3bbf2921b.png', 1, '2024-03-23 13:17:40', '2024-03-23 13:24:18', 21, NULL, 'high_school', 'ប្រាច', 'ពេញ', '2019', NULL),
(152, 159, 'SENG AN', 'EA', 'male', '091234567', 'EA SENG AN', NULL, 'EA.SENGAN@10mkr79.com', 'B1', NULL, NULL, 1, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, NULL, 'high_school', 'អ៊ា', 'សេងអាន', NULL, NULL),
(153, 160, 'PHALLET', 'SEAN', 'male', '096789012', 'SEAN PHALLET', NULL, 'SEAN.PHALLET@10mkr79.com', 'B2', NULL, NULL, 1, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, NULL, 'high_school', 'ស៊ាន', 'ផល្លិត', NULL, NULL),
(154, 161, 'SOTHY', 'SOUN', 'male', '082345678', 'SOUN SOTHY', NULL, 'SOUN.SOTHY@10mkr79.com', 'B3', NULL, NULL, 1, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, NULL, 'high_school', 'ស៊ន់', 'សុធី', NULL, NULL),
(155, 162, 'SEREI VUTH', 'VOATH', 'male', '094567890', 'VOATH SEREI VUTH', NULL, 'VOATH.SEREIVUTH@10mkr79.com', 'B4', NULL, NULL, 1, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, NULL, 'high_school', 'វត', 'សេរីវុធ', NULL, NULL),
(156, 163, 'SOPHAK', 'VAY', 'male', '089012345', 'VAY SOPHAK', NULL, 'VAY.SOPHAK@10mkr79.com', 'B5', NULL, NULL, 1, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, NULL, 'high_school', 'វ៉ៃ', 'សុភ័ក្រ្ត', NULL, NULL),
(157, 164, 'Veung', 'Bunleng', '1', '092919291', 'Veung Bunleng', '11112222', 'bunleng@gmail.com', 'M60', 'Siem Reap', '2024-03-23-65fe8b5cd7ff9.png', 1, '2024-03-23 18:58:01', '2024-03-23 18:58:31', 25, NULL, 'high_school', 'វឹង', 'ប៊ុនឡេង', '2024', NULL),
(158, 165, 'KHEN', 'TEY', 'male', '095678901', 'TEY KHEN', NULL, 'TEY.KHEN@10mkr79.com', 'E1', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'តី', 'ខេន', NULL, NULL),
(159, 166, 'TONY', 'KHOUN', 'male', '079012345', 'KHOUN TONY', NULL, 'KHOUN.TONY@10mkr79.com', 'E2', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'ឃួន', 'តូនី', NULL, NULL),
(160, 167, 'SOPHEAP', 'SOEUR', 'male', '081234567', 'SOEUR SOPHEAP', NULL, 'SOEUR.SOPHEAP@10mkr79.com', 'E3', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'សឿ', 'សុភាព', NULL, NULL),
(161, 168, 'THYRO', 'HOUT', 'male', '097890123', 'HOUT THYRO', NULL, 'HOUT.THYRO@10mkr79.com', 'E4', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'ហួត', 'ធីរ៉ូ', NULL, NULL),
(162, 169, 'HENG', 'HORN', 'male', '098765432', 'HORN HENG', NULL, 'HORN.HENG@10mkr79.com', 'E5', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'ហ៊ន', 'ហេង', NULL, NULL),
(163, 170, 'RATHA', 'SOK', 'male', '084567890', 'SOK RATHA', NULL, 'SOK.RATHA@10mkr79.com', 'E6', NULL, NULL, 1, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, NULL, 'high_school', 'សុខ', 'រដ្ឋា', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timetable_slots`
--

CREATE TABLE `timetable_slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `timetable_id` bigint(20) UNSIGNED NOT NULL,
  `week_day` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') NOT NULL,
  `time_slot_id` bigint(20) UNSIGNED DEFAULT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timetable_slots`
--

INSERT INTO `timetable_slots` (`id`, `timetable_id`, `week_day`, `time_slot_id`, `teacher_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(43, 144, 'monday', 22, 140, 21, '2024-03-23 13:23:30', '2024-03-23 17:41:19'),
(44, 146, 'monday', 2, 141, 21, '2024-03-23 13:26:42', '2024-03-23 13:26:42'),
(45, 147, 'monday', 22, 140, 21, '2024-03-23 13:58:02', '2024-03-23 13:58:02'),
(46, 144, 'monday', 2, 140, 21, '2024-03-23 17:41:55', '2024-03-23 17:41:55'),
(47, 149, 'monday', 22, 128, 27, '2024-03-23 18:10:51', '2024-03-23 18:11:21'),
(48, 149, 'tuesday', 22, 136, 25, '2024-03-23 18:11:07', '2024-03-23 18:11:07'),
(49, 149, 'monday', 2, 32, 21, '2024-03-23 19:07:11', '2024-03-23 19:07:46'),
(50, 150, 'monday', 22, 132, 25, '2024-03-23 19:07:26', '2024-03-23 19:07:26'),
(51, 151, 'monday', 22, 157, 25, '2024-03-23 19:16:45', '2024-03-23 19:16:45'),
(52, 151, 'monday', 2, 128, 27, '2024-03-23 19:17:23', '2024-03-23 19:17:23'),
(53, 151, 'tuesday', 22, 140, 21, '2024-03-23 19:17:54', '2024-03-23 19:17:54'),
(54, 151, 'wednesday', 22, 128, 27, '2024-03-23 19:18:09', '2024-03-23 19:18:09'),
(55, 151, 'thursday', 22, 129, 28, '2024-03-23 19:18:22', '2024-03-23 19:18:22'),
(56, 151, 'friday', 22, 128, 27, '2024-03-23 19:18:52', '2024-03-23 19:18:52'),
(57, 151, 'saturday', 22, 157, 25, '2024-03-23 19:19:14', '2024-03-23 19:19:14'),
(58, 151, 'tuesday', 2, 157, 25, '2024-03-23 19:19:37', '2024-03-23 19:19:37'),
(59, 151, 'wednesday', 2, 128, 27, '2024-03-23 19:19:50', '2024-03-23 19:19:50'),
(60, 151, 'thursday', 2, 130, 28, '2024-03-23 19:20:04', '2024-03-23 19:20:04'),
(61, 151, 'friday', 2, 32, 21, '2024-03-23 19:20:23', '2024-03-23 19:20:23'),
(62, 151, 'saturday', 2, 157, 25, '2024-03-23 19:20:39', '2024-03-23 19:20:39'),
(63, 153, 'tuesday', 22, 157, 25, '2024-03-23 19:25:15', '2024-03-23 19:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `time_tables`
--

CREATE TABLE `time_tables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `academic_year_id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `class_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_tables`
--

INSERT INTO `time_tables` (`id`, `academic_year_id`, `grade_id`, `class_id`, `created_by`, `created_at`, `updated_at`) VALUES
(144, 6, 2, 17, 3, '2024-03-23 13:23:30', '2024-03-23 13:23:30'),
(146, 6, 3, 19, 3, '2024-03-23 13:26:42', '2024-03-23 13:26:42'),
(147, 8, 2, 17, 146, '2024-03-23 13:58:02', '2024-03-23 13:58:02'),
(149, 6, 12, 23, 3, '2024-03-23 18:10:51', '2024-03-23 18:10:51'),
(150, 1, 10, 21, 3, '2024-03-23 19:07:26', '2024-03-23 19:07:26'),
(151, 6, 3, 25, 3, '2024-03-23 19:16:45', '2024-03-23 19:16:45'),
(153, 6, 3, 20, 3, '2024-03-23 19:25:15', '2024-03-23 19:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `translationable_type` varchar(255) DEFAULT NULL,
  `translationable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `translationable_type`, `translationable_id`, `locale`, `key`, `value`) VALUES
(1, 'App\\Models\\Setting', 5, 'en', 'about_us', 'This is about us'),
(2, 'App\\Models\\Setting', 5, 'kh', 'about_us', '<p>អំពីយើង</p>'),
(3, 'App\\Models\\Setting', 7, 'en', 'vision', '<h1 class=\"\" style=\"font-family: Inter, &quot;Kantumruy Pro&quot;, sans-serif;\">ចក្ខុវិស័យ<br></h1><h1 class=\"\"></h1><h6 class=\"\"></h6><h3 class=\"\"></h3><h1 class=\"\"><blockquote class=\"blockquote\"><ul style=\"\"><li style=\"\">អប់រំចំណេះសមូហភាព</li></ul><ul style=\"\"><li>អប់រំចំណេះអប់រំឥរិយាបថ</li></ul><ul style=\"\"><li>អប់រំចំណេះធ្វើ</li></ul><h6 class=\"\"><ul style=\"\"><li style=\"\">អប់រំចំណេះដឹង</li></ul></h6></blockquote></h1>'),
(4, 'App\\Models\\Setting', 7, 'kh', 'vision', '<h1 class=\"\">ចក្ខុវិស័យ</h1><ul><li>អប់រំចំណេះសមូហភាព</li><li>អប់រំចំណេះអប់រំឥរិយាបថ</li><li>អប់រំចំណេះធ្វើ</li><li>អប់រំចំណេះដឹង</li></ul>'),
(5, 'App\\Models\\Setting', 8, 'en', 'app_name', '10 Makra 1979 High School'),
(6, 'App\\Models\\Setting', 8, 'kh', 'app_name', 'វិទ្យាល័យ ១០ មករា ១៩៧៩'),
(7, 'App\\Models\\Setting', 10, 'en', 'desc', 'បទបង្ហាញ របស់សិស្ស ថ្នាក់ទី 12C ភាសាអង់គ្លេស'),
(8, 'App\\Models\\Setting', 10, 'kh', 'desc', 'បទបង្ហាញ យុវសិស្ស សៀ សុខហ៊ុង 11Q វិ 10 មករា 1979\r\n💁. ប្រធានបទ«ព្រះអង្គចេក ព្រះអង្គចម»អធិប្បាយជាខ្មែរនិងអង់គ្លេស(ខេត្តសៀមរាប)'),
(9, 'App\\Models\\Setting', 13, 'en', 'copyright', 'Copyright © 2023 10 Makra 1979 High School All rights reserved.'),
(10, 'App\\Models\\Setting', 13, 'kh', 'copyright', 'Copyright © 2023 វិទ្យាល័យ ១០ មករា ១៩៧៩.'),
(11, 'App\\Models\\Setting', 26, 'en', 'banner_text', 'Welcome to 10 Makra 1979 High School Website.'),
(12, 'App\\Models\\Setting', 26, 'kh', 'banner_text', 'សូមស្វាគមន៍មកកាន់គេហទំព័រវិទ្យាល័យ ១០ មករា ១៩៧៩');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `image`, `first_name`, `last_name`, `type`, `gender`) VALUES
(3, 'superadmin', 'admin@admin.com', NULL, '$2y$10$AHt7qAoD6KuwXb6QW33zj.f4sV3bRAm3BXM4yJUgGFbBRo7xqZGpi', NULL, '2023-10-17 04:48:06', '2024-03-23 14:35:19', '2024-03-22-65fcf5aacbf34.png', 'Admin', 'Admin', '', '1'),
(4, 'Darong CHAN', 'darong@gmail.com', NULL, '$2y$10$FHT/BcrMWy2A0SQAzIzOiuf9wzrZ3WST4gSI6jaI6OfkurcdWbx5m', NULL, '2023-10-17 06:00:26', '2023-12-08 00:49:35', '5w5woykycx8rxuwdccta.jpeg', 'Darong', 'CHAN', '', '1'),
(7, 'phierak', 'phierak@gmail.com', NULL, '$2y$10$R43oD.0ng4Ot3hw2202k9OVDLN6xn45rsQPGpfb9tZ4.lPWQ5p8Bm', NULL, '2023-12-09 15:15:11', '2024-03-21 01:15:38', NULL, 'Liem', 'Phierak', '', '1'),
(14, 'LY LAYHENG', 'lylayheng@10mk.com', NULL, '$2y$10$h0nE.CEp2DlAg.Z9Wg/EfeW4K5itjevoK5SjGAJuDqJE3gXRYXc0S', NULL, '2023-12-16 01:27:22', '2023-12-16 01:27:22', NULL, 'Ly', 'Layheng', NULL, NULL),
(15, 'CHY SAMNANG', 'samnangchy@10mk.com', NULL, '$2y$10$fpSss5TApWETNnfCms.DcOFMmaclmqPztkfMe1jX3nLMTr9Q6Awja', NULL, '2023-12-16 01:27:22', '2024-03-21 01:20:17', NULL, 'Chy', 'Samnang', '', '1'),
(16, 'MEN NARIN', 'narinmen@10mk.com', NULL, '$2y$10$bsxXxc73kjMz9JQzLCX76ebN6hxIckzZOu5zObi3sZXmZrGavKz22', NULL, '2023-12-16 01:27:22', '2023-12-16 01:27:22', NULL, 'Men', 'Narin', NULL, NULL),
(17, 'SIN VANNA', 'vannasin@10mk.com', NULL, '$2y$10$W089nVMsCshwtBR7cjQNZujSy1FhatCL01I7CmwYahu.mbD/j/7fK', NULL, '2023-12-16 01:27:22', '2023-12-16 01:27:22', NULL, 'Sin', 'Vanna', NULL, NULL),
(26, 'Tuy Chhunleang', 'tuychhunleang@gmail.com', NULL, '$2y$10$q8GWOSvShmuydkKaeha4P.TGM2iJV5SU5/Nk3g4YI6tZmfiNrR6J.', NULL, '2024-01-14 17:20:51', '2024-03-22 14:10:03', '2024-01-14-65a36f19dbf68.png', 'Chhunleang', 'Tuy', '', '1'),
(27, 'Kanha Mao', 'Kanha@mail.com', NULL, '$2y$10$HYATpC5oRDd/fNvsZOWCju9/HVwnuf8Xgoh418oSwjyFj5iIhogzS', NULL, '2024-01-15 14:09:48', '2024-01-15 23:50:56', '2024-01-15-65a493cbddc2b.png', 'dara', 'roth', '', '1'),
(28, 'sovan', 'sovan@mail.com', NULL, '$2y$10$MvuSpI3.yHzh4R2GsJfP9epbXEudadye3uKDRWwKqlDcTNm22J7Ca', NULL, '2024-03-13 00:29:11', '2024-03-13 00:29:11', NULL, NULL, NULL, NULL, NULL),
(29, 'rong', 'rong@gmail.com', NULL, '$2y$10$ba1f6UxUJxY3xjMmXPjU7eS4bdnrj9D3dN/y9Dp4.Ju/NkJro4D9O', NULL, '2024-02-13 23:46:14', '2024-03-19 23:15:57', '2024-03-19-65f981c1d4a3f.png', NULL, NULL, NULL, NULL),
(30, 'Darong', 'darong123@gmail.com', NULL, '$2y$10$BeW.F741ZwhEKTAxuHfGu.FRT4Z7Jf6C5NYdfdyNqpGiUQ39S3KIu', NULL, '2024-02-20 00:10:32', '2024-03-19 23:10:32', NULL, 'Chan', 'Darong', '', '1'),
(31, 'MEN Narin', 'Narin@mail.com', NULL, '$2y$10$dmOKjsCwfTNjYHAleZXTqujMvVV6oiLCldbzIYc28gVD6x5GBCUuC', NULL, '2024-03-22 15:54:45', '2024-03-22 15:54:45', NULL, NULL, NULL, NULL, NULL),
(36, 'John Doe', 'john@example.com', NULL, '$2y$10$mCRBqChp08/8ERHILl2umuzBgth3qhdLOJXpFemunioNGfVa3wwUK', NULL, '2024-03-22 16:14:45', '2024-03-22 16:14:45', NULL, 'John', 'Doe', NULL, NULL),
(37, 'Jane Smith', 'jane@example.com', NULL, '$2y$10$ZiuZpzqybdZflCjYqtcbRefv9tArdRugFgLy6luytlbM5XHmegoIi', NULL, '2024-03-22 16:14:45', '2024-03-22 16:14:45', NULL, 'Jane', 'Smith', NULL, NULL),
(38, 'Alex Johnson', 'alex@example.com', NULL, '$2y$10$wkWXGHOXS0xbyyq3SCErquGhj6ytY13ug.jfVkD3/cHbZmJA.7LYK', NULL, '2024-03-22 16:14:45', '2024-03-22 16:14:45', NULL, 'Alex', 'Johnson', NULL, NULL),
(39, 'sei la', 'seila@gmail.com', NULL, '$2y$10$4B..nTcfdpbbeJCewUUTH.pucbE74gXodnjKSYs3bDmfL9ggVU512', NULL, '2024-03-22 16:24:55', '2024-03-22 16:24:55', '2024-03-22-65fd159654964.png', NULL, NULL, NULL, NULL),
(130, 'LY LAYHENG', 'LY.LAYHENG@10mkr79.com', NULL, '$2y$10$JgCLX6Bwkv1.yyYydrzCeeBNl7LWDI57iaMHr3WGwr6gF5UolnosG', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'LAYHENG', 'LY', NULL, NULL),
(131, 'CHY SAMNANG', 'CHY.SAMNANG@10mkr79.com', NULL, '$2y$10$fKOOJwXJe1c89VALkRmp9Ot8/HgPAiF1I0Sagqxke.fx9rTMJuKsS', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'SAMNANG', 'CHY', NULL, NULL),
(132, 'MEN NARIN', 'MEN.NARIN@10mkr79.com', NULL, '$2y$10$Xd2SktBQPWlmSJHL43fxYOlswnxb6odeLCNl76EW/bSEpwpWi.iPa', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'NARIN', 'MEN', NULL, NULL),
(133, 'SIN VANNA', 'SIN.VANNA@10mkr79.com', NULL, '$2y$10$9iRvPPnAUHbRn5z5PA/rQ.kdW5rZF7FOqFaZ13auxEdcGseaPYQlu', NULL, '2024-03-22 18:16:43', '2024-03-23 12:02:25', NULL, 'VANNA', 'SIN', NULL, '1'),
(134, 'CHHENG ROATRASMEY', 'CHHENG.ROATRASMEY@10mkr79.com', NULL, '$2y$10$F/8IHrfJQd8DArFTIbZC3.oi/q6OPT8F0iN87sXSgHVez.dKX8OWa', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'ROATRASMEY', 'CHHENG', NULL, NULL),
(135, 'SUN VANDA', 'SUN.VANDA@10mkr79.com', NULL, '$2y$10$JKAuXb4cSaHg/nCKdBwKCOetR3c9h6Ei8aWAOJcaJQVGD50gZ8LnK', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'VANDA', 'SUN', NULL, NULL),
(136, 'SREY KEOVIRAK', 'SREY.KEOVIRAK@10mkr79.com', NULL, '$2y$10$P5UiUWB40b6c0cYWivOTB.0l67Fm8GFzDN7b3.6JYXrXPeuXGhxEi', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'KEOVIRAK', 'SREY', NULL, NULL),
(137, 'LAM TECHSONG', 'LAM.TECHSONG@10mkr79.com', NULL, '$2y$10$26HcByCP9/a.Cpl12NTYB.MVJZxWrdYUV8oqzr5hNezxun92g/0O2', NULL, '2024-03-22 18:16:43', '2024-03-22 18:16:43', NULL, 'TECHSONG', 'LAM', NULL, NULL),
(138, 'LEAM KIMCHO', 'LEAM.KIMCHO@10mkr79.com', NULL, '$2y$10$Gz16vQPryIyPSkLtPxVxIO1IWhqxoMKhib6pE4HUaShjieBEyp1ny', NULL, '2024-03-22 18:16:44', '2024-03-22 18:16:44', NULL, 'KIMCHO', 'LEAM', NULL, NULL),
(139, 'Som Dara', 'som.dara@gmail.com', NULL, '$2y$10$gXyOPmHix1YnWgscMXoJ9ek.Z4NW6o6LauY.ZxzyHvoZ30KMIS8lC', NULL, '2024-03-22 23:25:49', '2024-03-23 18:27:43', '2024-03-22-65fd79d6f28e0.png', NULL, NULL, NULL, NULL),
(142, 'darong1', 'darong1234@gmail.com', NULL, '$2y$10$EFr7uJ51QTq7QixvUF9o.uC9cP0SlzaqbPgYUZDSiUkq8GGkMMrCq', NULL, '2024-03-22 23:48:31', '2024-03-22 23:48:31', '2024-03-22-65fd7e19e6d19.png', 'Chan', 'Darong', '', '1'),
(143, 'Chum Cheat', 'cheat@gmail.com', NULL, '$2y$10$fhAh9HdyoPOV/mkkEITvreYIOD/eGAe/N3y/STR9isIVho6fnxtH2', NULL, '2024-03-22 23:51:59', '2024-03-22 23:51:59', '2024-03-22-65fd7ea452d77.png', NULL, NULL, NULL, NULL),
(144, 'Sor Sey', 'sey@gmail.com', NULL, '$2y$10$YV2fpZCDHDfZW/kXQe0EUOcZ5ZbecLaL16Fw0FTW1pcF.APtNGhnq', NULL, '2024-03-22 23:53:54', '2024-03-22 23:53:54', '2024-03-22-65fd7f507c8fe.png', NULL, NULL, NULL, NULL),
(145, 'Thoeun Khunhy', 'thoeunkhunhy@gmail.com', NULL, '$2y$10$VHqWmhK95DAb.HEvGjeQhOJNuwkbRgo7Ivic6kgAzkhAmvzYVPAJ2', NULL, '2024-03-23 11:42:23', '2024-03-23 11:42:23', '2024-03-23-65fe2554bc1c7.png', 'Thoeun', 'Khunhy', NULL, '1'),
(146, 'Khunhy', 'Khunhy@gmail.com', NULL, '$2y$10$0k0ZVHTyfYe27IhPvbnrrer5wX08o83q3X8xmYV4aUu2jrqszDppm', NULL, '2024-03-23 11:44:18', '2024-03-23 11:44:18', '2024-03-23-65fe25dbd17a6.png', 'Thoeun', 'Khunhy', '', '1'),
(147, 'Penh Penh', 'penh@mail.com', NULL, '$2y$10$Xq6ZEbdjFOwR9YN/TLooWO8XzktYMHC7rEvDPiyGpe9iimEcWUWae', NULL, '2024-03-23 13:17:40', '2024-03-23 13:24:18', '2024-03-23-65fe3bbf2921b.png', 'Prach', 'Penh', NULL, '1'),
(159, 'EA SENG AN', 'EA.SENGAN@10mkr79.com', NULL, '$2y$10$p.7Lk53s8RlM7bt.KTOg..E..CE8bYf/2vgLBKCtjbl6h8n0DZ8F.', NULL, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, 'SENG AN', 'EA', NULL, NULL),
(160, 'SEAN PHALLET', 'SEAN.PHALLET@10mkr79.com', NULL, '$2y$10$gZ5Zw8OF44vcECBtEDP4WuEWMdgnHsfgKdYSeqHZQh7TQaYGOIrwq', NULL, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, 'PHALLET', 'SEAN', NULL, NULL),
(161, 'SOUN SOTHY', 'SOUN.SOTHY@10mkr79.com', NULL, '$2y$10$PqXXNIeEi0owcJUKlqbIVudw3/JAfAuOCTYSLaHKLEM2KBtaoF4Vq', NULL, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, 'SOTHY', 'SOUN', NULL, NULL),
(162, 'VOATH SEREI VUTH', 'VOATH.SEREIVUTH@10mkr79.com', NULL, '$2y$10$9AFSOoGEnq8.STnFApVkW.C79CyQ4JCeN03ioBtIq.JCbuvzPsAH2', NULL, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, 'SEREI VUTH', 'VOATH', NULL, NULL),
(163, 'VAY SOPHAK', 'VAY.SOPHAK@10mkr79.com', NULL, '$2y$10$LF2/CFDNCc/Ep4WehE6RhehsEEqR6BvGR4MTDW2MManzybYFb5gLu', NULL, '2024-03-23 14:34:21', '2024-03-23 14:34:21', NULL, 'SOPHAK', 'VAY', NULL, NULL),
(164, 'Veung Bunleng', 'bunleng@gmail.com', NULL, '$2y$10$nN/SywEWs2C67GjY3ojrlO2IO8c3IThKAQntXrsbBmWGHYp.z3gk.', NULL, '2024-03-23 18:58:01', '2024-03-23 18:58:01', '2024-03-23-65fe8b5cd7ff9.png', 'Veung', 'Bunleng', NULL, '1'),
(165, 'TEY KHEN', 'TEY.KHEN@10mkr79.com', NULL, '$2y$10$YIf1hFTWPTLn6o5eP98Clu9/P5XzXT3aOmHwGwo/EGXzWJjsXsN1e', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'KHEN', 'TEY', NULL, NULL),
(166, 'KHOUN TONY', 'KHOUN.TONY@10mkr79.com', NULL, '$2y$10$4nUBF2fe4PhaNjAlJsVVoOZsAGPMaqbAvHiTl1jYM3.7sxTxELoXq', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'TONY', 'KHOUN', NULL, NULL),
(167, 'SOEUR SOPHEAP', 'SOEUR.SOPHEAP@10mkr79.com', NULL, '$2y$10$3ychbt0gE1yhhoJ364ZFteyHnWcxZ5o.17YrNfaFBEciV1mKj0vRm', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'SOPHEAP', 'SOEUR', NULL, NULL),
(168, 'HOUT THYRO', 'HOUT.THYRO@10mkr79.com', NULL, '$2y$10$8Mco50SlGXp7YM6A0aNUtu5nFEfbl9ZnPzltT55NDxq0ugFDZZSaG', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'THYRO', 'HOUT', NULL, NULL),
(169, 'HORN HENG', 'HORN.HENG@10mkr79.com', NULL, '$2y$10$/xj3OawsTbp0bgKaEH2ykOb3CpHAdptHvwtaCxhYXwHnsr7.QN.vG', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'HENG', 'HORN', NULL, NULL),
(170, 'SOK RATHA', 'SOK.RATHA@10mkr79.com', NULL, '$2y$10$8pxRyTOd88gdoHFzCtiz4e91dZ2Zzedx68TgwxOEPF.dYuTt3bbBq', NULL, '2024-03-23 18:59:09', '2024-03-23 18:59:09', NULL, 'RATHA', 'SOK', NULL, NULL),
(171, 'darong', 'darong1122@gmail.com', NULL, '$2y$10$CMnu57Et5M8PwY/1W3cjJ.Lic3Vwh979NFCVOvEeVxD7sH3Bsggra', NULL, '2024-03-23 19:01:00', '2024-03-23 19:01:44', '2024-03-23-65fe8c2d2d553.png', 'Chann', 'Darong', '', '1'),
(172, 'Kun Thang', 'kunthang2@gmail.com', NULL, '$2y$10$o7SoP9d3FDyGgcOrDvlSEOsbg4olNQGOoqdJrY7t4PrOoKXWn8sP.', NULL, '2024-03-23 19:30:11', '2024-03-23 19:30:11', '2024-03-23-65fe930c1f272.png', 'Kun', 'Thang', '', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_years`
--
ALTER TABLE `academic_years`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_rooms`
--
ALTER TABLE `class_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slot_times`
--
ALTER TABLE `slot_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_types`
--
ALTER TABLE `subject_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetable_slots`
--
ALTER TABLE `timetable_slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_years`
--
ALTER TABLE `academic_years`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `class_rooms`
--
ALTER TABLE `class_rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `slot_times`
--
ALTER TABLE `slot_times`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `subject_types`
--
ALTER TABLE `subject_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `timetable_slots`
--
ALTER TABLE `timetable_slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `time_tables`
--
ALTER TABLE `time_tables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
